import math
import os
import os.path
import sys

setUp=r'''
\documentclass[hyperref={pdfpagelabels=false}]{beamer}
\usepackage{verbatim}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,chains,fit,shapes}
\setbeamertemplate{navigation symbols}{}
\tikzstyle{vertex}=[circle,fill=black!25,minimum size=20pt,inner sep=0pt]
\tikzstyle{nullvertex}=[circle,fill=white!25,minimum size=20pt,inner sep=0pt]
\tikzstyle{newvertex} = [vertex, fill=blue!24]
\tikzstyle{edge} = [draw,thick,-]
\tikzstyle{selected edge} = [draw,line width=5pt,-,red!50]
\edef\arrayboxsize{0.7cm}
\tikzstyle{arrayelement}=[draw,minimum size=\arrayboxsize,align=left]
\tikzstyle{hiddenelement}=[draw=white!25,fill=white!25,minimum size=\arrayboxsize]
\begin{document}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}
'''
ending="\\end{document}"

def makeSlide(ArrayOne,ArrayTwo,c,m,ep,point,title):
	res=""
	res+="\\begin{frame}\n"
	res+="\\begin{center}"+title+"\\end{center}\n"
	res+="\\begin{figure}\n"
	res+="\\begin{tikzpicture}[->,scale=1.8, auto,swap]\n"
	res+=makeArray(ArrayOne,m,0,"(1,8)")
	res+=makeArray(ArrayTwo,m,1,"(1,6)")
	res+=makeStruct(c,m,3)
	res+=extraPtr(ep,point)
	res+=link(point)
	res+="\\end{tikzpicture}\n"
	res+="\\end{figure}\n"
	res+="\\end{frame}\n"
	return res
def link(idNum):
	nodeName = "chain_%03d_%03d" % (idNum,0)
	return "\\path (pointer.east) edge node {} ("+nodeName+");\n"
def makeStruct(c,m,idNum):
	res=""
	res+="\\begin{scope}[start chain="+str(idNum)+" going below, node distance=-0.15mm]\n"
	res+="\t\\node [on chain="+str(idNum)+", draw,minimum width=10em,align=left] at (-1,7) {{\\bf ResizeArray}};\n"
	res+="\t\\node [on chain="+str(idNum)+", draw,minimum width=10em] (pointer) {char* data};\n"
	res+="\t\\node [on chain="+str(idNum)+", draw,minimum width=10em] {int currentSize="+str(c)+"};\n"
	res+="\t\\node [on chain="+str(idNum)+", draw,minimum width=10em] {int maxSize="+str(m)+"};\n"
	res+="\\end{scope}\n"
	return res
def extraPtr(shown,point):
	if shown:
		type="arrayelement"
		value="char* newData"
	else:
		type="hiddenelement"
		value=""
	res= "\\node["+type+"] (newPtr) at (-1,8) {"+value+"};\n"
	nodeName = "chain_%03d_%03d" % ((point+1)%2,0)
	if shown:
		return res+"\\path (newPtr.east) edge node {} ("+nodeName+");\n"
	else:
		return res
def makeArray(L,maxSize,idNum,pos):
	res=""
	res+="\\begin{scope}[start chain=" \
		+str(idNum) \
		+" going right,node distance=0.16mm]\n"
	i=0
	nameFmt="chain_%03d_%03d"
	nodeFmt="\t\\node [on chain=%d,%s] (%s) %s{%s};\n"
	for x in L:
		nodeName=( nameFmt % (idNum,i))
		if x==None:
			type="hiddenelement"
			value=""
		else:
			type="arrayelement"
			value=str(x)
		if i==0:
			position="at "+pos+" "
		else:
			position=""
		res+=nodeFmt % (idNum,type,nodeName,position,value)
		i+=1
	res+="\\end{scope}\n"
	return res

def animate(wordToInsert,fileName):
	f = open(filename,"w")
	f.write(setUp)
	#Compute Max Size
	maxSize = 2**math.ceil(math.log(len(wordToInsert)+1,2))
	#Starting Size
	startSize = 1
	#Make Arrays
	ArrayOne = [None] * maxSize;
	ArrayTwo = [None] * maxSize;
	ArrayOne[0]=" "
	Arrays={0:ArrayOne,1:ArrayTwo}
	
	pointer=0
	count=0
	max=1
	X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,"Start with Empty Array")
	f.write(X)
	for letter in wordToInsert:
		if count < max:
			slideTitle="Insert Character "+letter+" into Array"
			count+=1
			Arrays[pointer][count-1]=letter
			X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,slideTitle)
			f.write(X)
		else:
			#Resize
			slideTitle="Create New Array With Double Size"
			for i in range(0,max*2):
				Arrays[(pointer+1) % 2][i]=" "
			X=makeSlide(ArrayOne,ArrayTwo,count,max,True,pointer,slideTitle)
			f.write(X)
			#Copy Elements to new Array
			for i in range(0,len(Arrays[pointer])):
				if Arrays[(pointer)][i]!=None and Arrays[(pointer)][i]!=" ":
					Arrays[(pointer+1)%2][i]=Arrays[(pointer)][i]
					slideTitle="Copy Value "+Arrays[(pointer)][i]+" to new space"
					X=makeSlide(ArrayOne,ArrayTwo,count,max,True,pointer,slideTitle)
					f.write(X)
			#Free old memory
			for i in range(0,len(Arrays[pointer])):
				Arrays[pointer][i]=None
			slideTitle="Free Old Memory"
			X=makeSlide(ArrayOne,ArrayTwo,count,max,True,pointer,slideTitle)
			f.write(X)
			#Move Pointer
			pointer=(pointer+1)%2
			slideTitle="Move Pointer to New Memory"
			max=max*2
			X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,slideTitle)
			f.write(X)
			#Do Insert
			slideTitle="Insert Character "+letter+" into Array"
			count+=1
			Arrays[pointer][count-1]=letter
			X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,slideTitle)
			f.write(X)
	X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,"Algorithm Complete")
	f.write(X)
	X=makeSlide(ArrayOne,ArrayTwo,count,max,False,pointer,"Algorithm Complete")
	f.write(X)
	f.write(ending)
	f.close()
if __name__=="__main__":
	wordToInsert="Ghosts";
	filename = os.path.splitext(sys.argv[0])[0]+".tex"
	animate(wordToInsert,filename)
	
