#include <stdio.h>

//This is a global
//it goes in the data section
int globalNumber=7;

//Factorial using a loop
int factorial(int x);

int main(int argc, char** argv){
	//This variable is constant.
	//It goes in the data section
	const char* greeting = "Hello";
	printf("%s\n",greeting);
	//x is only scoped to this loop
	//x is on the Stack
	for(int x=0; x < 10; x++){
		printf("%d! is %d\n",x,factorial(x));
	}//x is deleted when the loop ends
	//The below line will not work:
	//printf("What is x? %d\n",x);
	return 0;
}

//x is on the Stack
//it is a function parameter
int factorial(int x){
	//total is on the stack it is
	//a local
	int total = 1;
	//i is on the stack it is local
	//to this loop
	for(int i=1; i <= x; i++){
		total = total * i;
	}//i is released when this loop ends
	return total;
}//total and x are released when this function
//ends
