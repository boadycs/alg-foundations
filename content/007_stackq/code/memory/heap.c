
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
	int size=16;
	//Request a pointer for size characters
	char* ptr = malloc(size*sizeof(char));
	//Put in 15 letters and a trailing null
	for(int i=0; i < size-1; i++){
		ptr[i]=65+i;
	}
	ptr[size-1]=0;
	//Print the string to confirm it worked
	printf("String: %s\n",ptr);
	//A short is 2 bytes. We can pretend the
	//pointer points to shorts
	short* ptrS = (short*)ptr;
	//There are size/2 shorts in the array
	for(int i=0; i < size/sizeof(short); i++){
		printf("ptrS[%d]=%d\n",i,ptrS[i]);
	}
	//An integer is 4 bytes
	int* ptrI = (int*)ptr;
	for(int i=0; i < size/sizeof(int); i++){
		printf("ptrI[%d]=%d\n",i,ptrI[i]);
	}
	//A long long is 8 bytes
	long long* ptrL = (long long*)ptr;
	for(int i=0; i < size/sizeof(long long); i++){
		printf("ptrL[%d]=%lld\n",i,ptrL[i]);
	}
	//Change a value in the short version
	ptrS[0]=ptrS[0]-10;
	//See how the string changed
	printf("String After Change: %s\n",ptr);
	//Release the memory when we are done
	free(ptr);
	//End function
	return 0;
}
