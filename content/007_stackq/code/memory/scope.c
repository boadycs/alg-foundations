
#include <stdio.h>

int i=7;//Global i

//Function parameter i
int badDesign(int i);
//Function that changes global
void updateI(int x);

int main(int argc, char** argv){
	printf("main: Value of i at start: %d\n",i);
	int i=0;//Local i
	printf("main: Value of i after first statement : %d\n",i);
	int temp = badDesign(9);
	updateI(temp);
	printf("main: Value of i before return statement : %d\n",i);
	return 0;
}

int badDesign(int i){
	int total=0;
	printf("badDesign: Value of i before loop %d\n",i);
	for(int i=0; i < 4; i++){
		total = total + i;
		printf("badDesign: Value of i in loop %d\n",i);
	}
	printf("badDesign: Value of i after loop %d\n",i);
	return total*i;
}

void updateI(int x){
	printf("updateI: Value of i at start %d\n",i);
	i = x;
	printf("updateI: Value of i at end %d\n",i);
}
