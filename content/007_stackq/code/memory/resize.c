#include <stdlib.h>
#include <stdio.h>

//Resizing Array Data Structure
struct ResizeArray{
	char* data;
	int currentSize;
	int maxSize;
};
//Give the struct a short name
//typedef [struct Name] [short name]
typedef struct ResizeArray ResizeArray;

//Create a new array
ResizeArray* makeArray();
//Append to the end of the array
void append(ResizeArray* RA, char letter);
//Get the Size
int size(ResizeArray* RA);
//Expand the array
void expand(ResizeArray* RA);
//Get the array
char* getArray(ResizeArray* RA);


//Read any amount of text
char* readFromStdin();

int main(int argc, char** argv){
	printf("%lu\n",sizeof(char*));
	printf("%lu\n",sizeof(int));
	printf("Type Something:\n");
	char* str = readFromStdin();
	printf("You Typed:\n");
	printf("%s\n",str);
}

ResizeArray* makeArray(){
	//Get space for our data structure
	ResizeArray* RA = malloc(sizeof(ResizeArray));
	//Start with 8 spaces
	RA->maxSize = 8;
	//No values in the array yet
	RA->currentSize = 0;
	//Request space for the array
	RA->data = malloc(RA->maxSize * sizeof(char));
	return RA;
}


void append(ResizeArray* RA, char letter){
	//Make sure we have space
	if(RA->currentSize+1 > RA->maxSize){
		expand(RA);
	}
	//Add the element to the end
	RA->data[RA->currentSize]=letter;
	//Update the Size
	RA->currentSize++;
}

int size(ResizeArray* RA){
	return RA->currentSize;
}

void expand(ResizeArray* RA){
	printf("Resizing from %d to %d\n",RA->maxSize,RA->maxSize*2);
	//Make an array of double size
	char* newData = malloc((RA->maxSize*2) *sizeof(char));
	//Copy the values into the new array
	for(int i=0; i < RA->maxSize; i++){
		newData[i] = RA->data[i];
	}
	//Free the old array
	free(RA->data);
	//Point to the new array
	RA->data = newData;
	//Update Data Structure so it knows about new space
	RA->maxSize = RA->maxSize*2;
}

char* getArray(ResizeArray* RA){
	return RA->data;
}

char* readFromStdin(){
	//Make a Resizing array
	ResizeArray* buffer = makeArray();
	//Read Characters
	char temp = getchar();
	while(temp != EOF && temp != '\n'){
		append(buffer,temp);
		temp = getchar();
	}
	append(buffer,'\0');
	return getArray(buffer);
}
