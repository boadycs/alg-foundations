package main

import "fmt"

//The Node Data Structure
type Node struct {
	value    int   //The Value Stored at this node
	next     *Node //Pointer to the next value
	previous *Node //Pointer to previous value
}

//A Deque data structure
type Deque struct {
	head *Node //The front
	tail *Node //The back
}

//Create a new Empty Deque
func newDeque() *Deque {
	//Allocate Memory
	var D *Deque = &Deque{}
	//Set points to nil
	D.head = nil
	D.tail = nil
	//Return pointer to queue
	return D
}

//The Deque is empty if the head is null
func isEmpty(D *Deque) bool {
	return D.head == nil
}

//Look at the element at the head
//0 on nothing
func front(D *Deque) int {
	if D.head == nil {
		return 0
	}
	return D.head.value
}

//Look at the element at the tail
//0 on nothing
func back(D *Deque) int {
	if D.tail == nil {
		return 0
	}
	return D.tail.value
}

//Add to the front
func pushFront(v int, D *Deque) {
	//Allocate a new node
	var newNode *Node = &Node{}
	newNode.value = v
	newNode.next = nil
	newNode.previous = nil
	//If the head is nil, only node
	if D.head == nil {
		D.head = newNode
		D.tail = newNode
		return
	}
	//Add to front
	newNode.next = D.head
	D.head.previous = newNode
	D.head = newNode
}

//Add to the back
func pushBack(v int, D *Deque) {
	//Allocate a new node
	var newNode *Node = &Node{}
	newNode.value = v
	newNode.next = nil
	newNode.previous = nil
	//If tail is empty, only node
	if D.tail == nil {
		D.head = newNode
		D.tail = newNode
		return
	}
	//Add to back
	D.tail.next = newNode
	newNode.previous = D.tail
	D.tail = newNode
}

//Remove from front
func popFront(D *Deque) {
	//Check Empty
	if D.head == nil {
		return
	}
	//If this is the last node, nil
	if D.head == D.tail {
		D.head = nil
		D.tail = nil
		return
	}
	//Move Head forward 1
	D.head = D.head.next
	//Erase link to old front
	if D.head != nil {
		D.head.previous = nil
	}
}

//Remove from Back
func popBack(D *Deque) {
	//Check Empty
	if D.head == nil {
		return
	}
	//If this is the last node, nil
	if D.head == D.tail {
		D.head = nil
		D.tail = nil
		return
	}
	//Remove from back
	D.tail = D.tail.previous
	//Erase link to old tail
	if D.tail != nil {
		D.tail.next = nil
	}
}

func main() {
	var N int = 7
	var M int = 3
	var Q *Deque = newDeque()
	for i := 0; i < N; i++ {
		pushFront(i, Q)
	}

	var count int = 0
	for !isEmpty(Q) {
		var x int = back(Q)
		popBack(Q)
		if count%M == M-1 {
			fmt.Println(x)
		} else {
			pushFront(x, Q)
		}
		count = count + 1
	}
}
