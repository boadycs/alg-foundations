package main

import "fmt"

//The Node Data Structure
type Node struct {
	value int   //The Value Stored at this node
	next  *Node //Pointer to the next value
}

//A Queue data structure
type Queue struct {
	head *Node //The front
	tail *Node //The back
}

//Create a new Empty Queue
func newQueue() *Queue {
	//Allocate Memory
	var Q *Queue = &Queue{}
	//Set points to nil
	Q.head = nil
	Q.tail = nil
	//Return pointer to queue
	return Q
}

//Check if Queue is Empty
func isEmpty(Q *Queue) bool {
	return Q.head == nil
}

//Add a new element to back of queue
func enqueue(v int, Q *Queue) {
	var newNode *Node = &Node{}
	newNode.value = v
	newNode.next = nil
	if Q.head == nil {
		//This is the first value
		Q.head = newNode
		Q.tail = newNode
	} else {
		//Add to the end
		Q.tail.next = newNode
		Q.tail = newNode
	}
}

//Look at the first value
func front(Q *Queue) int {
	if Q.head == nil {
		return 0
	}
	return Q.head.value
}

//Remove element from queue
func dequeue(Q *Queue) {
	if Q.head == nil {
		return //Nothing to do
	}
	//Move forward 1 space
	Q.head = Q.head.next
	//We just erased the tail
	if Q.head == nil {
		Q.tail = nil
	}
}

//The Josephus Problem
func main() {
	var N int = 7
	var M int = 3
	var Q *Queue = newQueue()
	for i := 0; i < N; i++ {
		enqueue(i, Q)
	}

	var count int = 0
	for !isEmpty(Q) {
		var x int = front(Q)
		dequeue(Q)
		if count%M == M-1 {
			fmt.Println(x)
		} else {
			enqueue(x, Q)
		}
		count = count + 1
	}
}
