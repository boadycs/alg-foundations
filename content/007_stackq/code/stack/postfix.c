#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//The Node Data Structure
typedef struct node Node;
struct node {
	int value; //The Value Stored at this node
	Node* next; //Pointer to the next value
};


//A Stack
typedef struct stack Stack;
struct stack {
	Node* head; //The first element on the stack
};

//Create a new Stack
Stack* newStack(){
	Stack* S = malloc(sizeof(Stack));
	S->head = NULL;
	return S;
}

//isEmpty: Check if stack is empty
bool isEmpty(Stack* S){
	if(S->head == NULL){
		return true;
	}
	return false;
}

//Top: Find out the top value on the stack
//Return -1 if no value
int top(Stack* S){
	//Check for Empty
	if(S->head == NULL){
		return -1;
	}
	return S->head->value;
}

//Push: add a Value onto the stack
//Input is a POINTER to the stack
void push(int value, Stack* S){
	//Allocate a code and get pointer to it
	Node* newNode = malloc(sizeof(Node));
	//Add values to new Node
	newNode->value = value;
	newNode->next = S->head;
	//Add to Front of List
	S->head = newNode;
}

//Pop: Remove top value
void pop(Stack* S){
	if(S->head == NULL){
		return;
	}
	Node* old = S->head;
	S->head = S->head->next;
	free(old);
}
//return -1 if no chars
int findSpace(char* str, int start){
	for(int i=start; str[i]!=0; i++){
		if(str[i]==' '){return i;}
	}
	return -1;
}
char** splitOnSpaces(char* str){
	char** tokens = malloc(sizeof(char*)*50);
	int pos=0;
	int start = 0;
	int stop = findSpace(str,start);
	while(stop!=-1){
		char* token = malloc(sizeof(char)*(stop-start+1));
		for(int k=start;k<stop;k++){
			token[k-start]=str[k];
		}
		token[stop-start]=0;
		start=stop+1;
		stop = findSpace(str,start);
		tokens[pos]=token;
		pos++;
	}
	//Last Word
	char* token = malloc(sizeof(char)*(100));
	int k=0;
	while(str[start]!=0){
		token[k]=str[start];
		start++;
		k++;
	}
	token[k]=0;
	tokens[pos]=token;
	pos++;
	tokens[pos]=NULL;
	return tokens;
}
bool isNumber(char* s){
	for(int k=0; s[k]!=0; k++){
		if(s[k] < '0' || s[k] > '9'){
			return false;
		}
	}
	return true;
}
int stringToInt(char* s){
	int total=0;
	for(int k=0; s[k]!=0; k++){
		total = total*10 + (s[k]-48);
	}
	return total;
}
bool isOperator(char* s){
	if(s==NULL){return false;}
	if(s[0]=='-'){return true;}
	if(s[0]=='+'){return true;}
	if(s[0]=='*'){return true;}
	return false;
}

int compute(char* input){
	//Create a New Stack
	Stack* myStack = newStack();
	//Split the string up on spaces
	char** tokens = splitOnSpaces(input);
	for(int i = 0; tokens[i]!=NULL; i++){
		printf("TOKEN: %s\n",tokens[i]);
		if(isNumber(tokens[i])){
			int value = stringToInt(tokens[i]);
			push(value, myStack);
		}
		if(isOperator(tokens[i])){
			int b = top(myStack);
			pop(myStack);
			int a = top(myStack);
			pop(myStack);
			char operator = tokens[i][0];
			int result = 0;
			if(operator == '+'){
				result = a + b;
			}else if( operator == '-'){
				result = a - b;
			}else if(operator == '*'){
				result = a * b;
			}
			push(result, myStack);
		}
	}
	if(isEmpty(myStack)){
		return 0; //Invalid Expression
	} else {
		int res = top(myStack);
		pop(myStack);
		return res; //Result of Last Computation
	}
	return 0;
}


int main(int argc, char** argv){
	int x = compute("25 17 * 12 + 125 -");
	printf("Result: %d\n",x);
	return 0;
}
