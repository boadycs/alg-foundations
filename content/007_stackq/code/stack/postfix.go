package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//The Node Data Structure
type Node struct {
	value int   //The Value Stored at this node
	next  *Node //Pointer to the next value
}

//A Stack
type Stack struct {
	head *Node //The first element on the stack
}

//Functions to Work with DS
func newStack() *Stack {
	//Allocate Memory
	//Pointer to a Stack
	var S *Stack = &Stack{}
	//Set Head as Null
	S.head = nil
	return S
}

//Push: add a Value onto the stack
//Input is a POINTER to the stack
func push(value int, S *Stack) {
	//Allocate a code and get pointer to it
	var newNode *Node = &Node{}
	//Add values to new Node
	newNode.value = value
	newNode.next = S.head
	//Add to Front of List
	S.head = newNode
}

//Top: Find out the top value on the stack
//Return 0 if no value
func top(S *Stack) int {
	//Check for Empty
	if S.head == nil {
		return 0
	}
	return S.head.value
}

//Pop: Remove top value
func pop(S *Stack) {
	if S.head == nil {
		return
	}
	S.head = S.head.next
}

//isEmpty: Check if stack is empty
func isEmpty(S *Stack) bool {
	if S.head == nil {
		return true
	}
	return false
}

//Functions used to make code look prettier
func askForInput() string {
	fmt.Println("Enter a Blank line to quit")
	fmt.Println("Enter Math in Postfix:")
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		var input string
		input = scanner.Text()
		return input
	}
	return ""
}
func printResult(res int) {
	fmt.Println("Result:", res)
}
func greeting() {
	fmt.Println("Simple Postfix Calculator")
}
func farewell() {
	fmt.Println("Thank you for using.")
}
func splitUpLetters(input string) []string {
	return strings.Split(input, "")
}
func isNumber(letter string) bool {
	return strings.Contains("0123456789", letter)
}
func isOperator(letter string) bool {
	return strings.Contains("+-*", letter)
}
func stringToInt(letter string) int {
	i, err := strconv.Atoi(letter)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	return i
}

//Functions that do Stuff
func compute(input string) int {
	var myStack *Stack = newStack()

	letters := splitUpLetters(input)
	for i := 0; i < len(letters); i++ {
		if isNumber(letters[i]) {
			var value int = stringToInt(letters[i])
			push(value, myStack)
		}
		if isOperator(letters[i]) {
			var b int = top(myStack)
			pop(myStack)
			var a int = top(myStack)
			pop(myStack)
			var operator string = letters[i]
			var result int = 0
			if operator == "+" {
				result = a + b
			} else if operator == "-" {
				result = a - b
			} else if operator == "*" {
				result = a * b
			}
			push(result, myStack)
		}
	}
	if isEmpty(myStack) {
		return 0 //Invalid Expression
	} else {
		var res int = top(myStack)
		pop(myStack)
		return res //Result of Last Computation
	}
}

func main() {
	greeting()
	var input string = askForInput()
	for len(input) != 0 {
		var result int = compute(input)
		printResult(result)
		input = askForInput()
	}
}
