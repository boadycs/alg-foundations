
#30x15 grid

#Mark Boady
#CS 260 Drexel University 2017
#A room for A* to find a path in

h=15
w=30
empty=' '
wall='X'
exit = 'E'
room = [[ empty for x in range(w)] for y in range(h)]

#Border
for x in range(0,h):
	room[x][0]=wall
	room[x][w-1]=wall
for y in range(0,w):
	room[0][y]=wall
	room[h-1][y]=wall

#Wall 1
for x in range(3,10):
	room[x][3]=wall
	room[x][4]=wall

#Wall 2
for x in range(7,h-1):
	room[x][15]=wall
	room[x][16]=wall

#Wall 3
for x in range(1,h-3):
	room[x][20]=wall
	room[x][21]=wall

room[2][w-4]=exit
goal = (2,w-4)

#Methods
def print_map(m):
	for row in m:
		for tile in row:
			print(tile,end='')
		print("")


if __name__=="__main__":
	print_map(room)