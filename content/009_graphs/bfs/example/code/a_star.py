#Mark Boady
#CS 260 Drexel University 2017
#A room for A* to find a path in

import room
import queue
import sys
import math

def get_neighbors(node):
    x = node[0]
    y = node[1]
    N=[]
    if x+1 < len(room.room) and room.room[x+1][y]!=room.wall:
        N.append((x+1,y))
    if x > 0 and room.room[x-1][y] != room.wall:
        N.append((x-1,y))
    if y > 0 and room.room[x][y-1] != room.wall:
        N.append((x,y-1))
    if y < len(room.room[x]) and room.room[x][y+1] != room.wall:
        N.append((x,y+1))
    return N
#Fixed Weights
def weight(a,b):
    return 1
#Heuristic
def heuristic(g,n):
    x1 = g[0]
    y1 = g[1]
    x2 = n[0]
    y2 = n[1]
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)
    #return 0

def a_star(start):
    pqueue = queue.PriorityQueue()
    pqueue.put((0,start))
    came_from = {}
    cost_so_far={}
    came_from[start]=None
    cost_so_far[start]=0

    while not pqueue.empty():
        node = pqueue.get()
        current = node[1]
        N = get_neighbors(current)
        if current == room.goal:
            return came_from
        for next in N:
            new_cost = cost_so_far[current] + weight(current,next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(room.goal,next)
                pqueue.put((priority,next))
                came_from[next] = current
    return came_from

def recover_path(P):
    path=[]
    previous = P[room.goal]
    while previous!=None:
        path.insert(0,previous)
        previous = P[previous]
    return path

counter=1
if __name__=="__main__":
    P=a_star((6,1))
    path=recover_path(P)
    for node in path:
        print("Step "+str(counter))
        counter+=1
        x = node[0]
        y = node[1]
        room.room[x][y] = "*"
        room.print_map(room.room)
