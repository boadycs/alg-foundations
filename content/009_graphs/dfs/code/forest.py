#Mark Boady
#CS 260 Drexel University 2017
#A Forest for BFS
import random
random.seed()

def print_forest(m):
    for row in m:
        for col in row:
            print(col.format('sss'),end=' ')
        print()
def random_forest():
    forest = [[ empty for x in range(w)] for y in range(h)]
    forest[random.randint(0,w-1)][random.randint(0,h-1)]=target
    return forest

h = 5
w = 5
target = 'T'
found = 'X'
empty = ' '

if __name__=="__main__":
    f = random_forest()
    print_forest(f)

