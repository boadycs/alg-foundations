

#Mark Boady
#CS 260 Drexel University 2017
#A Maze for DFS

def print_maze(m):
    for row in m:
        for col in row:
            print(col.format('sss'),end=' ')
        print()

h=10
w=12

empty=' '
wall_h='_'
wall_v='|'
found='X'
exit='E'

maze = [[ empty for x in range(w)] for y in range(h)]

#Row 1
maze[0] = [wall_h for x in range(w)]
maze[0][1]=empty

#Bottom
maze[h-1] = [wall_h for x in range(w)]
maze[h-1][w-2]=exit

#side columns
for i in range(h):
    maze[i][0]=wall_v
    maze[i][w-1]=wall_v

#Maze Part
maze[0][2]=wall_v
maze[1][5]=wall_v
maze[2][2]=wall_h
maze[2][3]=wall_h
maze[2][5]=wall_v
maze[3][2]=wall_v
maze[3][5]=wall_v
maze[4][2]=wall_h
maze[4][3]=wall_h
maze[4][4]=wall_h
maze[4][5]=wall_h
maze[4][6]=wall_h
maze[4][7]=wall_h
maze[5][3]=wall_v
maze[5][7]=wall_v
maze[6][1]=wall_h
maze[6][7]=wall_v
maze[7][3]=wall_h
maze[7][3]=wall_h
maze[7][4]=wall_h
maze[7][5]=wall_h
maze[7][6]=wall_h
maze[7][7]=wall_h
maze[3][w-3]=wall_v
maze[4][w-3]=wall_v
maze[5][w-3]=wall_v
maze[6][w-3]=wall_v
maze[7][w-3]=wall_v
maze[8][w-3]=wall_v
maze[9][w-3]=wall_v


if __name__=="__main__":
    print_maze(maze)