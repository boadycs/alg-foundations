#Mark Boady
#CS 260 Drexel University 2017
#A Maze for DFS

from maze import *
iteration=0

#Note: this DFS is specific to the maze representation
#It is not of general use
#Run DFS on maze m starting at position (x,y)
#Runs until it finds an exit
def dfs(m,x,y):
    global iteration
    #In Maze
    if x < 0 or x >= len(m):
        return False
    if y < 0 or y >= len(m[x]):
        return False
    #Is this a valid position
    if m[x][y]==exit:
        print("Found Exit")
        return True
    if m[x][y]==empty:
        m[x][y]=found
        iteration+=1
        print("DFS Iteration "+str(iteration))
        print_maze(m)
        #There are 4 directions
        #Left
        if dfs(m,x-1,y):
            return True
        #Right
        if dfs(m,x+1,y):
            return True
        #Up
        if dfs(m,x,y-1):
            return True
        #Down
        if dfs(m,x,y+1):
            return True
    return False




dfs(maze,0,1)