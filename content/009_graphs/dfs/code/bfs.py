#Mark Boady
#CS 260 Drexel University 2017
#A BFS

from forest import *
from collections import deque

#Searches the forest f for traget starting at (x,y)
#This is only designed for this specific forest matrix model
#A python list is used as a queue
def bfs(f,x,y):
    iteration=0
    #Make a Queue
    queue = deque([[x,y]])
    while len(queue) > 0:
        pos = queue.popleft()
        if f[pos[0]][pos[1]]==target:
            print("Target Found!")
            return
        #See if we are target
        if f[pos[0]][pos[1]]==empty:
            f[pos[0]][pos[1]]=found
            print("Iteration "+str(iteration))
            print_forest(f)
            iteration+=1
                #There are 4 possible places to search
        #Left
        if pos[0]-1 >= 0 and f[pos[0]-1][pos[1]]!=found:
            queue.append([pos[0]-1,pos[1]])
        #Right
        if pos[0]+1 < len(f) and f[pos[0]+1][pos[1]]!=found:
            queue.append([pos[0]+1,pos[1]])
        #Up
        if pos[1]-1 >=0 and f[pos[0]][pos[1]-1]!=found:
            queue.append([pos[0],pos[1]-1])
        #Down
        if pos[1]+1 < len(f[pos[0]]) and f[pos[0]][pos[1]+1]!=found:
            queue.append([pos[0],pos[1]+1])



if __name__=="__main__":
    f = random_forest()
    bfs(f,0,0)

