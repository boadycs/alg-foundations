//Mark Boady
//Drexel 2021
//Graph Algorithms in Go
//This file is for graph representations.

package graphlib

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

/* ----------------------------------------------
	The goal is for this code to read well as
	language agnostic code.
	These helper functions abstract complexity.
   ----------------------------------------------
*/

//Type names that improve readability

//Call a 2D array a Matrix
type IntMatrix [][]int

//makeEmptyMatrix makes a rows x cols matrix that is empty
func makeEmptyMatrix(rows int, cols int) IntMatrix {
	var matrix IntMatrix = make(IntMatrix, rows)
	for i := 0; i < rows; i++ {
		matrix[i] = make([]int, cols)
	}
	return matrix
}

//Make an Empty Array
//because this constructor is very go specific
func newArray(size int) []int {
	return make([]int, size)
}

//Fill Array with Values
func fillArray(A []int, v int) {
	for i := 0; i < len(A); i++ {
		A[i] = v
	}
}

//printMatrix helps with testing
func printGraph(G Graph) {
	var rows int = G.NumberNodes()
	for i := 0; i < rows; i++ {
		var cols int = rows
		for j := 0; j < cols; j++ {
			if G.Weight(i, j) > 10000 {
				fmt.Print(" inf ")
			} else {
				fmt.Printf("%4d ", G.Weight(i, j))
			}
		}
		fmt.Println()
	}
}

//printArray with pretty infs
func printArray(Arr []int) {
	for i := 0; i < len(Arr); i++ {
		if Arr[i] > 10000 {
			fmt.Print(" inf ")
		} else {
			fmt.Printf("%4d ", Arr[i])
		}
	}
	fmt.Println()
}

//---------------------------------------------------------

/*
	Graph Interface
	This allows us to run the same algorithms
	with an Adjacency Matrix or
	and Adjacency List
*/

//Graph is an interface so we can use adjacency matrixes or lists
type Graph interface {
	//Make an empty Graph of n x n
	EmptyGraph(nodes int)
	//Add a Single Edge
	AddEdge(from int, to int, weight int)
	//Return number we treat as infinity
	GetInfinity() int
	//How many nodes are in the graph
	NumberNodes() int
	//What is the weight from x to y
	Weight(x int, y int) int
	//Return a list of nodes adjacent to x
	AdjacentTo(x int) []int
	//Make graph based on file that is directed
	LoadGraphDirected(filename string)
	//Make graph based on file that is undirected
	LoadGraphUndirected(filename string)
}

/*
	Adjacency Matrix Implementation
	Implement the Graph Interface using
	an Adjacency Matrix
*/

//AdjMatrix is an Adjacent Matrix Implementation of a Graph
type AdjMatrix struct {
	matrix   [][]int
	infinity int
}

//CreateAdjMatrix Constructs a new Graph
func CreateAdjMatrix() *AdjMatrix {
	return &AdjMatrix{}
}

//EmptyGraph Creates an n by n graph with all inf and 0 on diagonal
func (myGraph *AdjMatrix) EmptyGraph(nodes int) {
	//We need to know what infinity is
	myGraph.infinity = int(^uint(0)>>1) / 2
	myGraph.matrix = makeEmptyMatrix(nodes, nodes)
	//Infinity Fill
	for i := 0; i < nodes; i++ {
		for j := 0; j < nodes; j++ {
			if i == j {
				myGraph.matrix[i][j] = 0
			} else {
				myGraph.matrix[i][j] = myGraph.infinity
			}
		}
	}
}

//AddEdge adds a new edge at a given (from,to) with weight w
func (myGraph *AdjMatrix) AddEdge(from int, to int, w int) {
	myGraph.matrix[from][to] = w
}

//NumberNodes return the number of nodes in the graph
func (myGraph *AdjMatrix) NumberNodes() int {
	return len(myGraph.matrix)
}

//Weight returns the weight from x to y
func (myGraph *AdjMatrix) Weight(x int, y int) int {
	return myGraph.matrix[x][y]
}

//GetInfinity returns the value we treat as infinity
func (myGraph *AdjMatrix) GetInfinity() int {
	return myGraph.infinity
}

//AdjacentTo returns a list of all nodes adjacent to x
func (myGraph *AdjMatrix) AdjacentTo(x int) []int {
	var A []int = newArray(0)
	for i := 0; i < len(myGraph.matrix[x]); i++ {
		if myGraph.matrix[x][i] > 0 && myGraph.matrix[x][i] < myGraph.infinity {
			A = append(A, i)
		}
	}
	return A
}

//LoadGraphDirected loads a graph from a file with directed edges
func (myGraph *AdjMatrix) LoadGraphDirected(filename string) {
	//Fill Edges from file
	data, err := ioutil.ReadFile(filename)
	//We might not be able to open the file
	if err != nil {
		fmt.Println("Could not read", filename)
		fmt.Println(err)
		return
	}
	//Split up the contents into lines
	var contents string = string(data)
	var lines []string = strings.Split(contents, "\n")
	//The first line is the number of nodes
	numNodes, err := strconv.Atoi(lines[0])
	//Make sure we got a number for the size
	if err != nil {
		fmt.Println("Could Not Parse Number of Lines", lines[0])
		return
	}
	//Generate our Empty Graph
	myGraph.EmptyGraph(numNodes)
	//Each line has an edge, parse and add to graph
	for i := 1; i < len(lines); i++ {
		var values []string = strings.Split(lines[i], " ")
		if len(values) != 3 {
			return
		}
		from, err1 := strconv.Atoi(values[0])
		to, err2 := strconv.Atoi(values[1])
		weight, err3 := strconv.Atoi(values[2])
		if err1 != nil || err2 != nil || err3 != nil {
			fmt.Println("Could Not Parse:", lines[i])
			return
		}
		myGraph.AddEdge(from, to, weight)
	}
}

//LoadGraphUndirected loads a graph from a file with undirected edges
func (myGraph *AdjMatrix) LoadGraphUndirected(filename string) {
	//Fill Edges from file
	data, err := ioutil.ReadFile(filename)
	//We might not be able to open the file
	if err != nil {
		fmt.Println("Could not read", filename)
		fmt.Println(err)
		return
	}
	//Split up the contents into lines
	var contents string = string(data)
	var lines []string = strings.Split(contents, "\n")
	//The first line is the number of nodes
	numNodes, err := strconv.Atoi(lines[0])
	//Make sure we got a number for the size
	if err != nil {
		fmt.Println("Could Not Parse Number of Lines", lines[0])
		return
	}
	//Generate our Empty Graph
	myGraph.EmptyGraph(numNodes)
	//Each line has an edge, parse and add to graph
	for i := 1; i < len(lines); i++ {
		var values []string = strings.Split(lines[i], " ")
		from, err1 := strconv.Atoi(values[0])
		to, err2 := strconv.Atoi(values[1])
		weight, err3 := strconv.Atoi(values[2])
		if err1 != nil || err2 != nil || err3 != nil {
			fmt.Println("Could Not Parse:", lines[i])
			return
		}
		myGraph.AddEdge(from, to, weight)
		myGraph.AddEdge(to, from, weight)
	}
}

/*
	Edge Structure
	Sometimes we need to organize edges
*/
//An Edge is just a tuple (from,to,weight)
type edge struct {
	from int
	to   int
	w    int
}
