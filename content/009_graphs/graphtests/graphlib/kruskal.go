//Mark Boady
//Drexel University 2021

package graphlib

//arrayOfEdges makes an array with all edges
func arrayOfEdges(G Graph) []edge {
	var E []edge = make([]edge, 0)
	for i := 0; i < G.NumberNodes(); i++ {
		for j := 0; j < G.NumberNodes(); j++ {
			if G.Weight(i, j) < G.GetInfinity() && G.Weight(i, j) > 0 {
				var myEdge edge = edge{i, j, G.Weight(i, j)}
				E = append(E, myEdge)
			}
		}
	}
	return E
}

/*
	Quicksort
	Use Quicksort to sort the edges
*/

//Quicksort sorts a list in place
func Quicksort(E []edge) {
	quicksortRec(E, 0, len(E)-1)
}

//quicksortRec is the Recursive Quicksort
func quicksortRec(E []edge, start int, stop int) {
	if start < stop {
		var p int = partition(E, start, stop)
		quicksortRec(E, start, p-1)
		quicksortRec(E, p+1, stop)
	}
}

//partition is used to parition the array
func partition(A []edge, start int, stop int) int {
	//Select Random index
	var randomIndex int = stop
	//Swap with last position
	swap(A, stop, randomIndex)
	//Partition Based on Last Index Pivot
	var pivot edge = A[stop]
	var i int = start
	for j := start; j < stop; j++ {
		//Swap a small and large value into place
		if compareEdges(A[j], pivot) {
			swap(A, i, j)
			i = i + 1
		}
	}
	//Put the pivot in place
	swap(A, i, stop)
	//Return index of pivot
	return i
}

//Swap Two Values
func swap(A []edge, x int, y int) {
	var temp edge = A[x]
	A[x] = A[y]
	A[y] = temp
}

//compareEdges determines is w(a) < w(b)
func compareEdges(a edge, b edge) bool {
	return a.w < b.w
}

/*
	Bit Vector Implemenation of Collection of Sets
*/
//Make a Type for a Collection of Sets
type setCollection struct {
	mySets []int
}

//Create a Universe with n single sets
func createSetCollection(n int) *setCollection {
	if n > 64 {
		panic("Kruskal has to many nodes!")
	}
	var S *setCollection = &setCollection{}
	var thisSet int = 1
	for i := 0; i < n; i++ {
		S.mySets = append(S.mySets, thisSet)
		thisSet = thisSet << 1
	}
	return S
}

//findSet finds the array index of the set containing x
func findSet(x int, S *setCollection) int {
	var target int = 1 << x
	for i := 0; i < len(S.mySets); i++ {
		if (target & S.mySets[i]) == target {
			return i
		}
	}
	return -1 //Not in Collection
}

//union merges the sets containing x and y
func union(S *setCollection, x int, y int) {
	var posX int = findSet(x, S)
	var posY int = findSet(y, S)
	var setX int = S.mySets[posX]
	var setY int = S.mySets[posY]
	var newSet int = setX | setY
	//Swap my old values to the end
	//Delete larger index first
	if posX > posY {
		S.mySets[posX] = S.mySets[len(S.mySets)-1]
		S.mySets[posY] = S.mySets[len(S.mySets)-2]
		S.mySets = S.mySets[:len(S.mySets)-2]
	} else {
		S.mySets[posY] = S.mySets[len(S.mySets)-1]
		S.mySets[posX] = S.mySets[len(S.mySets)-2]
		S.mySets = S.mySets[:len(S.mySets)-2]
	}
	S.mySets = append(S.mySets, newSet)
}

/*
	Kruskal's Algorithm for MST
	Returns a new Graph with only the MST edges
*/
func Kruskal(G Graph) Graph {
	//Create an Empty Graph to store results
	var MST *AdjMatrix = CreateAdjMatrix()
	//Start with all infinity values except diagonal
	MST.EmptyGraph(G.NumberNodes())
	//Make an Array of All Edges
	var EdgeList []edge = arrayOfEdges(G)
	//Sort the Edges
	Quicksort(EdgeList)
	//Create a Collection of sets
	var S *setCollection = createSetCollection(G.NumberNodes())
	//Loop over all edges
	for i := 0; i < len(EdgeList); i++ {
		var e edge = EdgeList[i]
		if findSet(e.from, S) != findSet(e.to, S) {
			//Add to the MST
			MST.AddEdge(e.from, e.to, e.w)
			MST.AddEdge(e.to, e.from, e.w)
			//Merge the Sets
			union(S, e.from, e.to)
		}
	}
	//Return the MST Graph
	return MST
}

/* Expected Output with print of edges!
Testing Krukal's Algorithm
Matrix for tests/undir_input1.txt
   0    8    1    5  inf  inf
   8    0    7  inf    3  inf
   1    7    0    6    9    4
   5  inf    6    0  inf    2
 inf    3    9  inf    0   10
 inf  inf    4    2   10    0
Running Kurskal
Added Edge {2 0 1}
Added Edge {5 3 2}
Added Edge {4 1 3}
Added Edge {5 2 4}
Added Edge {1 2 7}
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
Matrix for tests/undir_input2.txt
   0    4  inf  inf  inf  inf  inf   10  inf
   4    0    8  inf  inf  inf  inf   13  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11   14  inf  inf  inf
 inf  inf  inf   11    0   12  inf  inf  inf
 inf  inf    5   14   12    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1    6
  10   13  inf  inf  inf  inf    1    0    9
 inf  inf    3  inf  inf  inf    6    9    0
Running Kurskal
Added Edge {7 6 1}
Added Edge {5 6 2}
Added Edge {8 2 3}
Added Edge {0 1 4}
Added Edge {5 2 5}
Added Edge {2 3 7}
Added Edge {2 1 8}
Added Edge {3 4 11}
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
Matrix for tests/undir_input3.txt
   0  inf   26  inf   38  inf   58   16
 inf    0   36   29  inf   32  inf   19
  26   36    0   17  inf  inf   40   34
 inf   29   17    0  inf  inf   52  inf
  38  inf  inf  inf    0   35   93   37
 inf   32  inf  inf   35    0  inf   28
  58  inf   40   52   93  inf    0  inf
  16   19   34  inf   37   28  inf    0
Running Kurskal
Added Edge {7 0 16}
Added Edge {2 3 17}
Added Edge {7 1 19}
Added Edge {0 2 26}
Added Edge {7 5 28}
Added Edge {4 5 35}
Added Edge {2 6 40}
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
*/
