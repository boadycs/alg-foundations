//Mark Boady
//Drexel 2021
//Test Dijsktra's Algorithm

package graphlib

import "fmt"

//Test with three example files
func DijkstraTest() {
	fmt.Println("Testing Dijkstra's Algorithm")
	//Files to Test
	var files []string = []string{
		"tests/dir_input1.txt",
		"tests/dir_input2.txt",
		"tests/dir_input3.txt",
	}
	//Loop over files
	for i := 0; i < len(files); i++ {
		var filename string = files[i]
		var G *AdjMatrix = CreateAdjMatrix()
		fmt.Println("Matrix for", filename)
		G.LoadGraphDirected(filename)
		printGraph(G)
		for j := 0; j < G.NumberNodes(); j++ {
			fmt.Println("Distance Starting at", j)
			var Res []int = Dijkstra(G, j)
			printArray(Res)
		}
	}
}

/*Expected Output

Matrix for input1.txt
   0    1  inf  inf  inf
 inf    0    2  inf  inf
 inf  inf    0    2    4
 inf    1    3    0  inf
 inf  inf  inf    5    0
Distance Starting at 0
   0    1    3    5    7
Distance Starting at 1
 inf    0    2    4    6
Distance Starting at 2
 inf    3    0    2    4
Distance Starting at 3
 inf    1    3    0    7
Distance Starting at 4
 inf    6    8    5    0
Matrix for input2.txt
   0    3  inf    4  inf    5
 inf    0    1  inf  inf    1
 inf  inf    0    2  inf  inf
 inf    3  inf    0  inf  inf
 inf  inf  inf    3    0    2
 inf  inf  inf    2  inf    0
Distance Starting at 0
   0    3    4    4  inf    4
Distance Starting at 1
 inf    0    1    3  inf    1
Distance Starting at 2
 inf    5    0    2  inf    6
Distance Starting at 3
 inf    3    4    0  inf    4
Distance Starting at 4
 inf    6    7    3    0    2
Distance Starting at 5
 inf    5    6    2  inf    0
Matrix for input3.txt
   0  inf   26  inf   38  inf  inf  inf
 inf    0  inf   29  inf  inf  inf  inf
 inf  inf    0  inf  inf  inf  inf   34
 inf  inf  inf    0  inf  inf   52  inf
 inf  inf  inf  inf    0   35  inf   37
 inf   32  inf  inf   35    0  inf   28
  58  inf   40  inf   93  inf    0  inf
 inf  inf  inf   39  inf   28  inf    0
Distance Starting at 0
   0  105   26   99   38   73  151   60
Distance Starting at 1
 139    0  121   29  174  183   81  155
Distance Starting at 2
 183   94    0   73   97   62  125   34
Distance Starting at 3
 110  186   92    0  145  154   52  126
Distance Starting at 4
 186   67  168   76    0   35  128   37
Distance Starting at 5
 171   32  153   61   35    0  113   28
Distance Starting at 6
  58  134   40  113   93  102    0   74
Distance Starting at 7
 149   60  131   39   63   28   91    0
*/
