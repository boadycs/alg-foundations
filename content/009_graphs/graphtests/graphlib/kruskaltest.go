//Mark Boady
//Drexel 2021
//Test Prim's Algorithm

package graphlib

import "fmt"

//Test the Algorithm on Input Files
func KruskalTest() {
	fmt.Println("Testing Krukal's Algorithm")
	//Files to Test
	var files []string = []string{
		"tests/undir_input1.txt",
		"tests/undir_input2.txt",
		"tests/undir_input3.txt",
	}
	//Loop over files
	for i := 0; i < len(files); i++ {
		var filename string = files[i]
		var G *AdjMatrix = &AdjMatrix{}
		fmt.Println("Matrix for", filename)
		G.LoadGraphUndirected(filename)
		printGraph(G)

		fmt.Println("Running Kurskal")
		var Res Graph = Kruskal(G)
		printGraph(Res)
	}
}

/* Expected Output:

 */
