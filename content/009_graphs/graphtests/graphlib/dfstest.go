//Mark Boady
//Drexel 2021
//Test DFS Algorithm

package graphlib

import "fmt"

//Test with three example files
func DfsTest() {
	fmt.Println("Testing DFS Algorithm")
	//Files to Test
	var files []string = []string{
		"tests/dfs_input1.txt",
	}
	//Loop over files
	for i := 0; i < len(files); i++ {
		var filename string = files[i]
		var G *AdjMatrix = CreateAdjMatrix()
		fmt.Println("Matrix for", filename)
		G.LoadGraphDirected(filename)
		printGraph(G)
		for j := 0; j < G.NumberNodes(); j++ {
			fmt.Println("DFS Starting at", j)
			var Res [][]int = DFS(G, j)
			fmt.Println("Found Counters")
			printArray(Res[0])
			fmt.Println("Finished Counters")
			printArray(Res[1])
		}
	}
}
