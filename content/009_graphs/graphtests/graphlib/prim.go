//Mark Boady
//Drexel University 2021

package graphlib

import (
	"container/heap"
)

/*
	Priority Queue of Edges
	We need to know the edge with the smallest weight
	from set U to set V
*/
//edgeQueue is a Priority Queue of Edges
type edgeQueue []*edge

//Len returns the number of values in the Priority Queue
func (pq edgeQueue) Len() int { return len(pq) }

//Less is used to determine the smallest value, uses weight
//breaks ties on from node then on to node
func (pq edgeQueue) Less(i, j int) bool {
	if pq[i].w == pq[j].w {
		if pq[i].from == pq[j].from {
			return pq[i].to < pq[j].to
		}
		return pq[i].from < pq[j].from
	}
	return pq[i].w < pq[j].w
}

//Pop removes the last item from the Priority Queue
func (pq *edgeQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

//Push adds a new item to the Priority Queue
func (pq *edgeQueue) Push(x interface{}) {
	item := x.(*edge)
	*pq = append(*pq, item)
}

//Swap trades to values
func (pq edgeQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

/*
	Priority Queue Helpers
	To make Prim's Algorithm easier to read
*/

//createPiorityQueue takes a distance array and makes a Priority Queue
func createEdgePQ() *edgeQueue {
	var PQ edgeQueue = make(edgeQueue, 0)
	return &PQ
}

//minEdge get the minimum edge from the queue
func minEdge(PQ *edgeQueue) edge {
	var e *edge = heap.Pop(PQ).(*edge)
	return *e
}

//addNode adds a new pair (distance, node) to the Priority Queue
func addEdge(PQ *edgeQueue, from int, to int, w int) {
	var e *edge = &edge{from, to, w}
	heap.Push(PQ, e)
}

/*
	Support Functions
	To make the Algorithm as readable as possible
*/

func pushAdjEdges(PQ *edgeQueue, G Graph, nodeID int) {
	var adj []int = G.AdjacentTo(nodeID)
	for i := 0; i < len(adj); i++ {
		var from int = nodeID
		var to int = adj[i]
		var weight int = G.Weight(from, to)
		addEdge(PQ, from, to, weight)
	}
}

/*
	Use an Array as a Set
*/

//A set is just a wrapper around an array
type set struct {
	myItems []int
}

//newSet creates a new empty set
func newSet() *set {
	var pt *set = &set{}
	return pt
}

//addToSet adds a new value if it is not in the set already
func addToSet(v int, S *set) {
	if !isInSet(v, S) {
		S.myItems = append(S.myItems, v)
	}
}

//isInSet determines if a value is in the set
func isInSet(needle int, haystack *set) bool {
	for i := 0; i < len(haystack.myItems); i++ {
		if needle == haystack.myItems[i] {
			return true
		}
	}
	return false
}

//countItems return the Count of Items in a Set
func countItems(S *set) int {
	return len(S.myItems)
}

/*
	Prim's Algorithm for MST
	Returns a new Graph with only the MST edges
*/
func Prim(G Graph, start int) Graph {
	//Create an Empty Graph to store results
	var MST *AdjMatrix = CreateAdjMatrix()
	//Start with all infinity values except diagonal
	MST.EmptyGraph(G.NumberNodes())
	//Create a Priority Queue to Manage Edges
	var PQ *edgeQueue = createEdgePQ()
	//Use a Set to track our nodes
	var U *set = newSet()
	//Initialize with Start Node
	addToSet(start, U)
	//Add Edges to Priority Queue that are adjacent to start node
	pushAdjEdges(PQ, G, start)
	//While we have not added all nodes to the set
	for countItems(U) != G.NumberNodes() {
		//Get the edge with the smallest weight
		var e edge = minEdge(PQ)
		//If the to node is not already in our tree
		if !isInSet(e.to, U) {
			//Add edge to the MST Set
			addToSet(e.to, U)
			//Add Edge to the MST
			MST.AddEdge(e.from, e.to, e.w)
			//Add both ways since Undirected
			MST.AddEdge(e.to, e.from, e.w)
			//Update the Heap with newly found edges
			pushAdjEdges(PQ, G, e.to)
		}
	}
	//Return the MST Graph
	return MST
}
