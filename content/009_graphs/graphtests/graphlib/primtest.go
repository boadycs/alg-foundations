//Mark Boady
//Drexel 2021
//Test Prim's Algorithm

package graphlib

import "fmt"

//Test the Algorithm on Input Files
func PrimTest() {
	fmt.Println("Testing Prim's Algorithm")
	//Files to Test
	var files []string = []string{
		"tests/undir_input1.txt",
		"tests/undir_input2.txt",
		"tests/undir_input3.txt",
	}
	//Loop over files
	for i := 0; i < len(files); i++ {
		var filename string = files[i]
		var G *AdjMatrix = &AdjMatrix{}
		fmt.Println("Matrix for", filename)
		G.LoadGraphUndirected(filename)
		printGraph(G)
		for j := 0; j < G.NumberNodes(); j++ {
			fmt.Println("MST Starting at", j)
			var Res Graph = Prim(G, j)
			printGraph(Res)
		}
	}
}

/* Expected Output:
Testing Prim's Algorithm
Matrix for tests/undir_input1.txt
   0    8    1    5  inf  inf
   8    0    7  inf    3  inf
   1    7    0    6    9    4
   5  inf    6    0  inf    2
 inf    3    9  inf    0   10
 inf  inf    4    2   10    0
MST Starting at 0
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
MST Starting at 1
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
MST Starting at 2
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
MST Starting at 3
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
MST Starting at 4
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
MST Starting at 5
   0  inf    1  inf  inf  inf
 inf    0    7  inf    3  inf
   1    7    0  inf  inf    4
 inf  inf  inf    0  inf    2
 inf    3  inf  inf    0  inf
 inf  inf    4    2  inf    0
Matrix for tests/undir_input2.txt
   0    4  inf  inf  inf  inf  inf   10  inf
   4    0    8  inf  inf  inf  inf   13  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11   14  inf  inf  inf
 inf  inf  inf   11    0   12  inf  inf  inf
 inf  inf    5   14   12    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1    6
  10   13  inf  inf  inf  inf    1    0    9
 inf  inf    3  inf  inf  inf    6    9    0
MST Starting at 0
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 1
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 2
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 3
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 4
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 5
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 6
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 7
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
MST Starting at 8
   0    4  inf  inf  inf  inf  inf  inf  inf
   4    0    8  inf  inf  inf  inf  inf  inf
 inf    8    0    7  inf    5  inf  inf    3
 inf  inf    7    0   11  inf  inf  inf  inf
 inf  inf  inf   11    0  inf  inf  inf  inf
 inf  inf    5  inf  inf    0    2  inf  inf
 inf  inf  inf  inf  inf    2    0    1  inf
 inf  inf  inf  inf  inf  inf    1    0  inf
 inf  inf    3  inf  inf  inf  inf  inf    0
Matrix for tests/undir_input3.txt
   0  inf   26  inf   38  inf   58   16
 inf    0   36   29  inf   32  inf   19
  26   36    0   17  inf  inf   40   34
 inf   29   17    0  inf  inf   52  inf
  38  inf  inf  inf    0   35   93   37
 inf   32  inf  inf   35    0  inf   28
  58  inf   40   52   93  inf    0  inf
  16   19   34  inf   37   28  inf    0
MST Starting at 0
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 1
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 2
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 3
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 4
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 5
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 6
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
MST Starting at 7
   0  inf   26  inf  inf  inf  inf   16
 inf    0  inf  inf  inf  inf  inf   19
  26  inf    0   17  inf  inf   40  inf
 inf  inf   17    0  inf  inf  inf  inf
 inf  inf  inf  inf    0   35  inf  inf
 inf  inf  inf  inf   35    0  inf   28
 inf  inf   40  inf  inf  inf    0  inf
  16   19  inf  inf  inf   28  inf    0
*/
