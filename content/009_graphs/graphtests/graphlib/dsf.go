//Mark Boady
//Drexel University 2021

package graphlib

//Local Helper Functions

//Make a Matrix
func makeMatrix(rows int, cols int) [][]int {
	var matrix [][]int = make([][]int, rows)
	matrix[0] = makeArray(cols)
	matrix[1] = makeArray(cols)
	return matrix
}

//Make an array
func makeArray(rows int) []int {
	return make([]int, rows)
}

//DFS runs a Depth First Search on G starting at node start
//Returns a 2D Array where
// M[0][x] is the found counter for x
// and M[1][x] is the finished counter for x
func DFS(G Graph, start int) [][]int {
	//How many nodes are in the graph
	var numNodes int = G.NumberNodes()
	//Make a matrix with 2 rows and numNodes Columns
	var data [][]int = makeMatrix(2, numNodes)
	//Run DFS for all nodes, starting at start
	dfsAll(G, start, data)
	//Return the counters
	return data
}

//Run DFS on all nodes
func dfsAll(G Graph, start int, data [][]int) {
	//Determine how many nodes
	var numNodes int = G.NumberNodes()
	//The counter starts at 1
	var counter int = 1
	//Use mod to cycle from start
	for i := 0; i < numNodes; i++ {
		var currentNode int = (start + i) % numNodes
		counter = dfsRecursive(G, currentNode, data, counter)
	}
}

//Recursive Implementation of DFS
//This is the real algorithm
func dfsRecursive(G Graph, v int, data [][]int, counter int) int {
	//Have we already been here?
	if data[0][v] > 0 {
		return counter //Already Marked
	}
	//Mark that we have visited this node
	data[0][v] = counter
	//Increase counter
	counter += 1
	//Get Adjacent Nodes
	var adjList []int = G.AdjacentTo(v)
	//Loop over adjacent nodes
	for i := 0; i < len(adjList); i++ {
		//Get the Node ID
		var w int = adjList[i]
		if data[0][w] == 0 {
			counter = dfsRecursive(G, w, data, counter)
		}
	}
	//Set Finished counter
	counter += 1
	data[1][v] = counter
	//Return next number to use
	return counter + 1
}
