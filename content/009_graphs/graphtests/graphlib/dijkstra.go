//Mark Boady
//Drexel University 2021

package graphlib

import (
	"container/heap"
)

/*
	Dijkstra Priority Queue (Heap)
	A heap of pairs (weight, node)
	that can be used to find the next closest
	node in the distance array in O(log2(n)) time.

	This is just standard GO template except Less which is custom
*/

//distancePair is a pair (distance, node) to store in heap
type distancePair struct {
	distance int
	nodeID   int
}

//distanceQueue is a implementation of Priority Queue for our pairs
type distanceQueue []*distancePair

//Len returns the number of values in the Priority Queue
func (pq distanceQueue) Len() int { return len(pq) }

//Less is used to determine the smallest value, uses distance
//breaks ties on nodeID
func (pq distanceQueue) Less(i, j int) bool {
	if pq[i].distance == pq[j].distance {
		return pq[i].nodeID < pq[j].nodeID
	}
	return pq[i].distance < pq[j].distance
}

//Pop removes the last item from the Priority Queue
func (pq *distanceQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

//Push adds a new item to the Priority Queue
func (pq *distanceQueue) Push(x interface{}) {
	item := x.(*distancePair)
	*pq = append(*pq, item)
}

//Swap trades to values
func (pq distanceQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

/*
	Priority Queue Helpers
	To make the code easier to read
	we use additional functions to manage the Priority Queue
*/

//createPiorityQueue takes a distance array and makes a Priority Queue
func createPiorityQueue(Array []int) *distanceQueue {
	var PQ distanceQueue = make(distanceQueue, 0)
	for i := 0; i < len(Array); i++ {
		var P *distancePair = &distancePair{Array[i], i}
		heap.Push(&PQ, P)
	}
	return &PQ
}

//minNode gets the NodeID of the minimum value in the Priority Queue
func minNode(PQ *distanceQueue) int {
	var pair *distancePair = heap.Pop(PQ).(*distancePair)
	return pair.nodeID
}

//addNode adds a new pair (distance, node) to the Priority Queue
func addNode(PQ *distanceQueue, nodeID int, distance int) {
	var P *distancePair = &distancePair{distance, nodeID}
	heap.Push(PQ, P)
}

/*
	Dijkstra's Algorithm
	Compute the Shortest Path to All Nodes
	Does not find the paths in this version
*/

//Dikstra is an implementation of Dijkstra's Shortest Path Algorithm
func Dijkstra(G Graph, startNode int) []int {
	//Create the Distance Array
	var D []int = newArray(G.NumberNodes())
	//Set all values to infinity
	fillArray(D, G.GetInfinity())
	//Set start node to 0
	D[startNode] = 0
	//Create a Priority Queue
	var PQ *distanceQueue = createPiorityQueue(D)
	//Number of Nodes we found path to so far
	var nodesDone int = 0
	//We find one path per loop
	for nodesDone < len(D) {
		//Determine the Minimum Node in the Heap
		var u int = minNode(PQ)
		//Determine which nodes are Adjacent
		var adjList []int = G.AdjacentTo(u)
		//For each Adjacent Node, relax
		for i := 0; i < len(adjList); i++ {
			var v int = adjList[i]
			//Relax the Node
			if D[v] > D[u]+G.Weight(u, v) {
				D[v] = D[u] + G.Weight(u, v)
				//Update the Heap on Changes
				addNode(PQ, v, D[v])
			}
		}
		//Count that we found a node
		nodesDone += 1
	}
	return D
}
