//Mark Boady
//Drexel 2021
//Graph algorithms

//Front end to run tests
package main

import "graphtests/graphlib"

func main() {
	graphlib.DfsTest()
	/*graphlib.DijkstraTest()
	graphlib.PrimTest()
	graphlib.KruskalTest()*/
}
