#include "adjmatrix.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

Graph* emptyGraph(int nodes){
	//Make an array of pointers
	int** pointers = malloc(nodes*sizeof(int*));
	//For each position put an array
	for(int i=0; i < nodes; i++){
		pointers[i]=malloc(nodes*sizeof(int));
	}
	//We now have enough space
	//Fill with infinity in all locations
	for(int i=0; i < nodes; i++){
		for(int j=0; j < nodes; j++){
			pointers[i][j]=getInfinity();
		}
	}
	//Put zero on diagonal
	for(int i=0; i < nodes; i++){
		pointers[i][i]=0;
	}
	//Update Data Stucture
	Graph* G = malloc(sizeof(Graph));
	G->M = pointers;
	G->nodes = nodes;
	return G;
}

void addEdge(Graph* G, int from, int to, int weight){
	G->M[from][to]=weight;
}

int getInfinity(){
	return INT_MAX;//From limits.h
}

int numberNodes(Graph* G){
	return G->nodes;
}

int weight(Graph* G, int x, int y){
	return G->M[x][y];
}

nodeList* adjacentTo(Graph* G, int x){
	//Create a new array
	int* adjacent = malloc(G->nodes*sizeof(int));
	//Only Copy in non-infinity values
	//Also skip self
	int pos=0;
	for(int i=0; i < G->nodes; i++){
		if(weight(G,x,i)!=getInfinity() && i!=x){
			adjacent[pos]=i;
			pos++;
		}
	}
	//Made a nodeList
	nodeList* nl = malloc(sizeof(nodeList));
	nl->nodes = adjacent;
	nl->count = pos;
	return nl;
}

void delete(Graph* G){
	for(int i=0; i < G->nodes; i++){
		free(G->M[i]);
	}
	free(G->M);
	free(G);
}
