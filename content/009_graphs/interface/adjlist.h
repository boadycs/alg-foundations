#ifndef _ADJ_LIST_H_
#define _ADJ_LIST_H_

//We need a limited linked list structure
typedef struct LinkedList LinkedList;
typedef struct EdgeNode EdgeNode;
struct LinkedList{
	EdgeNode* head;
};
struct EdgeNode{
	int from;
	int to;
	int weight;
	EdgeNode* next;
};

//Graph Structure
typedef struct AdjList Graph;
struct AdjList{
	//Array of pointers to edge nodes
	LinkedList** edgeInfo;
	int nodes;
};

//An array of nodes paired with
//its size
typedef struct nodeList nodeList;
struct nodeList{
	int* nodes;
	int count;
};

//Create An Empty Graph
Graph* emptyGraph(int nodes);

//Add an Edge to the Graph
void addEdge(Graph* G, int from, int to, int weight);

//What number represents Infinity in the graph?
int getInfinity();

//How many nodes are in the graph?
int numberNodes(Graph* G);

//What is the weight of edge (x,y)?
int weight(Graph* G, int x, int y);

//What nodes are Adjacent to node x?
nodeList* adjacentTo(Graph* G, int x);

//Clear Memory
void delete(Graph* G);

//Linked List Helpers
LinkedList* makeLL();
//Add to front
void addTo(LinkedList* L,int from, int to, int weight);

#endif
