#include "adjlist.h"
#include "stdlib.h"
#include "stdio.h"
#include "limits.h"

Graph* emptyGraph(int nodes){
	//Make Struct
	Graph* G = malloc(sizeof(Graph));
	//Set Node Count
	G->nodes = nodes;
	//Create Array
	G->edgeInfo = malloc(nodes* sizeof(LinkedList*));
	//Set all positions to null
	for(int i=0; i < nodes; i++){
		G->edgeInfo[i] = makeLL();
	}
	return G;
}

void addEdge(Graph* G, int from, int to, int weight){
	LinkedList* L = G->edgeInfo[from];
	addTo(L,from,to,weight);
}

int getInfinity(){
	return INT_MAX;
}

int numberNodes(Graph* G){
	return G->nodes;
}

int weight(Graph* G, int x, int y){
	//We can always reach ourselves
	if(x==y){return 0;}
	//We need to find it!
	LinkedList* L = G->edgeInfo[x];
	//Loop over the list
	EdgeNode* node = L->head;
	while(node!=NULL){
		if(node->to == y){
			return node->weight;
		}
		node = node->next;
	}
	//Never Found!
	return getInfinity();
}

nodeList* adjacentTo(Graph* G, int x){
	//Create a new array
	int* adjacent = malloc(G->nodes*sizeof(int));
	//Only Copy in non-infinity values
	//Also skip self
	int pos=0;
	for(int i=0; i < G->nodes; i++){
		if(weight(G,x,i)!=getInfinity() && i!=x){
			adjacent[pos]=i;
			pos++;
		}
	}
	//Made a nodeList
	nodeList* nl = malloc(sizeof(nodeList));
	nl->nodes = adjacent;
	nl->count = pos;
	return nl;
}

void delete(Graph* G){
	//Loop over every linked list
	for(int i=0; i < G->nodes; i++){
		//Loop over every node
		EdgeNode* n = G->edgeInfo[i]->head;
		while(n!=NULL){
			EdgeNode* temp = n;
			n = n->next;
			free(temp);
		}
		free(G->edgeInfo[i]);
	}
	//Free the Graph
	free(G);
}

//Linked List Helpers
LinkedList* makeLL(){
	LinkedList * L = malloc(sizeof(LinkedList));
	L->head = NULL;
	return L;
}
//Put this node in front of the list
void addTo(LinkedList* L,int from, int to, int weight){
	//See if already here overwrite
	EdgeNode* n = L->head;
	while(n!=NULL){
		if(n->to ==to && n->from==from){
			n->weight=weight;
			return;
		}
		n=n->next;
	}
	//Insert
	EdgeNode* N = malloc(sizeof(EdgeNode));
	N->from = from;
	N->to = to;
	N->weight = weight;
	N->next = L->head;
	L->head=N;
}
