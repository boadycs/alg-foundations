#ifndef _ADJ_MATRIX_H_
#define _ADJ_MATRIX_H_

typedef struct AdjMatrix Graph;
struct AdjMatrix{
	int** M;//Matrix to store weights
	int nodes;//The number of nodes
};

//An array of nodes paired with
//its size
typedef struct nodeList nodeList;
struct nodeList{
	int* nodes;
	int count;
};

//Create An Empty Graph
Graph* emptyGraph(int nodes);

//Add an Edge to the Graph
void addEdge(Graph* G, int from, int to, int weight);

//What number represents Infinity in the graph?
int getInfinity();

//How many nodes are in the graph?
int numberNodes(Graph* G);

//What is the weight of edge (x,y)?
int weight(Graph* G, int x, int y);

//What nodes are Adjacent to node x?
nodeList* adjacentTo(Graph* G, int x);

//Clear Memory
void delete(Graph* G);

#endif
