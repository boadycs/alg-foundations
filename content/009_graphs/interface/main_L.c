#include <stdlib.h>
#include <stdio.h>
//We can do the exact same things with
//An Adjacency Matrix
//#include "adjmatrix.h"
//or
//An Adjacency List
#include "adjlist.h"
/*
 To replicate the test, the input is given below:
 
5
7
0 1 10
0 2 30
0 4 100
1 2 50
3 2 20
3 4 60
2 4 10
 
 */

int readInt(){
	char c = getchar();
	int total=0;
	while(c != EOF && c != '\n' && c!=' '){
		total = total*10 + c-48;
		c = getchar();
	}
	return total;
}


int main(int argc, char** argv){
	//Ask for the number of nodes
	printf("How many nodes in the graph? \n");
	int countNodes = readInt();
	printf("Graph has %d nodes\n",countNodes);
	//Make the Graph
	Graph* G = emptyGraph(countNodes);
	//Ask Number of Edges
	printf("How Many Edges do you want to add?\n");
	int numEdges = readInt();
	printf("Enter %d Edges as from to weight:\n",numEdges);
	for(int i=0; i < numEdges; i++){
		int from = readInt();
		int to = readInt();
		int weight = readInt();
		addEdge(G,from,to,weight);
	}
	//Ok Lets test the graph
	//Try printing out all the edges
	for(int i=0; i < numberNodes(G); i++){
		for(int j=0; j < numberNodes(G); j++){
			if(weight(G,i,j)==getInfinity()){
				printf("Edge from %d to %d has weight infinity\n",i,j);
			}else{
				printf("Edge from %d to %d has weight %d\n",i,j,weight(G,i,j));
			}
		}
	}
	//Check that the Adjacent List is correct
	for(int i=0; i < numberNodes(G); i++){
		nodeList* A = adjacentTo(G,i);
		printf("Nodes Adjacent to %d [",i);
		for(int j=0; j < A->count; j++){
			printf("%d",A->nodes[j]);
			if(j+1<A->count){
				printf(", ");
			}
		}
		printf("]\n");
		free(A->nodes);
		free(A);
	}
	//Free Memory
	delete(G);
	return 0;
}
