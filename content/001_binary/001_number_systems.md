# Number Systems

Numbers can be represented in a variety of ways. For as long as humans have needed to keep track of numbers, we have developed abstract representations of those values. Examples of three very common representations are shown below.

| Representation | Description |
| ---- | ---- |
| 10 | Integer Number System (Base 10) |
| Ten | English Spelling | 
| X | Roman Numerals |

You are probably very familiar with the **Integer Number System**. This number system has a lot of advantages for humans completing calculations. We will look at two number systems, **Binary Numbers** and **Hexadecimal Numbers**. These number systems are used by computers. They provide system that allows us to build computers and solve problems.

First, we will examine the **Integer Number System** in greater detail. This number system is used for representing whole numbers. It is built from a set of ten symbols called **digits**.

| Digit | Description |
| ----- | ------------- |
| 0 | Zero |
| 1 | One |
| 2 | Two |
| 3 | Three |
| 4 | Four |
| 5 | Five |
| 6 | Six |
| 7 | Seven |
| 8 | Eight |
| 9 | Nine |

By placing two of these digits next to each other, we can create new representations. This allows for the representation of unlimited numbers. For example, 10 is the one digit followed by the zero digit.  We would read this as Ten. 

How do we determine the meaning of a sequence of digits? The position where the digit is written provides additional information about its value. Let us examine the three digit number 238.

| Hundreds Place | Tens Place | Ones Place |
| --- | --- | --- |
| 2 | 3 | 8 |

We read the last digit, 8, exactly as it is. The middle digit, 3, is read as thirty based on its location. The first digit, 2, is read as two hundred based on its location. 

This representation of numbers is built from two component parts. The **digits** are the symbols we write to represent numbers. There is also a **base**. The **base** is the smallest number that cannot be written with a single digit. In our case, ten is the smallest number that cannot be written without using two digits. To make it clear that the **base** is special in our system, we will write it with a special symbol for now. We will use the symbol $X$ as our placeholder for the **base**. This shows the general pattern for any base.

When we look at the number 238, each digit's position tells us what exponent to put on the base. 

An exponent is a mathematical operation where a number is multiplied by itself a certain number of times. For example, $3^5$ means multiply three by itself five times.

$
\begin{align}
    3^5 =& 3*3*3*3*3=243
\end{align}
$

Any number taken to the exponent $0$ is equal to 1. A number multiplied by itself zero times gives us 1.

$
\begin{align}
    3^0 =& 1
\end{align}
$

The position of each digit in the integer 238 tells us what exponent should be used on the base.

$
\begin{align}
    238 =& 2*X^2 + 3*X^1 + 8*X^0
\end{align}
$

Since we are using the same base ten number system on both sides, this expression simplifies to itself.

$
\begin{align}
    238 =& 2*X^2 + 3*X^1 + 8*X^0 \\
        =& 2*10^2 + 3*10^1 + 8*10^0 \\
        =& 2*100 + 3*10 + 8*1 \\
        =& 200 + 30 + 8 \\
        =& 238
\end{align}
$

What if we selected a different base? Then our numbers would be written differently. If it is not obvious what base a number is written in, a subscript will be used to clarify. For example, $238_{10}$ means this number is base 10.

What happens if we wanted to choose a different base? We will make numbers with a base of $5$ instead of 10. We first need digits up to, but not including, the base.

| Digit | Description |
| ---- | ---- |
| 0 | Zero |
| 1 | One |
| 2 | Two |
| 3 | Three |
| 4 | Four |

We can again write larger numbers by writing digits next to each other. What does writing $231_{5}$ mean? We can convert it into a base ten number.

$
\begin{align}
231_{5} =& 2*X^{2} + 3*X^{1} + 1*X^{0} \\
    =& 2*5^2 + 3*5^1 + 1*5^0 \\
    =& 2*25 + 3*5 + 1*1 \\
    =& 50 + 15 + 1 \\
    =& 66_{10}
\end{align}
$

The digits 231 can mean different things depending on what **base** is selected. In this case, we saw that $231_{5}$ is representing the same number as $66_{10}$. 

We have just seen how to convert a number into a base ten number. How can we convert a base ten number into a different base? This requires repeated integer division.

The division of two integers does not always give us an integer. The fraction $\frac{4}{2}=2$ divides evenly. The fraction $\frac{9}{2}=4.5$ does not divide evenly. In this case, the answer was written as a decimal number, but that changed the representation! We are no longer writing base ten integers. 

Another way to think of division is that each division gives a quotient and a remainder. Both the quotient and the remainder will be whole numbers. 

For any two integers $\frac{a}{b}$ it is true that

$
\begin{align}
    a=& q*b+r
\end{align}
$

For example, we saw that $\frac{9}{2}=4.5$. We can also think of this division as $9=4*2+1$. The quotient is $4$ and the remainder is $1$. The $0.5$ comes from $\frac{1}{2}$.

$
\begin{align}
    9=& 4*2+1 \\
    \frac{9}{2} =& \frac{4 * 2 + 1}{2} \\
    \frac{9}{2} =& \frac{4 * 2}{2} + \frac{1}{2} \\
    \frac{9}{2} =& 4 + \frac{1}{2} \\
    \frac{9}{2} =& 4.5
\end{align}
$

When we want to talk about integers, we need to split up the quotient and the remainder. 

$
\begin{align}
    a / b =& \text{Quotient} \\
    a \mod b =& \text{Remainder} \\
\end{align} 
$

```{note}
We use different operations depending on which number we need. The division operator will **always** be treated as giving the integer quotient. If the decimal number is needed, a special note will be given. 
```

Another way to think about $\frac{9}{2}=4.5$ is

$
\begin{align}
    9 / 2 =& 4 \\
    9 \mod 2 =& 1
\end{align}
$

```{note}
May programming languages use % for the remainder. We will use the term mod because it has a universally recognized mathematical meaning.
```

When we try to divide 9 items into sets of size 2, we can successfully make $4$ sets and we are left with $1$ extra. 

What happens when we divide $66$ into sets of size $5$?

$
\begin{align}
    66 / 5 =& 13 \\
    66 \mod 5 =& 1
\end{align}
$

The first remainder computed is 1. The quotient is 13. That quotient is still larger than $4$, we can't write it as a base-5 number. 

$
\begin{align}
    13 / 5 =& 2 \\
    13 \mod 5 =& 3
\end{align}
$

The second remainder found is $3$. The quotient is $2$. This quotient is less than $5$, which means we can write it as a base-5 digit.

We found the remainders 1 then 3 and were left with quotient 2. If we put these together and reverse the order we get $231_{5}=66_{10}$. 

We can now create different number representations and change between them. 

