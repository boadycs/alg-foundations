#Author: Mark Boady
#Date: 2/2/2021
#Drexel University

#Python Implementation of Euclid's Algorithm for GCD
#Assumes int(a) >= 0 and int(b) >= 0

def gcd(a,b):
    if b==0:
        return a
    else:
        return gcd(b, a%b)

#Ask for an int that is greater than equal to 0
def askForInput(question):
    try:
        x=input(question)
        x=int(x)
        if x >= 0:
            return x
        print("Negative Numbers not allowed.")
        return askForInput(question)
    except ValueError:
        print("Only Positive Integers allowed.")
        return askForInput(question)

#Manage the primary input/output
def main():
    print("Welcome to GCD Example")
    a=askForInput("Enter First Positive Integer: ")
    b=askForInput("Enter Second Positive Integer: ")
    result=gcd(a,b)
    outputText="The GCD of {:d} and {:d} is {:d}"
    print(outputText.format(a,b,result));

#Python executes this code when
#run directly.
if __name__=="__main__":
    main()
