package main

/*
   Author: Mark Boady
   Date: 2/2/2021
   Drexel University

   Go Implementation of Euclid's Algorithm
*/

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//Function Definitions

//The Euclidean Algorithm
//Assumes a and b are positive integers
func gcd(a int, b int) int {
	if b == 0 {
		return a
	}
	return gcd(b, a%b)
}

//Ask for user input
//Repeat the question until we get a positive int
func askForInput(question string) int {
	//Print the question
	fmt.Print(question)
	//Read Input
	reader := bufio.NewReader(os.Stdin)
	text, err := reader.ReadString('\n')
	//Make sure read happened (fatal error)
	if err != nil {
		fmt.Println("Could not parse input.")
		return 0
	}
	//Remove trailing newline
	text = strings.TrimSuffix(text, "\n")
	//Convert to Integer
	userInput, err := strconv.Atoi(text)
	if err != nil {
		fmt.Println("Only Positive Integers allowed.")
		return askForInput(question)
	}
	//Make sure positive
	if userInput < 0 {
		fmt.Println("Negative Numbers not allowed.")
		return askForInput(question)
	}
	//It worked!
	return userInput
}

//Main Function of the program
func main() {
	fmt.Println("Welcome to GCD Example")
	var a int = askForInput("Enter First Positive Integer: ")
	var b int = askForInput("Enter Second Positive Integer: ")
	var result int = gcd(a, b)
	fmt.Printf("The GCD of %d and %d is %d\n", a, b, result)
	return
}
