/*
    Author: Mark Boady
    Date: 2/2/2021
    Drexel University
    
    C++ Implementation of Euclid's Algorithm
 */
 #include <iostream>
 #include <string>
 #include<limits>
 
 //Function Templates
 
 //Assumes a and b are positive integers
 int gcd(int a, int b);
 
 //Function for asking for input
 int askForInput(std::string question);
 
 //Main Function of the program
 int main(){
    std::cout << "Welcome to GCD Example" << std::endl;
    int a = askForInput("Enter First Positive Integer: ");
    int b = askForInput("Enter Second Positive Integer: ");
    int result=gcd(a,b);
    std::cout << "The GCD of " << a << " and " << b;
    std::cout << " is " << result << std::endl;
    return 0;
 }

//Functions are defined below main

//The Euclidean Algorithm
int gcd(int a, int b)
{
    if(b==0)
    {
        return a;
    }else{
        return gcd(b, a%b);
    }
}

//Ask for user input
//Repeat the question until we get a positive int
int askForInput(std::string question)
{
    //Print the question
    std::cout << question;
    int userInput;
    std::cin >> userInput;
    //See if we succeeded
    if(std::cin.fail())
    {
        //Clear the stream for the next input
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        //Print Error
        std::cout << "Only Positive Integers allowed." << std::endl;
        //Ask Again
        return askForInput(question);
    }
    //Negative Number Error
    if(userInput < 0)
    {
        //Print Error
        std::cout << "Negative Numbers not allowed." << std::endl;
        //Ask Again
        return askForInput(question);
    }
    //It worked!
    return userInput;
}

