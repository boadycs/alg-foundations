#include <iostream>

int fastPower(int a, int b, int n){
    if(b==0)
        return 1;
    if((b & 1)==0)
    {
        int x = fastPower(a,b>>1,n);
        x=x%n;
        return (x*x)%n;
    }
    return (a*fastPower(a,b-1,n))%n;
}

int main(){
    std::cout << "Experiment: " << fastPower(987,123,10000) << std::endl;
}
