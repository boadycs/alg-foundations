package main

import (
	"fmt"
	"math/big"
	"math/rand"
	"time"
)

//Compute Mod Exponent using a loop
func modPower(a int, b int, n int) int {
	var temp int = 1
	for b > 0 {
		temp = (temp * a) % n
		b = b - 1
	}
	return temp
}

func main() {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 100; i++ {
		var x int = rand.Intn(25)
		var y int = rand.Intn(25)
		var n int = rand.Intn(25) + 1

		var mine int = modPower(x, y, n)

		a := big.NewInt(int64(x))
		b := big.NewInt(int64(y))
		m := big.NewInt(int64(n))
		r := big.NewInt(0)
		r.Exp(a, b, m)
		q := big.NewInt(int64(mine))
		if q.Cmp(r) != 0 {
			fmt.Println(q)
			fmt.Println(r)
			fmt.Println()
		}
	}
	fmt.Println("The End. :-)")
}
