#include "stdlib.h"
#include "stdio.h"

//Hi I am a comment
/*
	I am a long comment
*/

//Function prototype
int factorial(int n);
//Function Definition
int factorial(int n){
	if(n==0){
		return 1;
	}else{
		return n*factorial(n-1);
	}
}

struct Person{
	char* firstName;
	char* lastName;
	char* email;
};

int main(int argc, char** argv){

	int* primes = malloc(6*sizeof(int));
	primes[0]=2;
	primes[1]=3;
	primes[2]=5;
	primes[3]=7;
	primes[4]=11;
	primes[5]=13;
	for(int k = 0; k < 6; k++){
		printf("%d\n",primes[k]);
	}
	free(primes);
	
	int value = 22;
	if(value%2==0){
		printf("I am even\n");
	}else{
		printf("I am odd.\n");
	}
	
	char c = getchar();
	printf("Character was %c\n",c);
	
	printf("Fact(5): %d\n",factorial(5));
	
	int total = 0;
	int i = 0;
	while( i < 10 )
	{
		total = total + (2*i + 1);
		i = i + 1;
	}
	
	int num = 17;
	float dec = 25.37;
	char* word = "Example";
	
	num = num + 10; //Addition
	num = num - 2;  //Subtraction
	num = num / 10; //Division (normally integer by default)
	num = num * 2;  //Multiplication
	num = num % 5;  //Remainder
	
	int* myValue = malloc(sizeof(int));
	*myValue = 9;
	printf("Pointer: %p\n",myValue);
	printf("Value: %d\n",*myValue);
	free(myValue);
	
	
	return 1;
}
