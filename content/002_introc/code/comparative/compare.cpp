#include <iostream>
#include <string>
using namespace std;

int factorial(int n){
    if(n == 0){
        return 1;
    } else {
        return n * factorial(n-1);
    }
}

//Person is a struct that store info about a person.
struct Person {
    string firstName;
    string lastName;
    string email;
};

int main() {
    //Create an integer, float, and string
    int num = 17;
    float dec = 25.37;
    string word = "Example";
    cout << num << endl;
    cout << dec << endl;
    cout << word << endl;

    num = num + 10; //Addition
    num = num - 2;  //Subtraction
    num = num / 10; //Division (normally integer by default)
    num = num * 2;  //Multiplication
    num = num % 5;  //Remainder
    cout << num << endl;

    //Sum up 10 odd numbers
    int total = 0;
    int i = 0;
    while( i < 10 )
    {
        total = total + (2*i + 1);
        i = i + 1;
    }
    cout << total << endl;

    int value = 22;
    if(value%2 == 0){
        cout << "I am even." << endl;
    } else {
        cout << "I am odd." << endl;
    }

    cout << factorial(5) << endl;

    int* primes = new int[6];
    primes[0]=2;
    primes[1]=3;
    primes[2]=5;
    primes[3]=7;
    primes[4]=11;
    primes[5]=13;
    for(int k = 0; k < 6; k++){
        cout << primes[k] << endl;
    }

    int myValue = 10;
    int* myValuePointer = &myValue;
    cout << myValuePointer << endl;

    Person *p = new Person();
    p->firstName = "Mark";
    cout << p->firstName << endl;

    
    string text;
    cout << "Hello! What is your name?" << endl;
    cin >> text;
    cout << text << endl;
}

