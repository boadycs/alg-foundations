;Compute the remainder
JMP start
start:
	PUSH 9 ;Put into last memory add
	PUSH 2 ;Put into next to last memory
	CALL rem ;Run the function
	HLT
rem:
	;Make Backups
	PUSH A ;Back up register A
	PUSH B ;Back up register B
	;Load Inputs
	MOV A, [SP+5] ;regA= 9
	MOV B, [SP+4] ;regB = 2
	DIV B ;regA = regA/regB (q in alg)
	MUL B ;regB = q*b in alg
	MOV B, [SP+5] ; We need to remember 9
	SUB B,A ; RegB=a-q*b (The remainder)
	;Return Value
	MOV [SP+5],B
	;Restore Backups
	POP B
	POP A
	RET

