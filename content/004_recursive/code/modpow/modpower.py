#Mark Boady
#Fast Powering

#Computes a^b
#Integer math only
#Only considering cases where int(a), int(b), and b>0
def fastPower(a,b,n):
    if b==0:
        return 1
    if (b & 1)==0:
        x=fastPower(a,b>>1,n)
        x=x%n
        return (x*x)%n
    return (a*fastPower(a,b-1,n))%n
    
import random
import time
if __name__=="__main__":
    total=100
    failed=0
    print("Testing Fast Power with",total,"tests.")
    for x in range(0,total):
        x=random.randint(0,100000)
        y=random.randint(0,100000)
        z=100000
        T1=fastPower(x,y,z)
        T2=pow(x,y,z)
        if T1!=T2:
            failed+=1
            print("Inputs:",x,y,z)
    print("Passed:",total-failed,"tests.")
    print("Experiment:",fastPower(987,123,10000))
