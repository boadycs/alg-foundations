
#include "stdlib.h"
#include "stdio.h"
#include "time.h"

int fastPower(int a, int b, int n){
	if(b==0){
		return 1;
	}
	if((b & 1) == 0){
		int x = fastPower(a,b>>1,n);
		x = x % n;
		return (x*x) % n;
	}
	return (a*fastPower(a,b-1,n))%n;
}

int naivePower(int a, int b, int n){
	int temp = 1;
	while( b > 0){
		temp = (temp * a) % n;
		b = b - 1;
	}
	return temp;
}

int main(int argc, char** argv){
	//Tests
	int modulo = 9;
	for(int i=0; i < 20; i++){
		for(int j=0; j < 20; j++){
			int v1 = naivePower(i,j,modulo);
			int v2 = naivePower(i,j,modulo);
			if(v1!=v2){
				printf("FAILED: %d^%d mod %d\n",i,j,modulo);
				return 1;
			}
		}
	}
	printf("Passed Tests.\n");
	
	int experiment = fastPower(987,123,10000);
	printf("Experiment: %d\n",experiment);
	return 0;
}
