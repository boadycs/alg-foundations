package main

import (
	"fmt"
	"math/big"
	"math/rand"
	"time"
)

func fastPower(a int, b int, n int) int {
	if b == 0 {
		return 1
	}
	if (b&1) == 0 {
		var x int = fastPower(a, b>>1, n)
		x = x % n
		return (x * x) % n
	}
	return (a * fastPower(a, b-1, n)) % n
}

func main() {
	var digits int = 10000
	var total int = 100
	var failed int = 0

	rand.Seed(time.Now().UnixNano())

	for i := 0; i < total; i++ {

		var x int = rand.Intn(90000)
		var y int = rand.Intn(90000)

		//Built In Version
		a := big.NewInt(int64(x))
		b := big.NewInt(int64(y))
		m := big.NewInt(int64(digits))
		r := big.NewInt(40)
		r.Exp(a, b, m)

		var mine int = fastPower(x, y, digits)
		q := big.NewInt(int64(mine))
		if q.Cmp(r) != 0 {
			fmt.Println("Failure:", x, y, digits)
			fmt.Println("Expected:", r)
			fmt.Println("Provided:", q)
			failed++
		}
	}
	fmt.Println("Passed", (total - failed), "out of", total)
	fmt.Println("Experiment:", fastPower(987, 123, 10000))
}
