import java.math.BigInteger;
import java.util.Random;

class modpower {
    public static int fastPower(int a, int b, int n)
    {
        if(b==0){
            return 1;
        }
        if((b&1)==0){
            int x = fastPower(a,b>>1,n);
            x=x%n;
            return (x*x)%n;
        }
        return (a*fastPower(a,b-1,n))%n;
    }
    public static void main(String[] argv)
    {
        Random rand = new Random();
        int digits=10000;
        int total=100;
        int failed=0;

        for(int i=0; i < total; i++)
        {
            int x=rand.nextInt(9999);
            int y=rand.nextInt(9999);
            //Built In Version
            BigInteger a = new BigInteger(String.valueOf(x));
            BigInteger b = new BigInteger(String.valueOf(y));
            BigInteger m = new BigInteger(String.valueOf(digits));
            BigInteger r = a.modPow(b, m);
            //Mine
            int mine = fastPower(x,y,digits);
            BigInteger q = new BigInteger(String.valueOf(mine));

            if(r.compareTo(q)!=0){
                failed++;
                System.out.println(r);
                System.out.println(q);
            }

        }
        System.out.print("Passed: ");
        System.out.print(total-failed);
        System.out.print(" out of ");
        System.out.println(total);

        System.out.println();
        System.out.println("Experiment: ");
        System.out.println(fastPower(987,123,10000));
    }
}