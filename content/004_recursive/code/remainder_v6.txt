;Print A Number
JMP start
number: DB 255
base: DB 100
start:
	MOV D, 232 ;Output Goes here
loop:
	MOV A, [number]
	MOV B, [base]
	CMP A,0
	JZ exit ;If number==0 exit
	DIV B ;Let digit = Number/ Base
	MOV C,A ;store digit for later
	;Let Number = remainder(Number, Base)
	PUSH [number]
	PUSH [base]
	CALL rem
	POP A ;The Base
	POP A ;The Return Value!
	MOV [number],A
	;Base = Base / 10
	MOV A, B ;We can only Divide in A
	DIV 10
	MOV [base], A
	;Display ASCII for digit
	ADD C, 48 ;ASCII digits start at 48='0'
	MOV [D], C
	ADD D, 1
	JMP loop
exit:
	HLT
rem:
	;Make Backups
	PUSH A ;Back up register A
	PUSH B ;Back up register B
	;Load Inputs
	MOV A, [SP+5] ;regA= 9
	MOV B, [SP+4] ;regB = 2
	DIV B ;regA = regA/regB (q in alg)
	MUL B ;regB = q*b in alg
	MOV B, [SP+5] ; We need to remember 9
	SUB B,A ; RegB=a-q*b (The remainder)
	;Return Value
	MOV [SP+5],B
	;Restore Backups
	POP B
	POP A
	RET

