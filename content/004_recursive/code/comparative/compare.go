package main

import (
	"bufio"
	"fmt"
	"os"
)

func factorial(n int) int {
	if n == 0 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

//Person is a struct that store info about a person.
type Person struct {
	firstName string
	lastName  string
	email     string
}

func main() {
	//Create an integer, float, and string
	var num int = 17
	var dec float64 = 25.37
	var word string = "Example"
	fmt.Println(num)
	fmt.Println(dec)
	fmt.Println(word)

	num = num + 10 //Addition
	num = num - 2  //Subtraction
	num = num / 10 //Division (normally integer by default)
	num = num * 2  //Multiplication
	num = num % 5  //Remainder
	fmt.Println(num)

	//Sum up 10 odd numbers
	var total int = 0
	var i int = 0
	for i < 10 {
		total = total + (2*i + 1)
		i = i + 1
	}
	fmt.Println(total)

	var value int = 22
	if value%2 == 0 {
		fmt.Println("I am even.")
	} else {
		fmt.Println("I am odd.")
	}

	fmt.Println(factorial(5))

	var primes []int = []int{2, 3, 5, 7, 11, 13}
	for k := 0; k < len(primes); k++ {
		fmt.Println(primes[k])
	}

	var myValue int = 10
	var myValuePointer *int = &myValue
	fmt.Println(myValuePointer)

	var p Person
	p.firstName = "Mark"
	fmt.Println(p)

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Hello! What is your name?")
	text, _ := reader.ReadString('\n')
	fmt.Println(text)
}
