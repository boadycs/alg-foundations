package main

import "fmt"

func remainder(a int, b int) int {
	var q int = a / b
	var r int = a - q*b
	return r
}

func main() {
	var number int = 137
	var base int = 100
	for number != 0 {
		var digit int = number / base
		number = remainder(number, base)
		base = base / 10
		fmt.Print(digit)
	}
}
