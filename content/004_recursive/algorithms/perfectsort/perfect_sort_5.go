//Mark Boady - Drexel Univeristy
//2020
//Sorting Algorithms

package main

import "fmt"

func issorted(A []int) bool {
	for i := 0; i < len(A)-1; i++ {
		if A[i] > A[i+1] {
			return false
		}
	}
	return true
}
func swap(A []int, x int, y int) {
	var temp int = A[x]
	A[x] = A[y]
	A[y] = temp
}
func sort5(A []int) {
	if len(A) != 5 {
		return
	}
	//First Comparison
	if A[0] > A[1] {
		swap(A, 0, 1)
	}
	//Second Comparison
	if A[2] > A[3] {
		swap(A, 2, 3)
	}
	//Third Comparison
	if A[1] > A[3] {
		swap(A, 0, 2)
		swap(A, 1, 3)
	}
	//Fourth Comparison
	if A[1] > A[4] {
		//Fifth Comparison (Path A)
		if A[0] > A[4] {
			swap(A, 3, 4)
			swap(A, 1, 3)
			swap(A, 0, 1)
		} else {
			swap(A, 3, 4)
			swap(A, 1, 3)
		}
	} else {
		//Fifth Comparison (Path B)
		if A[3] > A[4] {
			swap(A, 3, 4)
		}
	}
	//Sixth Comparison
	if A[2] > A[1] {
		//Seventh Comparison (Path A)
		if A[2] > A[3] {
			swap(A, 2, 3)
		}
	} else {
		//Seventh Comparison (Path B)
		if A[0] > A[2] {
			swap(A, 2, 1)
			swap(A, 1, 0)
		} else {
			swap(A, 2, 1)
		}
	}
}
func sort1(a []int) {
	return
}
func sort2(a []int) {
	if len(a) != 2 {
		return
	}
	if a[0] > a[1] {
		swap(a, 0, 1)
	}
}
func sort3(ARRAY []int) {
	if len(ARRAY) != 3 {
		return
	}
	a := ARRAY[0]
	b := ARRAY[1]
	c := ARRAY[2]
	//Comparison 1
	if a < b {
		//Comparison 2
		if b < c {
			ARRAY[0] = a
			ARRAY[1] = b
			ARRAY[2] = c
		} else {
			//Comparison 3
			if a < c {
				ARRAY[0] = a
				ARRAY[1] = c
				ARRAY[2] = b
			} else {
				ARRAY[0] = c
				ARRAY[1] = a
				ARRAY[2] = b
			}
		}
	} else {
		//Comparison 2
		if b < c {
			//Comparison 3
			if a < c {
				ARRAY[0] = b
				ARRAY[1] = a
				ARRAY[2] = c
			} else {
				ARRAY[0] = b
				ARRAY[1] = c
				ARRAY[2] = a
			}
		} else {

			ARRAY[0] = c
			ARRAY[1] = b
			ARRAY[2] = a
		}
	}
}

//test all permutations
func perm(a []int, i int, f func([]int)) {
	//All Permutations Generated
	if i > len(a) {
		f(a)
		if !issorted(a) {
			fmt.Println(a)
		}
		return
	}
	perm(a, i+1, f)
	for j := i + 1; j < len(a); j++ {
		swap(a, i, j)
		perm(a, i+1, f)
		swap(a, i, j)
	}
}

func main() {
	fmt.Println("Perfect Sort Tests")

	A := []int{1}
	perm(A, 0, sort1)

	B := []int{1, 2}
	perm(B, 0, sort2)

	C := []int{1, 2, 3}
	perm(C, 0, sort3)

	D := []int{1, 2, 3, 4, 5}
	perm(D, 0, sort5)
}
