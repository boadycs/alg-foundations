package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix())

	fmt.Println("Hello. This sanity checks my sorts.")

	sanity(bubblesort)
	sanity(insertion)
	sanity(mergesort)
	sanity(quicksort)

	/*for i := 0; i < 100; i++ {
		fmt.Println(randint(0, 5))
	}*/
	//mergesort([]int{9, 8, 7, 6, 5, 4, 3, 2, 1}, 9)
	return
}
func printArray(A []int) {
	fmt.Print("Array: ")
}

//Sanity Checks
func isSorted(A []int) bool {
	for i := 0; i < len(A)-1; i++ {
		if A[i] > A[i+1] {
			return false
		}
	}
	return true
}

//Includes both a and b in generation
func randInt(a int, b int) int {
	return rand.Intn(b-a+1) + a
}
func randArray(size int) []int {
	A := []int{}
	for i := 0; i < size; i++ {
		A = append(A, randInt(0, size*2))
	}
	return A
}
func sanity(f func([]int, int)) {
	var i int = 2
	var passed = 0
	for i < 502 {
		m := randArray(i)
		f(m, i)
		if !isSorted(m) {
			fmt.Println("Failed!")
		} else {
			passed = passed + 1
		}
		i = i + 1
	}
	fmt.Print("Passed:")
	fmt.Print(passed)
	fmt.Print("\n")
}

//Bubble Sort
func swap(A []int, x int, y int) {
	var temp int = A[x]
	A[x] = A[y]
	A[y] = temp
}
func bubblesort(A []int, size int) {
	var swapped bool = true
	for swapped {
		swapped = false
		var i int = 1
		for i < size {
			if A[i-1] > A[i] {
				swap(A, i-1, i)
				swapped = true
			}
			i = i + 1
		}
	}
	return
}

//Insertion Sort
func insertion(A []int, size int) {
	var i int = 1
	for i < size {
		var j int = i
		for j > 0 && A[j-1] > A[j] {
			swap(A, j-1, j)
			j = j - 1
		}
		i = i + 1
	}
}

//MergeSort
func copyParts(From []int, To []int, start int, stop int) {
	var i int = 0
	for i < stop-start+1 {
		To[i] = From[start+i]
		i = i + 1
	}
	return
}
func allocateArray(size int) []int {
	return make([]int, size)
}
func deleteArray(A []int) {
	A = nil
}

func merge(A []int, start int, middle int, stop int) {
	//Create Two AUX Arrays
	var firstSectionSize int = middle - start + 1
	var firstSection []int = allocateArray(firstSectionSize)
	var secondSectionSize int = stop - middle
	var secondSection []int = allocateArray(secondSectionSize)
	//Copy Elements in A from start to stop into Aux Array
	copyParts(A, firstSection, start, middle)
	copyParts(A, secondSection, middle+1, stop)
	//Set Variables
	var i int = 0
	var j int = 0
	//Loop Over Values
	for k := start; k <= stop; k++ {
		if i >= firstSectionSize {
			A[k] = secondSection[j]
			j = j + 1
		} else if j >= secondSectionSize {
			A[k] = firstSection[i]
			i = i + 1
		} else if secondSection[j] > firstSection[i] {
			A[k] = firstSection[i]
			i = i + 1
		} else {
			A[k] = secondSection[j]
			j = j + 1
		}
	}
	//Clear Aux Data
	deleteArray(firstSection)
	deleteArray(secondSection)
}

//This function does the real mergesort
func msort(A []int, start int, stop int) {
	if start >= stop {
		return
	}
	var middle = start + ((stop - start) / 2)
	msort(A, start, middle)
	msort(A, middle+1, stop)
	merge(A, start, middle, stop)
}

//The MergeSort function gives the recursive
//call a consistent format with our other sorts
func mergesort(A []int, size int) {
	msort(A, 0, size-1)
}

//QuickSort
func quicksort(A []int, size int) {
	qsort(A, 0, size-1)
}
func qsort(A []int, start int, stop int) {
	if start < stop {
		var p int = partition(A, start, stop)
		qsort(A, start, p-1)
		qsort(A, p+1, stop)
	}
}
func partition(A []int, start int, stop int) int {
	//Select Random index
	var randomIndex int = randInt(start, stop)
	//Swap with last position
	swap(A, stop, randomIndex)
	//Partition Based on Last Index
	var pivot int = A[stop]
	var i int = start
	for j := start; j < stop; j++ {
		if A[j] < pivot {
			swap(A, i, j)
			i = i + 1
		}
	}
	swap(A, i, stop)
	return i
}
