#Mark Boady

def swap(A,i,j):
    temp = A[i]
    A[i] = A[j]
    A[j] = temp
#Generate an animation showing how bubblesort works
def makeFrame(instruction,i,j,pivot,A):
    title="Partition Example"
    res=""
    res+="\\begin{frame}\n"
    res+="\\begin{center}\n"
    res+="\t {\\bf "+title+"} \n\n"
    res+="\t \\vspace{0.01in} \n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t {\\bf Event}: \n\n"
    res+="\t "+instruction
    res+="\n\n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t \\vspace{0.02in} \n"
    res+=drawArray(A, i,j,pivot)
    res+="\t \\vspace{0.1in} \n"
    res+="\t {\\bf Size}: "+str(len(A))+" \n\n"
    res+="\t {\\bf Pivot Value}: "+str(pivot)+" \n\n"
    if i > 0:
        res+="\t {\\bf i}: "+str(i)+" \n\n"
    else:
        res+="\t {\\bf i}: 0\n\n"
    if j > 0:
        res+="\t {\\bf j}: "+str(j)+"\n\n"
    else:
        res+="\t {\\bf j}: 0\n\n"
        
    res+="\t { \\color{blue} Blue Elements are Smaller }\n\n"
    res+="\t { \\color{red} Red Elements are Larger} \n\n"
    res+="\\end{center}\n"
    res+="\\end{frame}\n\n"
    return res
    
def drawArray(A,i,j,pivot):
    size=len(A)
    res="\t \\begin{tabular}{|"
    for x in range(0,size+1):
        res+=" c |"
    res+="}\n"
    res+="\t\t \\hline \n"
    res+="\t\t Index "
    for x in range(0,size):
        res+="& "+str(x)+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Value "
    for x in range(0,size):
        if A[x] > pivot and pivot!=0:
            res+="& { \\color{red} "+str(A[x])+"} "
        elif A[x] < pivot and pivot!=0:
            res+="& { \\color{blue} "+str(A[x])+"} "
        else:
            res+="& "+str(A[x])+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Positions "
    for x in range(0,size):
        if x==i and x==j:
            res+="& i,j "
        elif x==i:
            res+="& i "
        elif x==j:
            res+="& j "
        else:
            res+="&  "
    res+=" \\\\ \\hline \n"
    res+="\t \\end{tabular}\n\n"
    return res

def copyText(fileStream, fileName):
    target = open(fileName,"r")
    for line in target:
        fileStream.write(line)
    target.close()


def main():
    f=open("partition.tex","w")
    
    #Copy Front Matter
    copyText(f,"frontmatter.tex")
    
    #Variables
    A=[9,2,7,4,3,1,8,6]
    i=-1
    j=-1
    
    #Opening Slide
    content=makeFrame("Initial Array is Not Partitioned.",i,j,0,A)
    f.write(content)
    
    #Partition
    #Select Random index
    start = 0
    stop = len(A)-1
    randomIndex = 3
    content=makeFrame("Select Pivot at index 3",i,j,A[3],A)
    f.write(content)
    #Swap with last position
    swap(A, stop, randomIndex)
    content=makeFrame("Swap Pivot to end",i,j,A[stop],A)
    f.write(content)
    #Partition Based on Last Index Pivot
    pivot = A[stop]
    i = start
    j = start
    content=makeFrame("Set i and j variables",i,j,A[stop],A)
    f.write(content)
    while(j < stop):
        #Swap a small and large value into place

        content=makeFrame("Compare A[j] $<$ pivot",i,j,A[stop],A)
        f.write(content)
        content=makeFrame("Compare "+str(A[j])+ " $<$ " +str(pivot),i,j,A[stop],A)
        f.write(content)
        if(A[j] < pivot):
            content=makeFrame("Swap Needed",i,j,A[stop],A)
            f.write(content)
            swap(A, i, j)
            content=makeFrame("Swap A[i] and A[j]",i,j,A[stop],A)
            f.write(content)
            i = i + 1
        j=j+1
    #Put the pivot in place
    swap(A, i, stop)
    #Return index of pivot

    #Closing Slide
    j=-1
    content=makeFrame("Final Array is partitioned.",i,j,pivot,A)
    f.write(content)
    
    #Copy Back Matter
    copyText(f,"backmatter.tex")
    
    f.close()

if __name__=="__main__":
    main()
