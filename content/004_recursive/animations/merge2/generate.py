#Mark Boady

def swap(A,i,j):
    temp = A[i]
    A[i] = A[j]
    A[j] = temp
def copyParts(A, B, start, stop):
    for i in range(0,stop-start+1):
        B[i] = A[i+start]
#Generate an animation showing how Merge works
def makeFrame(instruction,A,F,S,i,j,k,start,middle,stop):
    title="Merge Section of Larger Array"
    res=""
    res+="\\begin{frame}\n"
    res+="\\begin{center}\n"
    res+="\t {\\bf "+title+"} \n\n"
    res+="\t \\vspace{0.01in} \n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t {\\bf Event}: \n\n"
    res+="\t "+instruction
    res+="\n\n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t \\vspace{0.02in} \n"
    res+=drawArray(A,"Array", k,"k", True,start,stop)
    res+=drawArray(F,"Aux 1", i, "i", False,start,stop)
    res+=drawArray(S,"Aux 2", j, "j", False,start,stop)
    res+="\t \\vspace{0.1in} \n"
    res+="\t {\\bf Start}: "+str(start)+" \n"
    res+="\t {\\bf Middle}: "+str(middle)+" \n"
    res+="\t {\\bf Stop}: "+str(stop)+" \n\n"
    if i > 0:
        res+="\t {\\bf i}: "+str(i)+" \n"
    else:
        res+="\t {\\bf i}: 0\n"
    if j > 0:
        res+="\t {\\bf j}: "+str(j)+"\n"
    else:
        res+="\t {\\bf j}: 0\n"
    if k > 0:
        res+="\t {\\bf k}: "+str(k)+"\n\n"
    else:
        res+="\t {\\bf k}: 0\n\n"
    res+="\\end{center}\n"
    res+="\\end{frame}\n\n"
    return res
    
def drawArray(A,name,k,varname,color,start,stop):
    size=len(A)
    res="\t {\\bf "+name+"}: \\begin{tabular}{|"
    for x in range(0,size+1):
        res+=" c |"
    res+="}\n"
    res+="\t\t \\hline \n"
    res+="\t\t Index "
    for x in range(0,size):
        res+="& "+str(x)+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Value "
    for x in range(0,size):
        if A[x]==-1:
            res+=" & "
        elif color and x <= k and x >= start and x <= stop:
            res+="& { \\color{blue} "+str(A[x])+"} "
        else:
            res+="& "+str(A[x])+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Positions "
    for x in range(0,size):
        if k==x:
            res+="& "+str(varname)+" "
        else:
            res+="&  "
    res+=" \\\\ \\hline \n"
    res+="\t \\end{tabular}\n\n"
    return res

def copyText(fileStream, fileName):
    target = open(fileName,"r")
    for line in target:
        fileStream.write(line)
    target.close()


def main():
    f=open("merge2.tex","w")
    
    #Copy Front Matter
    copyText(f,"frontmatter.tex")
    
    #Variables
    A=[17,14,19,2,3,4,8,1,5,6,100,9,23]
    start=3
    middle=6
    stop=9
    
    
    #Opening Slide
    content=makeFrame("Merge Two Sorted Sections", A, [-1]*len(A), [-1]*len(A), -1, -1, -1, start, middle, stop)
    f.write(content)
    
    
    #Implement an actual merge
    firstSectionSize = middle - start + 1
    firstSection = [-1]*len(A)
    secondSectionSize = stop - middle
    secondSection = [-1]*len(A)
    copyParts(A, firstSection, start, middle)
    copyParts(A, secondSection, middle+1, stop)
    
    content=makeFrame("Copy Values to Aux Arrays", A, firstSection, secondSection, -1, -1, -1, start, middle, stop)
    f.write(content)

    i = 0
    j = 0
    k=start
    while k <= stop:
        if i >= firstSectionSize:
            A[k] = secondSection[j]
            content=makeFrame("Select Value from Second Section", A, firstSection, secondSection, i, j, k, start, middle, stop)
            f.write(content)
            j = j + 1
        elif j >= secondSectionSize:
            A[k] = firstSection[i]
            content=makeFrame("Select Value from First Section", A, firstSection, secondSection, i, j, k, start, middle, stop)
            f.write(content)
            i = i + 1
        elif secondSection[j] > firstSection[i]:
            A[k] = firstSection[i]
            content=makeFrame("Select Value from First Section", A, firstSection, secondSection, i, j, k, start, middle, stop)
            f.write(content)
            i = i + 1
        else:
            A[k] = secondSection[j]
            content=makeFrame("Select Value from Second Section", A, firstSection, secondSection, i, j, k, start, middle, stop)
            f.write(content)
            j = j + 1
        k=k+1
    firstSection = [-1]*len(A)
    secondSection = [-1]*len(A)
    

    #Closing Slide
    content=makeFrame("The Merge is Completed", A, [-1]*len(A), [-1]*len(A), i, j, k, start, middle, stop)
    f.write(content)
    #Copy Back Matter
    copyText(f,"backmatter.tex")
    
    f.close()

if __name__=="__main__":
    main()
