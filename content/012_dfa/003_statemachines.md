# State Machines

Talking about programs at the assembly level is very difficult. This is why we create abstractions. One we have already seen is pseudocode. Pseudocode allowed us to describe an algorithm at the abstract level. All programming langauges are also abstractions. They are closer to human readable then assembly, but eventually everything run on the processor needs to be binary. Even assembly is an abstraction of the pure binary.

Depending on what we want to design, different abstractions work better. We define different models of computation so we can easily design algorithms to solve specific problems. 

One way to model computation is with states. Imagine you have a person. The person has 3 states. They can be *happy*, *neutral*, or *sad*. The person can only be in any one state at a time. You can't be *happy* and *sad* at the same time. 

The person starts the day in the neutral state. If someone hands the person \$10.00 they will become happy. If someone steals \$20.00 they will become sad.

![Happy and Sad State Machine](graphs/happy.jpg)

This is called a **State Machine**. Each circle is a **state** the person can be in. The **transitions** show what causes the person to change states.

Each state has a **label** telling us about it. These could just be meaningless names like *State 3* or something that conveys information to the reader. A transition has a value that causes the transition. It also has a direction. The transition moves from one state to another.

State Machines can be used to design and study algorithms.
