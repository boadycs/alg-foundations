/*
    Author: Mark Boady
    Date: 2/2/2021
    Drexel University
    
    Java Implementation of Euclid's Algorithm
*/
 
//Useful Libraries
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class gcd
{
    //Main Function to ask for inputs
    public static void main(String argv[])
    {
        System.out.println("Welcome to GCD Example");
        int a = askForInput("Enter First Positive Integer: ");
        int b = askForInput("Enter Second Positive Integer: ");
        int result=gcd(a,b);
        String output=String.format(
            "The GCD of %d and %d is %d"
            ,a,b,result);
        System.out.println(output);
        return;
    }
    
    //Use Euclid's algorithm to find the gcd
    public static int gcd(int a, int b)
    {
        if(b==0)
        {
            return a;
        }else
        {
            return gcd(b, a%b);
        }
    }
    
    //Function to get a positive integer
    //with input checking
    public static int askForInput(String question)
    {
        BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
        System.out.print(question);
        String text;
        //Java Streams can become unreadable
        try{
            text = reader.readLine();
        }catch(IOException e)
        {
            System.out.println("Could not read input.");
            return 0;//Unrecoverable
        }
        //See if it is an int
        try{
            int x = Integer.parseInt(text);
            //Make sure it is positive
            if(x >= 0)
            {
                return x;
            }
            System.out.println("Negative Numbers not allowed.");
            return askForInput(question);
        }catch(NumberFormatException e) {
            System.out.println("Only Positive Integers allowed.");
            return askForInput(question);
        }
    }
 
}
