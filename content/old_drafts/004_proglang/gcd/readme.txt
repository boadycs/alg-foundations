Mark Boady - Drexel University 2020

GCD implementation in three languages.

To run the python code
python3 gcd.py


To run go code
go build gcd.go
./gcd

To run C++ code (on linux/mac with G++)
g++ -o gcdApp gcd.cpp
./gcdApp

To run Java code
javac gcd.java
java gcd

You can also use the provided makefile to test the code.
Run a specific language:
make cpp
make java
make python
make go

Run all code:
make all

Clean up compiled executables:
make clean

