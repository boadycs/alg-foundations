import java.lang.Math;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class comp
{
    public static int factorial(int n)
    {
        if(n==0){
            return 1;
        }else{
            return n*factorial(n-1);
        }
    }

    public static void main(String[] argv) throws IOException  
    {
        //Create an integer, float, and string
        int num = 17;
        double dec = 25.37;
        String word = "Example";
        System.out.println(num);
        System.out.println(dec);
        System.out.println(word);

        num = num + 10; //Addition
        num = num - 2;  //Subtraction
        num = num / 10; //Division (normally integer by default)
        num = num * 2;  //Multiplication
        num = num % 5;  //Remainder
        System.out.println(num);

        //Sum up 10 odd numbers
        int total = 0;
        int i = 0;
        while( i < 10 ){
            total = total + (2*i + 1);
            i = i + 1;
        }
        System.out.println(total);

        int value = 22;
        if(value%2 == 0){
            System.out.println("I am even.");
        } else {
            System.out.println("I am odd.");
        }

        System.out.println(factorial(5));

        int[] primes= new int[6];
        primes[0]=2;
        primes[1]=3;
        primes[2]=5;
        primes[3]=7;
        primes[4]=11;
        primes[5]=13;
        for(int k = 0; k < primes.length; k++){
            System.out.println(primes[k]);
        }
        
        //In Java we can't get a pointer
        //Everything is always a reference


        Person p = new Person();
        p.firstName = "Mark";
        System.out.println(p.firstName);
        
        BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Hello! What is your name?");
        String text = reader.readLine();
        System.out.println(text);

    }
}

class Person {
    public String firstName;
    public String lastName;
    public String email;
}
