import sys
import math

def factorial(n):
    if n==0:
        return 1
    else:
        return n*factorial(n-1)
        
class Person:
    firstName = ""
    lastName = ""
    email = ""
    
num = 17
dec = 25.37
word = "Example"
print(num,dec,word)

num = num + 10 #Addition
num = num - 2 #Subtraction
num = num // 10 #Integer Division in Python
num = num * 2 #Multiplication
num = num % 5 #Remainder
print(num)

total = 0
i=0
while i < 10:
    total = total + (2*i+1)
    i = i + 1
print(total)

value = 22
if value%2==0:
    print("I am even.")
else:
    print("I am odd.")

print(factorial(5))

primes = [2, 3, 5, 7, 11 ,13]
for k in range(0,len(primes)):
    print(primes[k])

#Python doesn't let us use the pointer address
#But we can see the ID to tell if two values point to the
#Same place
myValue = 10
myValueID = id(myValue)
print(myValueID)

p = Person()
p.firstName = "mark"
print(p)

print("Hello! What is your name?")
text = input()
print(text)




