#Mark Boady
#Draw a chart to show why fast power is faster
import math
import matplotlib.pyplot as plt

def mult(a,b):
	global count
	count+=1
	return a*b
	
def slowMethod(a,b,n):
	temp = 1
	while(b > 0):
		temp = mult(temp,a) % n
		b=b-1
	return temp
	
def fastMethod(a,b,n):
	if b==0:
		return 1
	if b%2==0:
		x=fastMethod(a,b>>1,n)
		x=x%n
		return mult(x,x)%n
	return mult(a,fastMethod(a,b-1,n))%n

def lowerBound(b):
	if b==0:
		return 0
	return math.floor(math.log2(b))+1
def upperBound(b):
	if b==0:
		return 0
	return 2*math.floor(math.log2(b))+2
def makeCoolChart(data):
	output="| Computation | Naive Mults | Fast Mults | Lower Est. | Upper Est. |\n"
	output+="| ---- | ---- | ---- | ---- | ---- |\n"
	compFmt="${:d}^{{{:d}}} \\mod {:d}={:d}$"
	for row in data:
		thisRow="| "
		thisRow+=compFmt.format(row[0],row[1],row[2],row[3])
		thisRow+="| "+str(row[4])
		thisRow+="| "+str(row[6])
		thisRow+="| "+str(row[7])
		thisRow+="| "+str(row[8])
		thisRow+="|\n"
		output+=thisRow
	print(output)
	
def barchart01(data):
	X=[data[i][1] for i in range(0,len(data))]
	Y1=[data[i][4] for i in range(0,len(data))]
	Y2=[data[i][6] for i in range(0,len(data))]
	
	plt.plot(X, Y1, color='blue',label="Naive")
	plt.plot(X, Y2, color='red',label="Fast Power")
	plt.legend()
	plt.title("Comparison of Number of Multiplications")
	plt.xlabel("Exponent (b)")
	plt.ylabel("Multiplications")
	plt.savefig("expComp01.png")
	plt.clf()
	plt.cla()
	return

def barchart02(data):
	X=[data[i][1] for i in range(0,len(data))]
	Y1=[data[i][6] for i in range(0,len(data))]
	Y2=[data[i][7] for i in range(0,len(data))]
	Y3=[data[i][8] for i in range(0,len(data))]
	
	plt.plot(X, Y1, color='blue',label="Fast Power")
	plt.plot(X, Y2, color='red',label="Lower Estimate")
	plt.plot(X, Y3, color='black',label="Upper Estimate")
	plt.legend()
	plt.title("Comparison of Number of Multiplications")
	plt.xlabel("Exponent (b)")
	plt.ylabel("Multiplications")
	plt.savefig("expComp02.png")
	plt.clf()
	plt.cla()
	return

if __name__=="__main__":
	count=0
	base=5
	modulus=9
	data=[]
	for exp in range(0,129):
		#print(base,exp,modulus)
		row=[]
		row.append(base)
		row.append(exp)
		row.append(modulus)
		count=0
		a = slowMethod(base,exp,modulus)
		row.append(a)
		row.append(count)
		count=0
		b = fastMethod(base,exp,modulus)
		row.append(b)
		row.append(count)
		x = lowerBound(exp)
		y = upperBound(exp)
		row.append(x)
		row.append(y)
		if a!=b:
			print("ERROR:",base,exp,modulus)
			break
		if row[6] < row[7] or row[6] > row[8]:
			print("ERROR in Estimates",base,exp,modulus)
			break
		data.append(row)
	#We did it!
	makeCoolChart(data)
	barchart01(data)
	barchart02(data)
