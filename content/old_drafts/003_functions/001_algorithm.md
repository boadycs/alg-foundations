# What is an Algorithm?

An **algorithm** is a list of steps you can follow to finish a task. A **program** is an algorithm that has been coded into something that can be run by a machine. These two concepts are often combined, but they are distinctly different. 
	
A **program** has lots of specifics and limitations. It is limited by the hardware it is being run on. It needs to deal with specifications like programming language syntax and processor architecture. 
	
An **algorithm** is a more general high level concept. It doesn't even need to be related to a computer at all. For example, the Euclidean algorithm was first described in 300 BC. It was not optimized for any specific type of computer. 
	
We can analyze both **algorithms** and **programs**. What we look for in each case will be slightly different, but in both cases we are trying to find the best solution. We will look at the algorithms and how we implement them as programs. If we can be sure an **algorithm** is optimal, any implementation will be **asymptotically optimal**. We will define **asymptotically optimal** formally later. For now, we will just call it "reasonably close to optimal".
	
There are many algorithms you can't **program** in our assembly simulator. It doesn't have enough memory to count the number of words in the bible. You also couldn't port Overwatch to our simulator. That doesn't mean these problems are computationally impossible. It just means they can't be implemented on this specific computer. 
	
There must be limits to what an algorithm can do. Problems that no computer can solve, regardless of hardware. Alan Turing came up with 4 principles that make an algorithm **effectively calculable** {cite}`sep-church-turing`. 
	
1. The algorithm is set out in terms of a finite number of exact instructions.
2. The algorithm will, if carried out without error, produce the desired result in a finite number of steps.
3. The algorithm can be carried out by a human being unaided by any machinery save paper and pencil.
4. The algorithm demands no insight or ingenuity on the part of the human being carrying it out.

Turing set out the idea that if all these requirements are met, then the algorithm can be implemented. If you can't meet these requirements, then no computer can solve the problem. (Classical Computer, he didn't know about quantum computer or molecular computation which might bend the rules.)

Alan Turing summed this up in a single sentence.
	
> LCMs [logical computing machines: Turing’s expression for Turing machines] can do anything that could be described as "rule of thumb" or "purely mechanical". {cite}`turingMachine`


All classic computers and programming language still fall within Turing's guidelines. We aren't even sure if any new methods of computation break them or not. It is possible, but not proven conclusively.
	
We will start to look at computation from the perspective of designing **algorithms** and then translating them into **programs**.
