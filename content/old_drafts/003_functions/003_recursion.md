# Recursion

Once we have a concept of a function, we can create a **recursive function**. A **recursive function** is a function that calls itself. This is a second way to create iteration. 

## Factorial Function

One of the classical examples is the factorial function. The `fact(n)` is the product of all integers from $1$ to $n$.

$
\begin{align}
    \text{fact}(n) = 1*2*3*4*5*6*7*\cdots*n
\end{align}
$

We can think of this problem in terms of smaller repeated problems. 

For example, I want to solve for `fact(5)`. If I knew the answer to `fact(4)` then I could multiply that answer by $5$. The problem `fact(4)` is strictly less complex then `fact(5)`. Further, to solve `fact(4)` it would be useful to already know `fact(3)`. We can solve the original problem if we know the solution to a simpler problem.

A **recursive case** is a way to define a function in terms of smaller versions of itself. Notice that this is a case because it only handles some inputs.

$
\begin{align}
    \text{fact}(n) =& \text{fact}(n-1) * n & n \ge 0
\end{align}
$

This pattern can't go on forever. There needs to be a simplest case. Each recursive call makes the problem simpler (like counting down). We can only stop if there is a smallest input. You can count down negative numbers forever. We need a stopping point. This is called the **base case**. For the factorial function, the smallest input is mathematically defined as `fact(0)`=1.

$
\begin{align}
    \text{fact}(1) =& 1 & n=0
\end{align}
$

In general, we may need multiple **base cases** and **recursive cases**. For this function, we only need one of each.

Now that we have handled all possible inputs, we can write the recursive definition.

$
\begin{align}
    \text{fact}(n) =& \begin{cases}
        \text{fact}(n-1)*n & n > 0 \\
        1 & n=0
    \end{cases}
\end{align}
$

Next, we can translate this mathematical definition into an algorithm.

```Go
func fact(n int) int {
        if( n == 0){
                return 1
        }else{
                return fact(n-1)*n
        }
}
```

How do we use this to compute a value? We can trace the logic of `fact(5)`.

| Function Call | Computation | Depends on | Result |
| --------------- | --------------- | -------------- | ------- |
| `fact(5)` | `return fact(5-1)*5` | `fact(4)` | $24 * 5 = 120$ |
| `fact(4)` | `return fact(4-1)*4` | `fact(3)` | $6 * 4 = 24$ |
| `fact(3)` | `return fact(3-1)*3` | `fact(2)` | $2 * 3 = 6$ |
| `fact(2)` | `return fact(2-1)*2` | `fact(1)` | $1 * 2=2$ |
| `fact(1)` | `return fact(1-1)*1` | `fact(0)` | $1 * 1=1$ |
| `fact(0)` | `return 1` | Nothing | 1 |

## Factorial Implementation

We need to convert the algorithm into assembly code. We need to think about how much memory we need. We need the value $n$ as input. That means we will need one value pushed onto the stack to start the algorithm. We also return a single integer. We overwrite the input $n$ with the result when we are done. We will also need the answer to the recursive call.

We can use two registers.

- A - Input to and Result of Recursive Call
- B - Current Value of $n$

That means at the start of each function call, we need to push registers A and B. Then when we return we need to pop them back into place. Since we need to multiply, our answer must end up in A. It is the one register that allows multiplication. We can set up a basic template.

```
;Recursive Factorial Function
JMP main
;----Global Variables
input_n: DB 5
result: DB 0
;------Main Program----
main:
    MOV A, [input_n]
    PUSH A
    MOV A, 0 ;Clear A to see it gets reset
    CALL fact
    POP A
    MOV [result],A
    HLT
;-----Factorial Function-----
fact:
    ;---Back up Inputs ----
    PUSH A
    PUSH B
    MOV A, [SP+4]
    ;---Set Return Value
    MOV [SP+4],A
    ;Restore Registers
    POP B
    POP A
    RET
```

The base case is the most straightforward. We can program that next.

```
;Recursive Factorial Function
JMP main
;----Global Variables
input_n: DB 0
result: DB 0
;------Main Program----
main:
    MOV A, [input_n]
    PUSH A
    MOV A, 0 ;Clear A to see it gets reset
    CALL fact
    POP A
    MOV [result],A
    HLT
;-----Factorial Function-----
fact:
    ;---Back up Inputs ----
    PUSH A
    PUSH B
    MOV A, [SP+4]
    ;Body of Function
    CMP A, 0
    JZ fact_base_case
fact_exit:
    ;---Set Return Value
    MOV [SP+4],A
    ;Restore Registers
    POP B
    POP A
    RET
fact_base_case:
    MOV A, 1 ;If n==0 then return 1
    JMP fact_exit
```

The Recursive call still needs to be handled. We compute the next input to the factorial function. We then make a recursive call. When the call returns, we know the answer will be on the stack. We can load it into register `A`. We then multiply register `A` (the recursive call) by register `B` (the value of $n$). The product is the return value of this call.

The register  `B` is treated as storing the value of $n$ for the function input. Keep in mind, the recursive call will also use `B` itself. We are trusting each recursive call to correct back up `B` and return it to the correct value when it is done. If this was not true, the code would not work. If recursive calls changed `B` without undoing their changes, we would lose values we needed.

```{note}
It is fine to jump within a function. We never want to jump out of a function. To enter a function we use **CALL** and to leave we use **RET**.
```

```
;Recursive Factorial Function
JMP main
;----Global Variables
input_n: DB 5
result: DB 0
;------Main Program----
main:
    MOV A, [input_n]
    PUSH A
    MOV A, 0 ;Clear A to see it gets reset
    CALL fact
    POP A
    MOV [result],A
    HLT
;-----Factorial Function-----
fact:
    ;---Back up Inputs ----
    PUSH A
    PUSH B
    MOV A, [SP+4]
    ;Body of Function
    CMP A, 0
    JZ fact_base_case
    JNZ fact_recur_case
fact_exit:
    ;---Set Return Value
    MOV [SP+4],A
    ;Restore Registers
    POP B
    POP A
    RET
fact_base_case:
    MOV A, 1 ;If n==0 then return 1
    JMP fact_exit
fact_recur_case:
    ;Recursive Call
    MOV B, A ;Remember N
    SUB A, 1 ;for fact(n-1)
    PUSH A
    CALL fact
    POP A
    MUL B ;fact(n-1)*n
    JMP fact_exit
```

## Factorial Runtime Analysis

How does our factorial function run?

There are two possible paths through this code. The first is the path where `JZ` causes a jump and the case where it doesn't. 

We can break the function up into sections for analysis.

```
fact:
    ;---Back up Inputs ----
    PUSH A
    PUSH B
    MOV A, [SP+4]
    ;Body of Function
    CMP A, 0
    JZ fact_base_case
    JNZ fact_recur_case
```

This first section is the function startup. 

| Operation | Times Run |
| ----------- | ------------ |
| CMP       |    1              |
| JNZ       |     1 or 0           |
|  JZ          |    1              |
| MOV       |   1             | 
| PUSH      |   2              |

All but 1 line executes every time. The last `JNZ` won't execute if the `JZ` before it caused a jump. The `JZ` always runs, but it doesn't always cause a jump.

The section of the code exiting the function also executes just once at the end. It is easy to analyze.

```
fact_exit:
    ;---Set Return Value
    MOV [SP+4],A
    ;Restore Registers
    POP B
    POP A
    RET
```

| Operation | Times Run |
| ----------- | ------------ |
| MOV      |   1             |
| POP       |   2             | 
| RET       |    1              |

The base case will also only run one time, when we call `fact(0)`.

```
fact_base_case:
    MOV A, 1 ;If n==0 then return 1
    JMP fact_exit
```
| Operation | Times Run |
| ----------- | ------------ |
| JMP       |   1             | 
| MOV      |   1             |

The recursive case will happen multiple times, but each **execution** will be the same. 

```
fact_recur_case:
    ;Recursive Call
    MOV B, A ;Remember N
    SUB A, 1 ;for fact(n-1)
    PUSH A
    CALL fact
    POP A
    MUL B ;fact(n-1)*n
    JMP fact_exit
```

| Operation | Times Run |
| ----------- | ------------ |
| CALL | 1 | 
| JMP | 1 |
| MOV      |   1             |
| MUL | 1 |
| POP | 1 |
| PUSH | 1 | 
| SUB       |   1             | 

We need to combine all these values to determine the complete runtime. There are two possible paths through the code.

One is the path when we call `fact(0)`. In this situation, the startup, base case, and exit code execute. The `JZ` causes a jump and `JNZ` does not run.


| Operation | Times Run |
| ----------- | ------------ |
| CMP       |    1              |
| JMP | 1 | 
|  JZ          |    1              |
| MOV       |   3             | 
| POP | 2 |
| PUSH      |   2              |
| RET | 1 |

We determine that computing `fact(0)` requires 11 operations.

The second path is the recursive call. It still does the startup and ending block. This time, we execute the recursive case and we do execute `JNZ`

| Operation | Times Run |
| ----------- | ------------ |
| CALL | 1 | 
| CMP       |    1              |
| JMP | 1 |
| JNZ       |     1          |
| JZ          |    1              |
| MOV       |   3             | 
| MUL | 1 |
| POP       | 3 |
| PUSH      |   3              |
| RET | 1 |
| SUB       |   1             | 

This path does 17 operations, and makes a recursive call. We can try to work out how many total operations are needed to complete a task. How many operations are needed to compute `fact(5)`?

| Function Call | Operations Needed |
| --------------  | ---------------------- |
| `fact(0)` | 11 |
| `fact(1)` | 17 + call to `fact(0)` = 17+11=28 |
| `fact(2)` | 17 + call to `fact(1)` = 17+28 = 45 |
| `fact(3)` | 17 + call to `fact(2)` = 17+45 = 62 |
| `fact(4)` | 17 + call to `fact(3)` = 17+62 = 79 |
| `fact(5)` | 17 + call to `fact(4)` = 17+79= 96 |

Notice that every call added 17 to the total. Additionally, we always had 11 extra for the base case. To compute `fact(n)` requires $17n+11$ operations.

We can now predict that is $n=100$ we could need to complete 1711 operations. This is another linear expression for the amount of work. It is linear in the integer $n$ given as input. 



## Factorial Memory Analysis

How much memory is required for our function? We can start by counting the memory taken up by the program itself. This is a fixed amount.

We just need to count the commands.

| Command | Times Used | Memory Needed (bytes) |
| ------------ | ------------- |  ------------ |
| CALL | 1 | 2 |
| CMP | 1 | 3 |
| JMP | 2 | 2 |
| JNZ | 1 | 2 |
| JZ | 1 | 2 |
| MOV | 4 | 3 |
| MUL | 1 | 2 |
| POP | 3 | 2 |
| PUSH | 3 | 2 | 
| RET | 1 | 1 |
| SUB | 1 | 3 |

In total we have: $2+3+4+2+2+12+2+6+6+1+3=43$ bytes. Since a byte is 8-bits, in our assembly language this is exactly 344 bits.

This is not all the memory the function uses. It also uses the stack. It uses `PUSH` two times to put values onto the stack when it starts. When a recursive call is make another `PUSH` is made. Additionally, the `CALL` command pushes a value onto the stack as well. For every recursive call, we use $4$ extra bytes from the stack. On the base case, only use $2$ extra bytes. The function also has to be called initially and get its initial input, this adds $2$ more bytes at the very start. 

To compute `fact(n)`, we need $43+4+4n$ bytes. We could simplify this to $47+4n$ bytes. This is also a linear expression in the integer input. To compute the value of `fact(100)` we could need $447$ bytes. 

Our assembly simulator only has $256$ bytes of memory. We can't compute `fact(100)` on it. That does not mean we can't predict the memory usage or number of operations needed to solve that problem. We could even use these values to predict what kind of computer we would need to solve that problem. 

Sadly, we would also need a 525 bit processor to store the results without overflow. This problem is easier to deal with. Your computer should have no problem calculating `fact(100)` even thought it doesn't have a 512 bit processor. This is done by breaking the number into parts and then rebuilding it in memory. This is handled by the programming language you are coding in, you never see it happening.

