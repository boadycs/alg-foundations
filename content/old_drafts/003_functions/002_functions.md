# Functions

When designing algorithms, one of the most basic design patterns is the function. A function encapsulates a task. This allows us to study one specific task, then use it inside larger programs. For example, we might encrypt data before sending it over a network. There are lots of tasks that require encryption, so if we can design one encryption algorithm, then implement it as a function. The function can then be used in many places.
    
A function has three parts:

1. The **function name** is the name we use to refer to the function.
2. The **function parameters** are placeholder variables for the values the function needs to do its job.
3. The **function return value** is the result of the computation.

We will write a function to compute the **remainder** of a division. In mathematics, this is traditionally written as the $a \mod b$. We want to compute $ a \mod b = r$ or $a\%b=r$. This requires us two know two values $a$ and $b$. We then compute one value $r$. 
    
Our assembly languages has a divide command, but no remainder command. We can build one. The divide command computes the quotient of division $a / b = q$. The remainder is whatever is left. For example, $9 / 2 = 4$. The quotient of this division is 4. If we compute $2 * 4=8$, we don't get back $9$. We are off by 1. We could also define the remainder as $r=a-q*b$.
    
This leads us to a very simple algorithm for computing the remainder. The algorithm given in a function named **remainder**.
    
```Go
func remainder(a int, b int) int {
    var q int = a / b
    var r int = a - q*b
    return r
}
```

We can run this function by hand on paper. When we use a function with specific values, it is referred to as **calling the function**.  What happens when we **call** the function `remainder(9,2)`. The first command says assign $q=a/b$. We know that $a=9$ and $b=2$ from the function call. The first number is $9$ and that matches with the first parameter $a$. This is telling us to compute $q=9/2=4$.
    
The second command says assign $r=a-q * b$. Plugging in the values we get $r=9-4*2=1$. The last command says **return** the value r. What does it mean to **return 1**? It means we are giving that back as the solution to our computation. 
    
What is the value of `remainder(9,2)+4`? The value of `remainder(9,2)` is 1, the return value. We can now do the addition $1+4=5$ and get our answer.
    
We might want to compute remainders multiple times in our code. By encapsulating this as a function, we can reuse it as needed. We have an algorithm for how the function works, now we need a program to actually do it.
    
First, we just implement the logic of the computing a remainder for fixed values.
    
```
;Compute the remainder
JMP rem
rem:
    MOV A, 9 ;regA= 9
    MOV B, 2 ;regB = 2
    DIV B ;regA = 9/2=4
    MUL B ;regA = 4*2=8
    MOV B, 9 ; regB=9 We need to remember 9
    SUB B,A ; RegB=9-8=1 The remainder
```

This works mechanically, but it is not very general. What if A is already in use by something else? What if the we need to compute the remainder repeatedly for different numbers?

This is where the **Stack Pointer** comes in. The **Stack Pointer** is the address of the last space in your program's usable memory. Assuming you didn't use all available memory, this should be pointing to empty space. We can use this as temporary memory. It gives us a place to store values that we only need for a short time, then leave the memory blank again for the next function/part of the program.

The command **PUSH** puts a value in the last available memory position and decreases the stack pointer by 1. The **POP** command increases the stack pointer by one, allowing that memory location to be used again. The **POP** command also puts the value back into a register.

We know that remainder needs two numbers. It had two **parameters** when we wrote the function. We will decide that you need to push those two values onto the stack before you call remainder. We will get the values out of memory.

Assuming the function call is set up correctly, we know that A is at `SP+2` and B is at `SP+1`. The stack pointer will be at an empty position. `SP+1` is the location of the **last** value pushed, this is `B`. The value of `A` is before it, at `SP+2`. 
    
```
;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    JMP rem ;Run the function
rem:
    MOV A, [SP+2] ;regA= 9
    MOV B, [SP+1] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+2] ; We need to remember org a
    SUB B,A ; Compute Remainder
```
    
Assembly has a special command for **calling** functions. Since we might need to call a function from many different places in our code. How would we know where to jump back to? We wouldn't. The processor can track this for us.

The `CALL` command jumps to a function and stores the position in code it left on the stack. The `RET` command returns to wherever the function was called from. Since `CALL` will use the stack, we need to change our offsets. We know the memory location of the instruction we left will be added to the stack. That instruction will be at `SP+1`, so our parameters are at `SP+2` and `SP+3`.

```
;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember q
    SUB B,A ; RegB= The remainder
    RET
```

Notice that this code now returns to the correct position and halts.

We need to place the answer somewhere in memory. We can put it back on the stack when we are done. We can put the return value in '[SP+3]' so that it can be found later. Since we are implementing this in assembly, we need to decide where the return value goes. This would normally be something a high level programming language did automatically.

```
;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember a
    SUB B,A ; RegB= The remainder
    MOV [SP+3],B
    RET
```

Lastly, to truly make this an **self-contained** function, it should not effect any existing code. This allows us to call that function anywhere in our code and be certain it will not have any **side-effects**.

```{note}
A **side-effect** is when a function modifies the state of a program. For example, changing variables or registers. This means that calling the function can have unexpected consequences in the program. 
```

We need to use register A and B. What if they are already in use? We can make backup copies of A and B and restore them when we are done. We need to change our stack offsets yet again to make sure everything lines up correctly. Now, we can call this function at any point without damaging any ongoing computations. It is completely encapsulated.


```
;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
```
    
Now, we can use this function to solve a problem. What if we wanted to print a number? The following algorithm gives a method for printing out a number. 

If we want to print out a 3 digit number, we know the first digit is the number divided by 100. The remainder will leave us with the digits we need to print. By repeatedly taking digits, we can print the whole number.

```Go
var number int = 137
var order int = 100
for number != 0 {
    var digit int = number / order
    number = remainder(number, order)
    order = order / 10
    fmt.Print(digit)
}
```

The assembly for printing the result is shown below. It uses the remainder function to solve the problem.

```
;Print A Number
JMP start
number: DB 255
order: DB 100
start:
    MOV D, 232 ;Output Goes here
loop:
    MOV A, [number]
    MOV B, [order]
    CMP A,0
    JZ exit ;If number==0 exit
    DIV B ;Let digit = Number/ order
    MOV C,A ;store digit for later
    ;Let Number = remainder(Number, order)
    PUSH [number]
    PUSH [order]
    CALL rem
    POP A ;The order
    POP A ;The Return Value!
    MOV [number],A
    ;Base = Base / 10
    MOV A, B ;We can only Divide in A
    DIV 10
    MOV [order], A
    ;Display ASCII for digit
    ADD C, 48 ;ASCII digits start at 48='0'
    MOV [D], C
    ADD D, 1
    JMP loop
exit:
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
```
