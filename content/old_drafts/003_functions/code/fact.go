package main

import "fmt"

func fact(n int) int {
	if( n == 0){
		return 1
	}else{
		return fact(n-1)*n
	}
}

func main(){

	for i:=0; i < 10; i++ {
		fmt.Println(fact(i))
	}
}
