;Compute the remainder
JMP rem
rem:
	MOV A, 9 ;regA= 9
	MOV B, 2 ;regB = 2
	DIV B ;regA = 9/2=4
	MUL B ;regA = 4*2=8
	MOV B, 9 ; regB=9 We need to remember 9
	SUB B,A ; RegB=9-8=1 The remainder