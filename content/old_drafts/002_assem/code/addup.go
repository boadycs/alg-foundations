package main

import "fmt"

func main() {
	var N int = 10
	var A int = 0
	var B int = 0
	for B <= N {
		A = A + B
		B = B + 1
	}
	var result int = A

	fmt.Println(result)
}
