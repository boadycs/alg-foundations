package main

import "fmt"

func main() {

	//Variables
	var found int = 0
	var target int = 35
	var size int = 9
	var arr = []int{1, 10, 9, 7, 21, 19, 100, 32, 99}
	//Replicate Registers
	var a int = 0
	var b int = target
	var c int = 0
	var d int = size
	//Loop
	for c < d {
		a = arr[c]
		if a == b {
			found = 1
			break //exit loop
		}
		c = c + 1
	}
	//Result is in variable found

	fmt.Println(found)
}
