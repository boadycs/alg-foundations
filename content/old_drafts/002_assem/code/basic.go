package main

import "fmt"

func main() {
	//We need an array with 5 spaces
	var size int = 5
	var x1 = []int{10, 20, 55, 7, 8}
	//We need a place to store the total
	var a int = 0
	//Loop over the array
	for i := 0; i < size; i++ {
		a = a + x1[i]
	}
	fmt.Println(a)

}
