# How Computers Work

If all computers can store is numbers in binary, how do we compute? We have to use numbers in binary! A computer program is just a really big number. Actually, a program is almost exactly like the strings we saw already. It is a sequence of numbers where each number represents something.

We could store 5 numbers in memory and assign some meaning to them. The computer's memory is just a big collection of memory locations. For our purposes, we will assume these are all 8-bit locations. Location 0 is the first location in the physical computers memory. Each location has a value. In the table below, they are shown as base-10 integers.


| Memory Location | Memory Value |
| ---- | ---- |
| 0 | 16 |
| 1 | 9 |
| 2 | 32 |
| 3 | 0 |
| 4 | 0 |

When the computer executes a program, it starts reading memory from left to right. It has a table which tells it what each number means. We could pretend that 16 means "add the next two numbers you see". Then the computer would compute $9+32=41$. Then it would reach 0, which means do nothing and it would just continue doing nothing. In a modern operating system, the OS would let another program run at this point.

In the early days of computers, people actually had to program like this. The PDP-8/S control panel is shown below. The switches each represent a single bit. The programmer would type in the byte by hand and then save it to memory. Then they would flip the switches for the next byte.

*PDP-8 on Exhibit in the Tekniska museet, Stockholm, Sweden*
![PDP-8 Control Panel](pdp8.jpg)

As time passed, PDP-8 systems would eventually allow the values to be entered by punch cards, keyboard, and even floppy disks. We are not going to look *this far* back in history. 

We will start by experimenting with an 8-Bit Assembly Language. Then move to a modern fully featured language. The 8-bit language we are using is based on the one used by the NES, Atari 2600, and Commodore 64. We will be using a small simple instruction set. 

When we look at algorithm designs, we need to think about what the algorithm is actually asking the computer to do. It is great to write code in Python, but how do we analyze how well the code runs? We need an understanding of what the code we write is asking the computer to do.

