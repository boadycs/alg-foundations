package main

import (
	"fmt"
	"math/rand"
	"time"
)

//Mark Boady
//Drexel 2020
//A Classic Heap data structure
//with heapsort

//Implementing a min-heap

//Heap is a classic min-heap stored using an array
type Heap struct {
	//A pointer to the array of values
	data []int
	//The max number of values array can store
	maxSize int
	//The current amount of values
	currentSize int
}

//MakeHeap Allocates a new empty heap
func MakeHeap(capacity int) *Heap {
	var H *Heap = new(Heap)
	H.data = allocateIntArray(capacity)
	H.currentSize = 0
	H.maxSize = capacity
	return H
}

//Empty returns true if the heap is empty
func Empty(H *Heap) bool {
	return H.currentSize == 0
}

//Min returns the smallest value in the heap or 0 when empty
func Min(H *Heap) int {
	if Empty(H) {
		return 0
	}
	//The min is always at the root
	return H.data[0]
}

//Insert value x into heap H
func Insert(x int, H *Heap) {
	if H.currentSize == H.maxSize {
		expand(H)
	}
	H.data[H.currentSize] = x
	H.currentSize++
	upheap(H, H.currentSize-1)
	return
}

//DeleteMin deletes the root of the heap
func DeleteMin(H *Heap) {
	//Nothing to do
	if Empty(H) {
		return
	}
	//Swap Root and last element
	swap(H, 0, H.currentSize-1)
	//Decrease size
	H.currentSize = H.currentSize - 1
	//Fix the heap starting at root
	downheap(H, 0)
}

//HeapSort Sorts array A with size given using a heap
func HeapSort(A []int, size int) {
	var H *Heap = MakeHeap(size)
	//Insert all Elements
	for i := 0; i < size; i++ {
		Insert(A[i], H)
	}
	//Delete All Elements
	for i := 0; i < size; i++ {
		A[i] = Min(H)
		DeleteMin(H)
	}
}

//Helper Functions
//These are used to support the main operations

//We can predict where the parent,
//left, and right children are
//The parent of node i is at index floor((i-1)/2).
func parent(i int) int {
	//Root has no parent
	if i == 0 {
		return -1
	}
	//Reminder: Integer division
	return (i - 1) / 2
}

//The left child of node i is at (i+1)*2-1=2i+1
func leftChild(i int) int {
	return 2*i + 1
}

//The right child of node i is at (i+1)*2=2i+2
func rightChild(i int) int {
	return 2*i + 2
}

//Swap Two Values in the Heap's data array
func swap(H *Heap, i int, j int) {
	var temp int = H.data[i]
	H.data[i] = H.data[j]
	H.data[j] = temp
	return
}

//Expand the size of the array to fit more elements
func expand(H *Heap) {
	var newSize int = 2 * H.currentSize
	var newData []int = allocateIntArray(newSize)
	for i := 0; i < H.currentSize; i++ {
		newData[i] = H.data[i]
	}
	H.data = newData
	H.maxSize = newSize
	return
}

//Upheap value at index i until heap is valid again
func upheap(H *Heap, i int) {
	//Determine the parent
	var p int = parent(i)
	//Already at root
	if p < 0 {
		return
	}
	//Determine parent's value
	var pVal = H.data[p]
	//If less than value at i, then heap valid
	if pVal <= H.data[i] {
		return
	}
	//Otherwise Swap with parent
	swap(H, i, p)
	//Continue to swap till valid heap
	upheap(H, p)
	return
}

//Starting at index i, move value down until
//the heap is valid again
func downheap(H *Heap, i int) {
	//Determine Location of Children
	var leftIndex = leftChild(i)
	var rightIndex = rightChild(i)
	//Part 1: No children, nothing to do
	if leftIndex >= H.currentSize {
		return
	}
	//Part 2: Determine Smallest Child Node
	//Create a variable to store smallest child index
	var minIndex int = pickSmallerChild(H, leftIndex, rightIndex)
	//Part 3: Repair Heap
	//If the smallest child is less than value at i, swap
	if H.data[i] > H.data[minIndex] {
		swap(H, i, minIndex)
		downheap(H, minIndex)
	}
	return
}

//Given two indexes, pick the smaller of the two
func pickSmallerChild(H *Heap, leftIndex int, rightIndex int) int {
	//Variable to store return value
	var minIndex int
	//If no right child, left is smaller
	if rightIndex >= H.currentSize {
		//Left is smaller
		minIndex = leftIndex
	} else { //With two children we need to compare
		//Decide which is smaller
		if H.data[leftIndex] < H.data[rightIndex] {
			minIndex = leftIndex
		} else {
			minIndex = rightIndex
		}
	}
	return minIndex
}

//Functions used just to clear up the code
//Allocates a new Array of size given and returns pointer
func allocateIntArray(size int) []int {
	var A []int = make([]int, size)
	return A
}

//Print the Heap in a pretty format for easy debugging
func printHeap(H *Heap) {
	fmt.Printf("Heap Current Size: %d\n", H.currentSize)
	fmt.Printf("Heap Max Size: %d\n", H.maxSize)
	fmt.Printf("Contents:\n")
	for i := 0; i < H.currentSize; i++ {
		fmt.Printf("H.data[%d]=%d\n", i, H.data[i])
	}
	return
}

func main() {
	var H *Heap = MakeHeap(10)
	//Replicate Slides
	var toInsert = []int{79, 87, 28, 6, 46, 66, 17, 1, 58, 94}
	for i := 0; i < len(toInsert); i++ {
		Insert(toInsert[i], H)
		printHeap(H)
	}
	//Delete All the Values
	for !Empty(H) {
		fmt.Printf("Min: %d\n", Min(H))
		DeleteMin(H)
		printHeap(H)
	}

	//Sanity Check with Random Sorts
	fmt.Printf("\n\nSanity Check\n")
	rand.Seed(time.Now().Unix())
	sanity(HeapSort)
}

//Sanity Checks
func issorted(A []int) bool {
	for i := 0; i < len(A)-1; i++ {
		if A[i] > A[i+1] {
			return false
		}
	}
	return true
}
func randint(a int, b int) int {
	return rand.Intn(b-a) + a
}
func randArray(size int) []int {
	A := []int{}
	for i := 0; i < size; i++ {
		A = append(A, randint(0, size*2))
	}
	return A
}
func sanity(f func([]int, int)) {
	var i int = 0
	var total int = 800
	var passed = 0
	for i < total {
		m := randArray(i)
		f(m, i)
		if !issorted(m) {
			fmt.Println("Failed!")
		} else {
			passed = passed + 1
		}
		i = i + 1
	}
	fmt.Printf("Passed: %d out of %d tests.\n", passed, total)
}
