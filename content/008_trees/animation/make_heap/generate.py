
LOCATIONS=[
    (0,3),
    (-1,2),(1,2),
    (-1.5,1),(-0.5,1),(0.5,1),(1.5,1),
    (-1.75,0),(-1.25,0),(-0.75,0),(-0.25,0),
    (0.25,0),(0.75,0),(1.25,0),(1.75,0)
    ]

def frontmatter():
    res=""
    res+="\\documentclass[hyperref={pdfpagelabels=false}]{beamer}\n"
    res+="\\usepackage{verbatim}\n"
    res+="\\usepackage{tikz}\n"
    res+="\\usetikzlibrary{arrows,shapes}\n"
    res+="\\setbeamertemplate{navigation symbols}{}\n"
    res+="\\tikzstyle{vertex}=[circle,fill=black!25,minimum size=20pt,inner sep=0pt]\n"
    res+="\\tikzstyle{nullvertex}=[circle,fill=white!25,minimum size=20pt,inner sep=0pt]\n"
    res+="\\tikzstyle{newvertex} = [vertex, fill=blue!24]\n"
    res+="\\tikzstyle{edge} = [draw,thick,-]\n"
    res+="\\tikzstyle{selected edge} = [draw,line width=5pt,-,red!50]\n"
    res+="\\begin{document}\n"
    res+="\\pgfdeclarelayer{background}\n"
    res+="\\pgfsetlayers{background,main}\n"
    return res
def backmatter():
    res=""
    res+="\\end{document}\n"
    return res

def make_title(text):
    res=""
    res+="\t \\begin{center}\n"
    res+="\t\t {\\bf "+text+"}\n"
    res+="\t \\end{center}\n"
    return res
def make_table(Data,MaxSize,CurrentSize):
    res=""
    res+="\t \\begin{center}\n"
    res+="\t\t {\\it Max Size}: "+str(MaxSize)
    res+=" {\\it Current Size}: "+str(CurrentSize)+"\n"
    #Start the Table
    res+="\t\t \\begin{tabular}{|"
    for x in range(0,MaxSize+1):
        res+=" c |"
    res+="}\n"
    res+="\t\t\t \\hline \n"
    #Index Row
    res+="\t\t\t Index "
    for x in range(0,MaxSize):
        res+="& "+str(x)+" "
    res+="\\\\ \\hline \n"
    #Value Row
    res+="\t\t\t Value "
    for x in range(0,MaxSize):
        if Data[x]!=0:
            res+="& "+str(Data[x])+" "
        else:
            res+="&   ";
    res+="\\\\ \\hline \n"
    #Close the Table
    res+="\t\t \\end{tabular}\n"
    res+="\t \\end{center}\n"
    return res

#Beat Frame (Nothing Happens)
def beatFrame(title,Data,MaxSize,CurrentSize):
    res=""
    res+="\\begin{frame}\n"
    res+=make_title(title)
    res+=make_table(Data,MaxSize,CurrentSize)
    res+=draw_tree_new([],D)
    res+="\\end{frame}\n\n"
    return res;
#Insert Frame (Inserts a New Number)
def insertFrame(val, D, MS, CS):
    if MS < CS:
        print("ERROR!!!")
        return ""
    title="Insert "+str(val)+" into next empty position."
    D[CS]=val
    res=""
    res+="\\begin{frame}\n"
    res+=make_title(title)
    res+=make_table(D,MS,CS+1)
    res+=draw_tree_new([val],D)
    res+="\\end{frame}\n\n"
    return res;
#Compare Frame
def compareFrame(index, D, MS, CS):
    if MS < CS:
        print("ERROR!!!")
        return ""
    parent=(index-1)//2
    if parent < 0:
        return ""
    if D[parent] < D[index]:
        title=str(D[parent])+" $<$ "+str(D[index])+" is True. Heap is correct."
    else:
        title=str(D[parent])+" $<$ "+str(D[index])+" is False. Heap is incorrect."
    res=""
    res+="\\begin{frame}\n"
    res+=make_title(title)
    res+=make_table(D,MS,CS)
    res+=draw_tree_new([D[parent],D[index]],D,[(parent,index)])
    res+="\\end{frame}\n\n"
    return res;
#Swap Frame
def swapFrame(index, D, MS, CS):
    if MS < CS:
        print("ERROR!!!")
        return ""
    parent=(index-1)//2
    if parent < 0:
        return ""
    title="Swap "+str(D[parent])+" and "+str(D[index])+" to repair heap"
    temp = D[parent]
    D[parent]=D[index]
    D[index]=temp
    res=""
    res+="\\begin{frame}\n"
    res+=make_title(title)
    res+=make_table(D,MS,CS)
    res+=draw_tree_new([D[parent],D[index]],D,[(parent,index)])
    res+="\\end{frame}\n\n"
    return res;
    
    
    
def draw_tree_new(vals,D,highlight=[]):
    res=""
    res+="\t \\begin{figure}\n"
    res+="\t \\begin{tikzpicture}[->,scale=1.8, auto,swap]\n"
    #Vertices
    res+="\t\t %Draw the vertices.\n"
    for i in range(0,len(D)):
        res+="\t\t \\node"
        if D[i] in vals:
            res+="[newvertex] "
        elif D[i]==0:
            res+="[nullvertex] "
        else:
            res+="[vertex]"
        res+=" (node"+str(i)+") at "+str(LOCATIONS[i])
        res+=" {"
        if(D[i]==0):
            res+=" "
        else:
            res+="$"+str(D[i])+"$"
        res+="};\n"
    #Edges
    for i in range(0,len(D)):
        lc = 2*i+1
        rc = 2*i+2
        if lc < len(D):
            if D[i]!=0 and D[lc]!=0:
                res+="\t\t \\path (node"
                res+=str(i)+") edge node {} (node"
                res+=str(lc)+");\n"
        if rc < len(D):
            if D[i]!=0 and D[rc]!=0:
                res+="\t\t \\path (node"
                res+=str(i)+") edge node {} (node"
                res+=str(rc)+");\n"
    #Background
    if len(highlight)>0:
        res+="\t\t \\begin{pgfonlayer}{background}\n"
        for pair in highlight:
            res+="\t\t\t \\path[selected edge] (node"
            res+=str(pair[0])
            res+=") edge node {} (node"
            res+=str(pair[1])
            res+=");\n"
        res+="\t\t \\end{pgfonlayer}\n"
    #Close up shop
    res+="\t \\end{tikzpicture}\n"
    res+="\t \\end{figure}\n"
    return res

ValuesToInsert=[79, 87, 28, 6, 46, 66, 17, 1, 58, 2]

D=[0]*10
MS=10
CS=0

print(frontmatter(),end="")

Frames=[]
Frames.append(beatFrame("The Heap Starts Empty",D,MS,CS))

#Insert Values
for value in ValuesToInsert:
    f=insertFrame(value,D,MS,CS)
    Frames.append(f)
    CS+=1
    index = CS-1
    f=compareFrame(index,D,MS,CS)
    Frames.append(f)
    parent=(index-1)//2
    while index!=0 and D[parent] > D[index]:
        f=swapFrame(index, D, MS, CS)
        Frames.append(f)
        index=parent
        f=compareFrame(index,D,MS,CS)
        Frames.append(f)
        parent=(index-1)//2

#Hold on the ending
Frames.append(beatFrame("The Final Heap",D,MS,CS))
Frames.append(beatFrame("The Final Heap",D,MS,CS))

for f in Frames:
    print(f,end="")

print(backmatter(),end="")
