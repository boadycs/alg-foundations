
class nNode:
    #Create A New Tree Node
    #I know who is below me and to the left
    def __init__(self,v):
        self.value = v
        self.leftMostChild = None
        self.rightSibling = None
    #Print out the node
    def __str__(self):
        return str(self.value)
    #Accessors
    def getLeftC(self):
        return self.leftMostChild
    def getRightS(self):
        return self.rightSibling
    def getValue(self):
        return self.value
    #Mutators
    def setLeftC(self,l):
        self.leftMostChild=l
    def setRightS(self,r):
        self.rightSibling=r
    def setValue(self,v):
        self.value = v
#Inorder Print
def preorder(my_node):
    print(str(my_node))
    s = my_node.getLeftC()
    while s != None:
        preorder(s)
        s = s.getRightS()

#Lets Make Every possible tic-tac-toe board
#then compute the changes of winning on each

#Each Space on the board can have one of three values
#Blank, X or O
from enum import Enum
class Pieces(Enum):
    b=0
    x=1
    o=2

class TTboard:
    #Create a new Empty Board
    def __init__(self):
        self.board=[Pieces.b for x in range(0,9)]
        self.x_prob=50
        self.y_prob=50
        self.tie_prob=0
        self.move="None"
        self.label=""
    def setLabel(self,name):
        self.label=name
    def getLabel(self):
        return self.label
    def getMove(self):
        return self.move
    def asGraphviz(self,counter):
        self.label="Node"+str(counter)
        res="Node"+str(counter)+" [label=\""
        res=res+"X: "+str(self.x_prob)+"% "
        res=res+"Y: "+str(self.y_prob)+"% "
        res=res+"Tie: "+str(self.tie_prob)+"%"
        res=res+"\"];\n"
        return res
    #Print the Board
    def __str__(self):
        res="Board X Victory Probability: "+str(self.x_prob)+"%\n"
        res=res+"Board Y Victory Probability: "+str(self.y_prob)+"%\n"
        for i in range(1,len(self.board)+1):
            res=res+str(self.board[i-1].name)+" "
            if i%3==0 and i < 9:
                res=res+"\n"
        return res
    #Count number of Blanks
    def numBlanks(self):
        counter=0
        for x in self.board:
            if x==Pieces.b:
                counter=counter+1
        return counter
    #Make a Move
    def setPiece(self,x,y,p):
        location = x+3*y;
        self.board[location]=p
    #Copy the entire board
    def copy(self,toBoard):
        for i in range(0,len(self.board)):
            x = i%3
            y = int(i/3)
            toBoard.setPiece(x,y,self.board[i])
    #Set n-th blank space
    def setBlank(self,pos,sym):
        counter=0
        for x in range(0,len(self.board)):
            if self.board[x]==Pieces.b:
                if counter==pos:
                    self.board[x]=sym
                    pos_x = x%3
                    pos_y = int(x/3)
                    self.move="Place "+sym.name+" at ("+str(pos_x)+\
                        ", "+str(pos_y)+")"
                    return
                counter=counter+1
        return

def make_children(node,p):
    myleft=None
    T = node.getValue()
    blanks = T.numBlanks()
    if blanks==0:
        return
    for i in range(0,blanks):
        temp_board = TTboard()
        T.copy(temp_board)
        temp_board.setBlank(i,p)
        new_node = nNode(temp_board)
        if myleft==None:
            node.setLeftC(new_node)
        else:
            myleft.setRightS(new_node)
        myleft = new_node
    return

def build_board(node,p,depth=0):
    #if(depth > 1):
    #    return
    if node==None:
        return
    make_children(node,p)
    new_piece = Pieces.x if p==Pieces.o else Pieces.o
    
    s = node.getLeftC()
    while s != None:
        build_board(s,new_piece,depth+1)
        s = s.getRightS()

def compute_prob(node):
    T = node.getValue()
    if T.numBlanks()==0:
        print("Found a Victory Condition")
        return [100,0,0]
    else:
        s = node.getLeftC()
        while s!= None:
            probs = compute_prob(s)
            s=s.getRightS()
        return probs


def print_digraph(my_tree,max_depth=3):
    #Print all the names and labels
    counter=0
    print("digraph {")
    print_labels(my_tree,counter,0,max_depth)
    print_edges(my_tree,0,max_depth)
    print("}")
def print_labels(my_tree,counter,depth,max_depth):
    if depth > max_depth:
        return counter
    print(my_tree.getValue().asGraphviz(counter))
    counter=counter+1
    s = my_tree.getLeftC()
    while s != None:
        counter = print_labels(s,counter,depth+1,max_depth)
        s=s.getRightS()
    return counter
def print_edges(my_tree,depth,max_depth):
    if depth+1 > max_depth:
        return
    s=my_tree.getLeftC()
    while s != None:
        res=my_tree.getValue().getLabel()+"->"+s.getValue().getLabel()+" ";
        res=res+"[ label=\""+s.getValue().getMove()+"\"];\n"
        print(res)
        print_edges(s,depth+1,max_depth)
        s=s.getRightS()

T = TTboard()
#Generate All Possible boards
tree = nNode(T)
piece=Pieces.x
build_board(tree,piece)
#preorder(tree)

#print_digraph(tree,1)



