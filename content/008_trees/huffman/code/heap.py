#Mark Boady
#CS 260 - HW3 Answer Key
import math
import subprocess
class heap:
    def __init__(self,name_to_use):
        self.data = []
        self.name=name_to_use
        self.counter=0
    def __str__(self):
        res=""
        for x in self.data:
            res=res+str(x)+" "
        return res
    def makenull(self):
        self.data=[]
    def insert(self,x):
        self.data.append(x)
        self.upheap(len(self.data)-1)
        filename=self.name+str(self.counter).zfill(2)
        self.output_dot(filename+".dot")
        subprocess.run(["dot","-Teps","-o"+filename+".eps",filename+".dot"])
        self.counter=self.counter+1
    def parent(self,index):
        return int( (index-1)/2 )
    def left(self,index):
        return (index+1)*2-1
    def right(self,index):
        return (index+1)*2
    def swap(self,a,b):
        temp = self.data[a]
        self.data[a] = self.data[b]
        self.data[b] = temp
    def upheap(self,index):
        p = self.parent(index)
        if p < 0:
            return
        if self.data[p] > self.data[index]:
            self.swap(index,p)
            self.upheap(p)
        return
    def inorder(self,index):
        if(index >= len(self.data)):
            return ""
        res=""
        res=res+self.inorder(self.left(index))
        res=res+str(self.data[index])+" "
        res=res+self.inorder(self.right(index))
        return res
    def preorder(self,index):
        if(index >= len(self.data)):
            return ""
        res=""
        res=res+str(self.data[index])+" "
        res=res+self.preorder(self.left(index))
        res=res+self.preorder(self.right(index))
        return res
    def postorder(self,index):
        if(index >= len(self.data)):
            return ""
        res=""
        res=res+self.postorder(self.left(index))
        res=res+self.postorder(self.right(index))
        res=res+str(self.data[index])+" "
        return res
    def min(self):
        if len(self.data)>0:
            return self.data[0]
        else:
            return None
    def deletemin(self):
        if len(self.data) < 1:
            return
        #Swap min with last space
        self.data[0] = self.data[len(self.data)-1]
        self.data.pop()
        self.downheap(0)
        filename=self.name+str(self.counter).zfill(2)
        self.output_dot(filename+".dot")
        subprocess.run(["dot","-Teps","-o"+filename+".eps",filename+".dot"])
        self.counter=self.counter+1

    def downheap(self,index):
        l=self.left(index)
        r=self.right(index)
        #No Children
        if l >= len(self.data):
            return
        #Only One Child
        if r >= len(self.data):
            min_child = l
        else:
            if self.data[l] < self.data[r]:
                min_child = l
            else:
                min_child = r
        if self.data[index] > self.data[min_child]:
            self.swap(index,min_child)
            self.downheap(min_child)
            return
    def output_dot(self,filename):
        f = open(filename,'w')
        res=""
        res=res+"digraph \n"
        res=res+"{ \n"
        for i in range(0,len(self.data)):
            res=res+"\t node"+str(i)+"[label=\""+str(self.data[i])+"\";]\n"
        for i in range(0,len(self.data)):
            if self.left(i) < len(self.data):
                res=res+"\t node"+str(i)+" -> node"+str(self.left(i))+";\n"
            if self.right(i) < len(self.data):
                res=res+"\t node"+str(i)+" -> node"+str(self.right(i))+";\n"
            if self.left(i) < len(self.data) and self.right(i) >= len(self.data):
                res=res+" null [label=\"Null\"; shape=none;]\n"
                res=res+"\t node"+str(i)+" -> null; \n "
        res=res+"} \n"
        f.write(res)



def print_all(H):
    print("Array: "+str(H))
    print("Preorder: "+H.preorder(0))
    print("Inorder: "+H.inorder(0))
    print("Postorder: "+H.postorder(0))
    print("Current Min: "+str(H.min()))


if __name__=="__main__":
    import random
    
    H = heap("makeheap_01_")
    T = [random.randint(0,100) for x in range(0,10)]
    for x in T:
        print("insert "+str(x))
    for x in T:
        H.insert(x)
    print_all(H)
    while H.min() != None:
        print(H.min())
        H.deletemin()



