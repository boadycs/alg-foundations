#Mark Boady
#Determine Huffman Codes
import htree
import sys

#countLetters counts the number
#of times each letter appears in
# a file.
def countLetters(filename):
    D={}
    f = open(filename,"r")
    for line in f:
        for letter in line:
            num=ord(letter)
            if num in D.keys():
                D[num]+=1
            else:
                D[num]=1
    f.close()
    return D

#getTotal counts the total number of letters
def getTotal(D):
    grand=0
    for letter in D.keys():
        grand+=D[letter]
    return grand

#Get the Precent for each letter
def getPrecents(letterCount,grandTotal):
    P = {}
    for letter in letterCount.keys():
        P[letter] = letterCount[letter]/grandTotal
    return P

#Expected Size
def estimateSize(letterCount,codes):
    total=0
    for letter in letterCount:
        total+=len(codes[letter])*letterCount[letter]
    return total

#printTable prints a pretty table
#Format is | Char Code | Letter | Count | Binary |
#Formatted for Markdown
def printTable(count,precent,codes):
    print("Non-printing and whitespace characters shown with N/W")
    print()
    #Draw the Titles
    titleRow="| {:^8s} | {:^8s} | {:^8s} | {:^8s} | {:^15s} |"
    print(titleRow.format(
        "Unicode",
        "Letter",
        "Count",
        "Precent",
        "Binary"))
    #Draw a line row
    print("|"+("-"*10)+"|",end="")
    print(("-"*10)+"|",end="")
    print(("-"*10)+"|",end="")
    print(("-"*10)+"|",end="")
    print(("-"*17)+"|")
    #Rows
    rowFormat="| {:^8d} | {:^8s} | {:8d} | {:>7.4f}% | {:>15s} |"
    letters = list(count.keys())
    letters.sort()
    for letter in letters:
        if letter >= 33 and letter <= 126:
            disp = chr(letter)
        else:
            disp = "N/W"
        total = count[letter]
        prec = precent[letter]*100
        print(rowFormat.format(
            letter,
            disp,
            total,
            prec,
            codes[letter]
            ))
    
if __name__=="__main__":
    if len(sys.argv)!=2:
        print("Usage: python3 huffman.py [filename.txt]")
        sys.exit(0)
    #Parse and Understand the File
    letterCount=countLetters(sys.argv[1])
    grandTotal = getTotal(letterCount)
    precents = getPrecents(letterCount,grandTotal)
    #Run Huffman
    codes = htree.huffman(precents)
    
    print()
    printTable(letterCount,precents,codes)
    
    rawSize = grandTotal*8
    compressedSize=estimateSize(letterCount,codes)
    
    print("Total Letters:",grandTotal)
    print("Raw ASCII Size:",rawSize,"bits.")
    print("Compressed Size:",compressedSize,"bits.")
    print("Compression Ratio:",rawSize/compressedSize)
