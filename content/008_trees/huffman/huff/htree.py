#Mark Boady
#Tree for Huffman Codes

import heapq

#A node
#has optional letter [leaves]
#has precentage
#Has two children
class Node:
    def __init__(self,prec,letter=0):
        self.p = prec
        self.l = letter
        self.left = None
        self.right = None
    def __str__(self):
        res="["
        res+=str(self.l)
        res+=","
        res+=str(self.p)
        res+="] "
        return res
        
#Huffman Tree
class HTree:
    def __init__(self,prec,letter=0):
        self.root = Node(prec,letter)
    def __str__(self):
        return self.preorder(self.root)
    def preorder(self,current):
        if current==None:
            return ""
        else:
            res=""
            res+=str(current)
            res+=self.preorder(current.left)
            res+=self.preorder(current.right)
            return res
    def setLeft(self,n):
        self.root.left = n.root
    def setRight(self,n):
        self.root.right = n.root
    def getPrecent(self):
        return self.root.p
    def __lt__(self,other):
        return self.root.p < other.root.p

def encodeTree(myTree):
    codes={}
    encodeTreeR(myTree.root,codes,"")
    return codes
def encodeTreeR(myTree,codes,binary):
    if myTree.right==None and myTree.left==None:
        codes[myTree.l]=binary
    else:
        encodeTreeR(
            myTree.left,
            codes,
            binary+"0")
        encodeTreeR(
            myTree.right,
            codes,
            binary+"1")

def huffman(letters):
    P=[]
    #Make the Heap
    for letter in letters.keys():
        T = HTree(letters[letter],letter)
        heapq.heappush(P,T)
    #Make the Code Tree
    while len(P) > 1:
        A = heapq.heappop(P)
        B = heapq.heappop(P)
        X = HTree(A.getPrecent()+B.getPrecent())
        if A.getPrecent() < B.getPrecent():
            X.setLeft(A)
            X.setRight(B)
        else:
            X.setLeft(B)
            X.setRight(A)
        heapq.heappush(P,X)
    #Encode
    return encodeTree(P[0])
    

if __name__=="__main__":

    example={
        ord("a"):.12,
        ord("b"):.40,
        ord("c"):.15,
        ord("d"):.08,
        ord("e"):.25
    }
    B=huffman(example)

    #Print out
    letters = list(B.keys())
    letters.sort()
    for letter in letters:
        print(chr(letter),B[letter])
