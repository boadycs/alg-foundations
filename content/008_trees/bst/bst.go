//Mark Boady
//Drexel University 2021

//A Binary Search Tree

package main

import (
	"fmt"
)

//Node is a Data Structure to hold a binary tree node.
type Node struct {
	//Store the Node's Value
	value int
	//Pointer to Left subtree
	left *Node
	//Pointer to Right subtree
	right *Node
}

//BST is a Data Structure to hold a Binary Search Tree
type BST struct {
	//We need a pointer to the Root node
	root *Node
}

//Binary Search Tree Functions

//MakeTree creates a new empty binary search tree
func MakeTree() *BST {
	//Allocate a new BST and get pointer
	var T *BST = new(BST)
	//Set the root to be empty
	T.root = nil
	//Return pointer to tree
	return T
}

//GetRoot returns the root node of the tree
func GetRoot(T *BST) *Node {
	return T.root
}

//Height tells you the height of the tree
func Height(T *BST) int {
	//The tree height is the node height of the root
	return nodeHeight(T.root)
}

//nodeHeight determines the height of a node
func nodeHeight(currentNode *Node) int {
	if currentNode == nil {
		return -1
	}
	var leftHeight int = nodeHeight(currentNode.left)
	var rightHeight int = nodeHeight(currentNode.right)
	if leftHeight > rightHeight {
		return leftHeight + 1
	}
	return rightHeight + 1
}

//Find searches for target value in tree
func Find(value int, Tree *BST) bool {
	//Start recursive search at the root
	return findInTree(value, Tree.root)
}

//findInTree searches nodes for a value
func findInTree(value int, currentNode *Node) bool {
	//If the node is empty then nowhere to look.
	if currentNode == nil {
		return false
	}
	//If the value is the current node, we found target
	if currentNode.value == value {
		return true
	}
	//Otherwise, compare and search one side.
	if currentNode.value > value {
		//Node is to big, search left
		return findInTree(value, currentNode.left)
	}
	//Node is to small, search right
	return findInTree(value, currentNode.right)
}

//Insert inserts a new value into the tree (if not already in it).
func Insert(value int, Tree *BST) {
	//This might change the root node
	Tree.root = insertValue(value, Tree.root)
}

//insertValue recursively searches for a location to insert the value.
//It returns a pointer to the updated node.
func insertValue(value int, currentNode *Node) *Node {
	//If we are an empty position, create a new node
	if currentNode == nil {
		var newNode *Node = new(Node)
		newNode.value = value
		newNode.left = nil
		newNode.right = nil
		return newNode
	}
	//If the value is found, ignore it
	if currentNode.value == value {
		return currentNode //No Changes
	}
	//Recursively update either left or right side
	if currentNode.value > value {
		//Insert and update on left
		currentNode.left = insertValue(value, currentNode.left)
		//Return updated node
		return currentNode
	}
	//Insert and update on right
	currentNode.right = insertValue(value, currentNode.right)
	//Return updated node
	return currentNode
}

//Print Functions

//Preorder prints the tree in preorder format.
func Preorder(Tree *BST) {
	//Start printing at root
	printPreorder(Tree.root)
	//End with a newline
	fmt.Println()
}

//printPreorder Recursively prints in preorder
func printPreorder(currentNode *Node) {
	if currentNode == nil {
		fmt.Print("N ")
		return
	}
	fmt.Print(currentNode.value)
	fmt.Print(" ")
	printPreorder(currentNode.left)
	printPreorder(currentNode.right)
	return
}

//Postorder prints the tree in postorder format.
func Postorder(Tree *BST) {
	//Start printing at root
	printPostorder(Tree.root)
	//End with a newline
	fmt.Println()
}

//printPostorder Recursively prints in Postorder
func printPostorder(currentNode *Node) {
	if currentNode == nil {
		fmt.Print("N ")
		return
	}
	printPostorder(currentNode.left)
	printPostorder(currentNode.right)
	fmt.Print(currentNode.value)
	fmt.Print(" ")
	return
}

//Inorder prints the tree in inorder format.
func Inorder(Tree *BST) {
	//Start printing at root
	printInorder(Tree.root)
	//End with a newline
	fmt.Println()
}

//printInorder Recursively prints in inorder format
func printInorder(currentNode *Node) {
	if currentNode == nil {
		fmt.Print("N ")
		return
	}
	printInorder(currentNode.left)
	fmt.Print(currentNode.value)
	fmt.Print(" ")
	printInorder(currentNode.right)
	return
}

//Delete needs a few helpers

//Min determines the minimum value in the tree
func Min(Tree *BST) int {
	return findMin(Tree.root)
}

//findMin finds the min starting at any node
func findMin(currentNode *Node) int {
	//Return -1 for error
	if currentNode == nil {
		return -1
	}
	//If no left child then at min
	if currentNode.left == nil {
		return currentNode.value
	}
	//Otherwise keep searching
	return findMin(currentNode.left)
}

//Delete removes a value from the tree
func Delete(value int, Tree *BST) {
	Tree.root = deleteValue(value, Tree.root)
}

//deleteValue removes a value starting at node given and returns updated tree
func deleteValue(value int, currentNode *Node) *Node {
	//Case 1: Empty Node
	if currentNode == nil {
		return nil
	}
	//Case 2: This is the node we want
	if currentNode.value == value {
		var x *Node = deleteNode(currentNode)
		return x
	}
	//Case 3: Search left or right for target
	if currentNode.value > value {
		//Search left
		currentNode.left = deleteValue(value, currentNode.left)
		return currentNode
	}
	//Search right
	currentNode.right = deleteValue(value, currentNode.right)
	return currentNode
}

//deleteNode removes the node with pointer given. Returns new pointer
func deleteNode(currentNode *Node) *Node {
	//Case 1: If no left child then replace with right child
	if currentNode.left == nil {
		return currentNode.right
	}
	//Case 2: If no right child then replace with left child
	if currentNode.right == nil {
		return currentNode.left
	}
	//Case 3: Swap with min value on right side
	var minVal int = findMin(currentNode.right)
	//Swap min here
	currentNode.value = minVal
	//Delete from right side
	currentNode.right = deleteValue(minVal, currentNode.right)
	//Return updated pointer
	return currentNode
}

//Test the BST
func main() {
	//Test Values
	tests := [7]int{6, 2, 8, 1, 3, 7, 9}
	//Create a new Tree
	var myTree *BST = new(BST)
	//Insert all values
	fmt.Println("Test Insert")
	for i := 0; i < 7; i++ {
		Insert(tests[i], myTree)
		fmt.Printf("Inserted %d\n", tests[i])
		fmt.Printf("Current Height: %d\n", Height(myTree))
		fmt.Println("Preorder")
		Preorder(myTree)
		fmt.Println("Inorder")
		Inorder(myTree)
		fmt.Println("Postorder")
		Postorder(myTree)
	}
	fmt.Println()
	fmt.Println("Test Find")
	//Find a bunch of values
	for i := 0; i < 15; i++ {
		res := Find(i, myTree)
		fmt.Printf("Search for %d in BST and returned %t\n", i, res)
	}
	fmt.Println()
	fmt.Println("Test Delete")
	for i := 0; i < 7; i++ {
		fmt.Printf("Delete %d\n", tests[i])
		Delete(tests[i], myTree)
		fmt.Printf("Current Height: %d\n", Height(myTree))
		fmt.Println("Preorder")
		Preorder(myTree)
		fmt.Println("Inorder")
		Inorder(myTree)
		fmt.Println("Postorder")
		Postorder(myTree)
	}
}
