package main

import "fmt"

/*
	Mark Boady
	Feb 2021 Drexel University
*/

func linearIterative(needle int, haystack []int, size int) bool {
	var i int = 0
	for i < size {
		if needle == haystack[i] {
			return true
		}
		i = i + 1
	}
	return false
}

func linearRecursive(needle int, haystack []int, size int) bool {
	return linearRecHelper(needle, haystack, 0, size-1)
}
func linearRecHelper(needle int, haystack []int, start int, stop int) bool {
	//Nothing Left to search
	if start > stop {
		return false
	}
	//Compare to first element
	if needle == haystack[start] {
		return true
	}
	//Search the remaining elements
	return linearRecHelper(needle, haystack, start+1, stop)
}

func binaryRecursive(needle int, haystack []int, size int) bool {
	return binaryRecHelper(needle, haystack, 0, size-1)
}
func binaryRecHelper(needle int, haystack []int, start int, stop int) bool {
	//No elements to search
	if start > stop {
		return false
	}
	//Determine the middle
	var middle int = start + (stop-start)/2
	//Check for target
	if needle == haystack[middle] {
		return true
	}
	//Search either left or right side
	if needle < haystack[middle] {
		return binaryRecHelper(needle, haystack, start, middle-1)
	}
	return binaryRecHelper(needle, haystack, middle+1, stop)
}

func binaryIterative(needle int, haystack []int, size int) bool {
	var start int = 0
	var stop int = size - 1
	for start <= stop {
		var middle int = start + (stop-start)/2
		if needle == haystack[middle] {
			return true
		}
		if needle < haystack[middle] {
			//Eliminate Upper Section
			stop = middle - 1
		} else {
			//Eliminate Lower Section
			start = middle + 1
		}
	}
	//Never Found
	return false
}

func main() {
	a := []int{2, 4, 6, 8, 10, 12}
	var passed int = 0
	var failed int = 0
	for i := -5; i < 15; i++ {
		//Expected Answer
		var expected bool
		if i < 2 {
			expected = false
		} else if i > 12 {
			expected = false
		} else if i%2 == 1 {
			expected = false
		} else {
			expected = true
		}
		var result bool = binaryIterative(i, a, len(a))
		if expected == result {
			passed++
		} else {
			fmt.Printf("Failed: %d\n", i)
			failed++
		}
	}
	if failed != 0 {
		fmt.Println("FAILURE!!!")
	}
	fmt.Printf("Passed %d out of %d.\n", passed, passed+failed)

}
