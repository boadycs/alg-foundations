package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

//Ask if the letter is the one the user is thinking. True means yes.
func isLetter(letter byte) bool {
	fmt.Printf("Are you thinking of the letter '%s'?\n", string(letter))
	rdr := bufio.NewReader(os.Stdin)
	line, err := rdr.ReadString('\n')
	if err != nil {
		return false
	}
	line = strings.TrimSpace(line)
	if len(line) == 0 {
		return false
	}
	if line[0] == 'y' || line[0] == 'Y' {
		return true
	}
	return false
}

//Ask the user to select a letter
func linearLetter() byte {
	//The alphabet used to select possible letters.
	const alphabet string = " !.abcdefghijklmnopqrstuvwxyz"
	//Ask about all possible letters
	return linearLetterSearch(alphabet, 0, len(alphabet)-1)
}

//Select a letter between start and stop
func linearLetterSearch(letters string, start int, stop int) byte {
	//Not found, restart
	if start > stop {
		return linearLetter()
	}
	//Ask about first letter
	if isLetter(letters[start]) {
		return letters[start]
	}
	//Search Rest of Letters
	return linearLetterSearch(letters, start+1, stop)
}

//Get an entire sentence
func getSentence() string {
	var text string = ""
	var letter byte = linearLetter()
	for letter != '!' {
		text = text + string(letter)
		//letter = linearLetter()
		letter = linearLetter()
	}
	return text
}

func main() {
	var text string = getSentence()
	fmt.Println("You typed: ")
	fmt.Println(text)
}
