package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

//Ask if the letter is the one the user is thinking. True means yes.
func isLetter(letter byte) bool {
	fmt.Printf("Is your letter '%s'? (y/n)\n", string(letter))
	rdr := bufio.NewReader(os.Stdin)
	line, err := rdr.ReadString('\n')
	if err != nil {
		return false
	}
	line = strings.TrimSpace(line)
	if len(line) == 0 {
		return false
	}
	if line[0] == 'y' || line[0] == 'Y' {
		return true
	}
	return false
}

//Ask if the letter is between to (inclusive)
func isLetterBetween(firstLetter byte, secondLetter byte) bool {
	fmt.Printf("Is your letter between '%s' and '%s'? (y/n)\n", string(firstLetter), string(secondLetter))
	rdr := bufio.NewReader(os.Stdin)
	line, err := rdr.ReadString('\n')
	if err != nil {
		return false
	}
	line = strings.TrimSpace(line)
	if len(line) == 0 {
		return false
	}
	if line[0] == 'y' || line[0] == 'Y' {
		return true
	}
	return false
}

//Ask the user to select a letter
func binaryLetter() byte {
	//The alphabet used to select possible letters.
	const alphabet string = " !.abcdefghijklmnopqrstuvwxyz"
	//Ask about all possible letters
	return binaryLetterSearch(alphabet, 0, len(alphabet)-1)
}
func binaryLetterSearch(letters string, start int, stop int) byte {
	//Case 1: No Letters to ask about. Restart
	if start > stop {
		return binaryLetter()
	}
	//Case 2: Only on letter in range
	if start == stop {
		//If letter return, otherwise restart
		if isLetter(letters[start]) {
			return letters[start]
		}
		//Above didn't return, so restart
		return binaryLetter()
	}
	//Case 3: There is a range to ask about
	var middle int = start + (stop-start)/2
	var answer bool = isLetterBetween(letters[start], letters[middle])
	if answer {
		//Search First Half
		return binaryLetterSearch(letters, start, middle)
	}
	//Search Second Half
	return binaryLetterSearch(letters, middle+1, stop)
}

//Get an entire sentence
func getSentence() string {
	var text string = ""
	var letter byte = binaryLetter()
	for letter != '!' {
		text = text + string(letter)
		letter = binaryLetter()
	}
	return text
}

func main() {
	var text string = getSentence()
	fmt.Println("You typed: ")
	fmt.Println(text)
}
