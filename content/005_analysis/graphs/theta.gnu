set terminal png

#Define ranges
set xrange [0:20]
set yrange [0:1000]
set xtics 2
set ytics 100

#Legend Position
set key at 8,900

#Labels
set xlabel 'Input'
set ylabel 'Result'
set title 'Theta Bound Example'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2
set style line 3 linecolor rgb '#01df01' linetype 1 linewidth 2

#Define out plots
T(x) = 5*x**2 + 3*x + 2
f1(x) = 4*x**2
f2(x) = 6*x**2

plot T(x) title 'T(n)=5n^2 + 3n + 2' with lines linestyle 1, \
	f1(x) title '4n^2' with lines linestyle 2, \
	f2(x) title '6n^2' with lines linestyle 3
