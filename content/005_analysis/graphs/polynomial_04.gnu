set terminal png

#Define ranges
set xrange [0:40]
set yrange [0:3]
set xtics 5
set ytics 0.5

#Legend Position
set key at 20,2.5

#Labels
set xlabel 'Input'
set ylabel 'Result'
set title 'Increasing Towards a Constant'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
#set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
k(x) = (5*x**2)/(5*x**2 + 3*x - 3)
#f(x) = 5*x

plot k(x) title 'r(x)=(5x^2)/(5x^2 + 3x - 3)' with lines linestyle 1
