set terminal png

#Define ranges
set xrange [0:10]
set yrange [0:300]
set xtics 5
set ytics 20

#Legend Position
set key at 9,40

#Labels
set xlabel 'Input'
set ylabel 'Result'
set title 'Little Oh (Loose Upper Bound) Example'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
T(x) = 5*x**2 + 3*x + 2
f(x) = x**6

plot T(x) title 'T(n)=5n^2 + 3n + 2' with lines linestyle 1, \
	f(x) title 'f(x)=x^6' with lines linestyle 2
