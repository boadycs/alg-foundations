set terminal png

#Define ranges
set xrange [0:20]
set yrange [0:10]
set xtics 2
set ytics 2

#Legend Position
set key at 18,9

#Labels
set xlabel 'Input'
set ylabel 'Result'
set title 'Decreasing Towards Zero'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
#set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
h(x) = 5/x
#f(x) = 5*x

plot h(x) title 'h(x)=5/x' with lines linestyle 1
