set terminal png

#Define ranges
set xrange [0:20]
set yrange [0:200]
set xtics 5
set ytics 25

#Legend Position
set key at 8,175

#Labels
set xlabel 'n'
set ylabel 'Operations'
set title 'Loose Bound on Number Operations`

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
t(x) = 5*x-7
f(x) = x**10

plot t(x) title 'T(n)=5n-7' with lines linestyle 1, \
	f(x) title 'n^{10}' with lines linestyle 2

