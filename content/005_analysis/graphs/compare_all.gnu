set terminal png

#Define ranges
set xrange [0:20]
set yrange [0:50]
set xtics 5
set ytics 25

#Legend Position
set key at 18,45

#Labels
set xlabel 'n'
set ylabel 'Operations'
set title 'Comparison of Common Bounds'

# Line width of the axes
set border linewidth 1.5
# Line styles
#set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
#set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
f1(x) = 1
f2(x) = log(x)/log(2)
f3(x) = x
f4(x) = x*log(x)/log(2)
f5(x) = x**2
f6(x) = 2**x

plot f1(x) title '1' with lines linestyle 1, \
	f2(x) title 'log_2(n)' with lines linestyle 2, \
	f3(x) title 'n' with lines linestyle 3, \
	f4(x) title 'n lg n' with lines linestyle 4, \
	f5(x) title 'n^{2}' with lines linestyle 5, \
	f6(x) title '2^{n}' with lines linestyle 6

