set terminal png

#Define ranges
set xrange [0:10]
set yrange [-35:0]
set xtics 5
set ytics 5

#Legend Position
set key at 8,-2

#Labels
set xlabel 'Input'
set ylabel 'Result'
set title 'Decreasing Polynomial'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
#set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
g(x) = -3*x
#f(x) = 5*x

plot g(x) title 'g(x)=-3*x' with lines linestyle 1
