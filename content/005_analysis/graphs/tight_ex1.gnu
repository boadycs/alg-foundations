set terminal png

#Define ranges
set xrange [0:20]
set yrange [0:200]
set xtics 5
set ytics 25

#Legend Position
set key at 6,175

#Labels
set xlabel 'n'
set ylabel 'Operations'
set title 'Tight Bound on Number Operations`

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
t(x) = 5*x-7
f(x) = 5*x

plot t(x) title 'T(n)=5n-7' with lines linestyle 1, \
	f(x) title '5n' with lines linestyle 2

