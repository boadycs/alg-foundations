set terminal png

#Define ranges
set xrange [0:40]
set yrange [0:20]
set xtics 5
set ytics 5

#Legend Position
set key at 15,19

#Labels
set xlabel 'n'
set ylabel 'Questions'
set title 'Search Comparisons'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
t(x) = x/2
f(x) = 1+log(x)/log(2)

plot t(x) title 'Linear 1/2*n' with lines linestyle 1, \
	f(x) title 'Binary 1+log2(n)' with lines linestyle 2

