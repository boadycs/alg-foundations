set terminal png

#Define ranges
set xrange [0:40]
set yrange [0:30]
set xtics 5
set ytics 5

#Legend Position
set key at 20,2.5

#Labels
set xlabel 'Input'
set ylabel 'Ratio'
set title 'Ratio of two functions'

# Line width of the axes
set border linewidth 1.5
# Line styles
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
#set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2

#Define out plots
k(x) = (5*x**2 + 3*x + 2)/(7*x+1)
#f(x) = 5*x

plot k(x) title 'f(n)/g(n)' with lines linestyle 1
