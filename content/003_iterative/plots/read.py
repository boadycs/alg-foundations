import matplotlib.pyplot as plt
import sys
import os.path

def t1(n):
	return 9*n+16
def t2(n):
	return 5*n**2 + 2*n + 3
	
X=[i for i in range(0,15)]
Y1=[t1(i) for i in X]
Y2=[t2(i) for i in X]

plt.plot(X,Y1,label="Version 1")
plt.plot(X,Y2,label="Version 2")
plt.xlabel("Characters n")
plt.ylabel("Operations Needed")
plt.title("Comparison of Operations")
plt.legend()
#plt.show()
myname = os.path.splitext(sys.argv[0])[0]
filename=myname+".png"
plt.savefig(filename)
