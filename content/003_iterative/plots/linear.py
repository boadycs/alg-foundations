import matplotlib.pyplot as plt
import sys
import os.path
import math

	
X=[1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288]
Y1=[0.51,0.44,0.46,0.69, 0.63, 0.65, 0.67, 0.95, 1.46, 2.73, 4.57, 8.29, 13.47, 21.47, 31.37, 51.91, 104.38, 210.70, 430.07, 845.18]

plt.plot(X,Y1,label="Version 1")
plt.xlabel("Size of Array")
plt.ylabel("Clock Ticks")
plt.title("Runtime of Linear Search")
#plt.legend()
#plt.show()
myname = os.path.splitext(sys.argv[0])[0]
filename=myname+".png"
plt.savefig(filename)
