/**
  @file
  @author Mark Boady <mwb33@drexel.edu>
  @date January 13, 2022
  @section DESCRIPTION
 
  Analysis and Coding of Linear Search
 
 */

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "time.h"
/**
 Linear Search of an Array of Numbers
 @param n is the target to search for
 @param h is a pointer to the array to search
 @param size is the number of elements in the array
 @return true if n is in h and false otherwise
 */
bool linear(int n, int* h, int size);
/**
 Generate a Random Array with n even numbers (Starting with 0)
 @param n is the size of the array you want
 @return Pointer to array created
 */
int* randArray(int n);

/**
 Print Array for Testing
 @param p is a pointer to an array
 @param n is the number of elements to print
 */
void printArray(int* p, int n);
/**
 Test Search Array A of size n for values -1 to 2*n+1
 We know that even number between 0 and 2n-2 should be found
 and no other numbers should be.
 @param A is the array to search
 @param n is the size of the array
 @return true if all tests pass and false on any failure
 */
bool testArray(int* A, int n);

/**
 Run multiple tests and return average time
 @param n is the array size to test
 @param t is the number of tests
 @return average clock ticks over all runs
 */
float averageTime(int n, int t);

/**
 Run many timing experiments and print a chart
 */
void runTimings();

int main(int argc, char** argv){
	//Simple Example
	int haystack[4] = {2,4,6,8};
	int size = 4;
	//Test 1:
	bool res1 = linear(7,haystack,size);
	bool res2 = linear(6,haystack,size);
	printf("Result of Test 1: %d\n",res1);
	printf("Result of Test 2: %d\n",res2);
	//Sizes
	printf("int: %lu\n",sizeof(int));
	printf("int*: %lu\n",sizeof(int*));
	printf("bool: %lu\n",sizeof(bool));
	//Random Testing
	//Seed the Number Generator
	srand(time(NULL));
	//run test for multiple sizes
	for(int i=0; i < 100; i++){
		int* temp = randArray(i);
		printf("Testing Arrays of Size %d\n",i);
		bool result = testArray(temp,i);
		if(result){printf("All Tests Passed\n");}
		free(temp);
	}
	//Compute Timings
	float testTime = averageTime(10,1000);
	printf("Code Took: %0.2f Clock Ticks\n", testTime);
	printf("The Clock Ticks %d times per second.\n",CLOCKS_PER_SEC);
	//Real Timings
	runTimings();
	return 0;
}

//Run Many Experiments
void runTimings(){
	printf("The Clock Ticks %d times per second.\n",CLOCKS_PER_SEC);
	printf("| %7s | %8s |\n","Size","Clocks");
	printf("| ------- | -------- |\n");
	int size = 1;
	for(int i=0; i < 20; i++){
		//Compute Timings
		float testTime = averageTime(size,1000);
		printf("| %7d | %8.2f |\n", size,testTime);
		size = size * 2;
	}
}

//Implementation of Linear Search
bool linear(int n, int* h, int size){
	for(int i=0; i < size; i++){
		if(n==h[i])
			return true;
	}
	return false;
}

void printArray(int* p, int n){
	printf("[");
	for(int i=0; i < n; i++){
		printf("%d",p[i]);
		if(i+1 < n){
			printf(", ");
		}
	}
	printf("]\n");
}

//Implemenation of Random Array Generation
int* randArray(int n){
	//Make Array
	int* A = malloc(n*sizeof(int));
	int temp;//For swapping later
	int target;//Where to swap
	int i;//For counters
	//Insert Even Numbers
	for(i=0; i < n; i++){
		A[i] = i*2;
	}
	//Shuffle
	//Swap every value with a random position
	for(i=0; i < n; i++){
		target = rand()%n;
		//Swap values at i and target
		temp = A[i];
		A[i] = A[target];
		A[target] = temp;
	}
	//Return the array we made
	return A;
}

//Test on Array
//Since we made the array we know what it should look like
bool testArray(int* A, int n){
	int start=-1;//-1 is never in the array
	int stop = 2*n+1;//Also never in the array
	for(int i=start; i <=stop; i++){
		//What does linear search say?
		bool lsearch = linear(i,A,n);
		//What do we expect?
		//If it is even in range we expect true
		bool expect = i>=0 && i < 2*n && i%2==0;
		//Error
		if(lsearch != expect){
			printf("******START ERROR********\n");
			printf("Search for %d\n",i);
			printf("Answer: %d\n",lsearch);
			printf("Expected Answer: %d\n",expect);
			printf("Array Used:\n");
			printArray(A,n);
			printf("******END ERROR********\n");
			return false;
		}
			
	}
	return true;
}

//Run t tests on size n and average
float averageTime(int n, int t){
	int total = 0;
	int* X = randArray(n);
	for(int i=0; i < t; i++){
		//Pick a random number 0 to 2n (exclusive of 2n)
		int target = rand()%(2*n);
		int before = clock();
		bool t = linear(target,X,n);
		int after = clock();
		total = total + (after-before);
	}
	free(X);
	return (float)(total)/(float)(t);
}
