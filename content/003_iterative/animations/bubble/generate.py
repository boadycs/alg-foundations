#Mark Boady
#Generate an animation showing how bubblesort works
def makeFrame(instruction,swapped,i,A,highlights=[]):
    title="Bubble Sort Example"
    res=""
    res+="\\begin{frame}\n"
    res+="\\begin{center}\n"
    res+="\t {\\bf "+title+"} \n\n"
    res+="\t \\vspace{0.1in} \n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t {\\bf Event}: \n\n"
    res+="\t "+instruction
    res+="\n\n"
    res+="\t \\line(1,0){200} \n\n"
    res+=drawArray(A, highlights)
    res+="\t \\vspace{0.1in} \n"
    res+="\t {\\bf Size}: "+str(len(A))+" \n\n"
    res+="\t {\\bf Swapped}: "+str(swapped)+" \n\n"
    res+="\t {\\bf i}: "+str(i)+"\n\n"
    #res+="\t {\\bf Array } \n\n"
    res+="\\end{center}\n"
    res+="\\end{frame}\n\n"
    return res
    
def drawArray(A,highlights):
    size=len(A)
    res="\t \\begin{tabular}{|"
    for i in range(0,size+1):
        res+=" c |"
    res+="}\n"
    res+="\t\t \\hline \n"
    res+="\t\t Index "
    for i in range(0,size):
        res+="& "+str(i)+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Value "
    for i in range(0,size):
        if i in highlights:
            res+="& {\\bf "+str(A[i])+"} "
        else:
            res+="& "+str(A[i])+" "
    res+=" \\\\ \\hline \n"
    res+="\t \\end{tabular}\n\n"
    return res

def copyText(fileStream, fileName):
    target = open(fileName,"r")
    for line in target:
        fileStream.write(line)
    target.close()


def main():
    f=open("bubble.tex","w")
    
    #Copy Front Matter
    copyText(f,"frontmatter.tex")
    
    #Variables
    A=[6,5,4,3,2,1]
    swapped=True
    i=1
    
    #Opening Slide
    content=makeFrame("Initial Array is unsorted.",swapped,i,A)
    f.write(content)
    
    #Do a Bubblesort
    while swapped:
        swapped=False
        i=1
        text="Reset Swapped=False and i=1"
        content=makeFrame(text,swapped,i,A)
        f.write(content)
        while i < len(A):
            text="Compare A[i-1] $>$ A[i]"
            content=makeFrame(text,swapped,i,A,[i-1,i])
            f.write(content)
            text="{:d} $>$ {:d} is {:s}".format(A[i-1],A[i],str(A[i-1]>A[i]))
            content=makeFrame(text,swapped,i,A,[i-1,i])
            f.write(content)
            
            if A[i-1] > A[i]:
                temp = A[i-1]
                A[i-1] = A[i]
                A[i] = temp
                text="Swap values in A[%d] and A[%d]"%(i-1,i)
                content=makeFrame(text,swapped,i,A,[i-1,i])
                f.write(content)
                swapped=True
                text="Set Swapped to True"
                content=makeFrame(text,swapped,i,A,)
                f.write(content)
            i=i+1
            text="Increment i"
            content=makeFrame(text,swapped,i,A,)
            f.write(content)
            

    #Closing Slide
    content=makeFrame("Final Array is sorted.",swapped,i,A)
    f.write(content)
    
    #Copy Back Matter
    copyText(f,"backmatter.tex")
    
    f.close()

if __name__=="__main__":
    main()
