#Mark Boady
#Generate an animation showing how bubblesort works
def makeFrame(instruction,i,j,A):
    title="Insertion Sort Example"
    res=""
    res+="\\begin{frame}\n"
    res+="\\begin{center}\n"
    res+="\t {\\bf "+title+"} \n\n"
    res+="\t \\vspace{0.01in} \n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t {\\bf Event}: \n\n"
    res+="\t "+instruction
    res+="\n\n"
    res+="\t \\line(1,0){200} \n\n"
    res+="\t \\vspace{0.02in} \n"
    res+=drawArray(A, i,j)
    res+="\t \\vspace{0.1in} \n"
    res+="\t {\\bf Size}: "+str(len(A))+" \n\n"
    if i > 0:
        res+="\t {\\bf i}: "+str(i)+" \n\n"
    else:
        res+="\t {\\bf i}: 0\n\n"
    if j > 0:
        res+="\t {\\bf j}: "+str(j)+"\n\n"
    else:
        res+="\t {\\bf j}: 0\n\n"
        
    res+="\t { \\color{blue} Blue Elements are Sorted }\n\n"
    res+="\t { \\color{red} Red Elements are Being Inserted} \n\n"
    res+="\\end{center}\n"
    res+="\\end{frame}\n\n"
    return res
    
def drawArray(A,i,j):
    size=len(A)
    res="\t \\begin{tabular}{|"
    for x in range(0,size+1):
        res+=" c |"
    res+="}\n"
    res+="\t\t \\hline \n"
    res+="\t\t Index "
    for x in range(0,size):
        res+="& "+str(x)+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Value "
    for x in range(0,size):
        if x==j:
            res+="& { \\color{red} "+str(A[x])+"} "
        elif x <= i and j!=-1:
            res+="& { \\color{blue} "+str(A[x])+"} "
        elif x < i and j==-1:
            res+="& { \\color{blue} "+str(A[x])+"} "
        else:
            res+="& "+str(A[x])+" "
    res+=" \\\\ \\hline \n"
    res+="\t\t Positions "
    for x in range(0,size):
        if x==i and x==j:
            res+="& i,j "
        elif x==i:
            res+="& i "
        elif x==j:
            res+="& j "
        else:
            res+="&  "
    res+=" \\\\ \\hline \n"
    res+="\t \\end{tabular}\n\n"
    return res

def copyText(fileStream, fileName):
    target = open(fileName,"r")
    for line in target:
        fileStream.write(line)
    target.close()


def main():
    f=open("insert.tex","w")
    
    #Copy Front Matter
    copyText(f,"frontmatter.tex")
    
    #Variables
    A=[10,8,6,7,5,9,4]
    i=-1
    j=-1
    
    #Opening Slide
    content=makeFrame("Initial Array is unsorted.",i,j,A)
    f.write(content)
    
    #Insertion Sort!!
    i=1
    size=len(A)
    while i < size:
        j=i
        content=makeFrame("Set j="+str(i),i,j,A)
        f.write(content)
        
        content=makeFrame("while j $>$ 0 and A[j-1] $>$ A[j]",i,j,A)
        f.write(content)
        
        text="while "+str(j)+" $>$ 0 and "+str(A[j-1])+" $>$ "+str(A[j])
        content=makeFrame(text,i,j,A)
        f.write(content)
        while j > 0 and A[j-1] > A[j]:
            #Swap
            temp = A[j]
            A[j]=A[j-1]
            A[j-1]=temp
            content=makeFrame("Swap j and j-1",i,j,A)
            f.write(content)
            
            j = j - 1
            content=makeFrame("Set j=j-1",i,j,A)
            f.write(content)
            text="while "+str(j)+" $>$ 0 and "+str(A[j-1])+" $>$ "+str(A[j])
            content=makeFrame(text,i,j,A)
            f.write(content)
        i = i + 1
        content=makeFrame("Set i=i+1",i,-1,A)
        f.write(content)

    #Closing Slide
    j=-1
    content=makeFrame("Final Array is sorted.",i,j,A)
    f.write(content)
    
    #Copy Back Matter
    copyText(f,"backmatter.tex")
    
    f.close()

if __name__=="__main__":
    main()
