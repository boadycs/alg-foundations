# Euclidean algorithm

## Overview

One of the most ancient algorithms is the Euclidean Algorithm for finding the **Greatest Common Divisor** of two numbers. It is named after the Greek mathematician Euclid who first described it in 300BC.

The greatest common divisor (GCD) of two integers is the largest positive integer that evenly divides both numbers.

For example, the `gcd(27, 9)` is 9. Both numbers are divisible by 1 because $\frac{27}{1}=27$ and $\frac{9}{1}=9$. They are also both divisible by 3 because $\frac{27}{3}=9$ and $\frac{9}{3}=3$. These are both smaller than the greatest common divisor 9. Both numbers are divisible by 9 because $\frac{27}{9}=3$ and $\frac{9}{9}=1$.

The GCD algorithm can be described as a recursive mathematical function.

$
\begin{align}
    \text{gcd}(a,b) =& 
    \begin{cases}
    a & b=0 \\
    \text{gcd}(b, a \mod b) & b > 0
    \end{cases}
\end{align}
$

To write this in a more programmatic way, we would use conditionals and recursion.

```Go
func gcd(a int, b int) int {
    if b == 0 {
        return a
    }
    return gcd(b, a%b)
}
```

## Example

What is the GCD of 36 and 81? 

We are executing `gcd(36,81)`. The inputs are `a=36` and `b=81`. The value of `b` is clearly not zero. We need to compute $36 \mod 81 = 36$. Since 36 is less than 81, it is the remainder of division. 

We now that the `gcd(36, 81)` is also the `gcd(81, 36)`. We now have `a=81` and `b=36`. Again, `b` is not equal to 0. We compute $81 \mod 36=9$. We still don't have the answer, so we make another recursive call.

The answer to the original question is the same as the answer to `gcd(36, 9)`. We have `a=36` and `b=9`. We still haven't reached `b` equal to 0. We compute $36 \mod 9 = 0$. We have another recursive call to make.

The next recursive call is `gcd(9,0)`. We have `a=9` and `b=0`. This is the case where `b` is equal to 0. The GCD is 9. This is also the GCD of the original question `gcd(36,81)` is also 9.

In summary, we computed the following.

$
\begin{align}
    \text{gcd}(36,81) =& \text{gcd}(81,36) \\
    \text{gcd}(81,36) =& \text{gcd}(36, 9) \\
    \text{gcd}(36,9) =& \text{gcd}(9,0) \\
    \text{gcd}(9,0) =& 9
\end{align}
$

## Justification

Why does this algorithm work? It relies on the properties of the remainder and quotient.

When we divide ($\frac{a}{b}$) two integers $a$ and $b$, we get a remainder $r$ and a quotient $q$ based on the following formula.

$
\begin{align}
    a =& q*b+r
\end{align}
$

Let us assume we have two positive integers $a$ and $b$. A GCD must exist for these two numbers. Let us assume that $g$ is the GCD. 

This means that when we divide $a$ and $b$ by $g$, there will be no remainder. This means when we look at the division formula we know that following.

$
\begin{align}
    a =& q_{1} * g + 0 \\
    b =& q_{2} * g + 0
\end{align}
$

We don't know the quotients. They could be anything, but we know the remainder is 0.

What happens when we compute $a \mod b$? We know the result will be the remainder of the division formula. We can solve this for the remainder.

$
\begin{align}
    a =& q_{3} * b+r_1 \\
    a-r_1 =& q_{3} * b \\
    -r_1 =& q_{3} * b-a \\
    r_{1} =& a-q_{3} * b
\end{align}
$

That means we can describe the $a \mod b$ as 

$
\begin{align}
    a \mod b =& a-q_{3} * b
\end{align}
$

Let us plug our definitions of $a$ and $b$ in terms of the GCD $g$ into this expression.

$
\begin{align}
    a-q_{3} * b =& (q_{1} * g + 0)-q_{3} * (q_{2} * g + 0) \\
    =& q_{1} * g - q_{3} * q_{2} * g \\
    =& g \left( q_{1} - q_{3} * q_{2} \right)
\end{align}
$

If $g$ is a common divisor of $a$ and $b$, then it is also a common divisor of $a \mod b$. 

Each recursive call will find a common divisor. We just need to make sure it is the greatest common divisor. This comes from the **base case**. When we compute $a \mod b$ the answer will become zero when $a$ divides $b$ evenly. The *first* time this happens, we return the answer. We start with larger numbers and work down. The first answer we find is the **greatest** divisor. 

This also explains why the algorithm always terminated. We can look at the value of $b$. It must be true that $a \mod b < b$. Every time we compute a remainder, the value of the second input into the function decreases. It moves towards 0. The code will terminate when it reaches $b=0$.


## Implementations

These implementations are full working files. You can run them all. They all work the same way. The output will always look the same. 

```text
Welcome to GCD Example
Enter First Positive Integer: 100
Enter Second Positive Integer: 75
The GCD of 100 and 75 is 25
```

### Go

Go is a compiled language. This code needs to be in a file named `gcd.go`. To compile the code, we tell go to build it. The command is `go build gcd.go`. This makes an executable `gcd`. We can execute the executable by typing `./gcd` on the command line.

```Go
package main

/*
   Author: Mark Boady
   Date: 2/2/2021
   Drexel University

   Go Implementation of Euclid's Algorithm
*/

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
    "strings"
)

//Function Definitions

//The Euclidean Algorithm
//Assumes a and b are positive integers
func gcd(a int, b int) int {
    if b == 0 {
        return a
    }
    return gcd(b, a%b)
}

//Ask for user input
//Repeat the question until we get a positive int
func askForInput(question string) int {
    //Print the question
    fmt.Print(question)
    //Read Input
    reader := bufio.NewReader(os.Stdin)
    text, err := reader.ReadString('\n')
    //Make sure read happened (fatal error)
    if err != nil {
        fmt.Println("Could not parse input.")
        return 0
    }
    //Remove trailing newline
    text = strings.TrimSuffix(text, "\n")
    //Convert to Integer
    userInput, err := strconv.Atoi(text)
    if err != nil {
        fmt.Println("Only Positive Integers allowed.")
        return askForInput(question)
    }
    //Make sure positive
    if userInput < 0 {
        fmt.Println("Negative Numbers not allowed.")
        return askForInput(question)
    }
    //It worked!
    return userInput
}

//Main Function of the program
func main() {
    fmt.Println("Welcome to GCD Example")
    var a int = askForInput("Enter First Positive Integer: ")
    var b int = askForInput("Enter Second Positive Integer: ")
    var result int = gcd(a, b)
    fmt.Printf("The GCD of %d and %d is %d\n", a, b, result)
    return
}

```

### Python

Python is an interpreted language. If we save this code in a file called `gcd.py` then we just need to call the interpreter. On the command line, we would just have to say `python3 gcd.py`.

```Python
#Author: Mark Boady
#Date: 2/2/2021
#Drexel University

#Python Implementation of Euclid's Algorithm for GCD
#Assumes int(a) >= 0 and int(b) >= 0

def gcd(a,b):
    if b==0:
        return a
    else:
        return gcd(b, a%b)

#Ask for an int that is greater than equal to 0
def askForInput(question):
    try:
        x=input(question)
        x=int(x)
        if x >= 0:
            return x
        print("Negative Numbers not allowed.")
        return askForInput(question)
    except ValueError:
        print("Only Positive Integers allowed.")
        return askForInput(question)

#Manage the primary input/output
def main():
    print("Welcome to GCD Example")
    a=askForInput("Enter First Positive Integer: ")
    b=askForInput("Enter Second Positive Integer: ")
    result=gcd(a,b)
    outputText="The GCD of {:d} and {:d} is {:d}"
    print(outputText.format(a,b,result));

#Python executes this code when
#run directly.
if __name__=="__main__":
    main()

```

### C++

C++ is a compiled language. Assume this code is in the file `gcd.cpp`. First, we need to compile it. Which compiler used will effect this command. Using the GNU C++ compiler, it would be `g++ -o gcdApp gcd.cpp`. We then get an executable file `gcdApp`. We can run this file from the command line by typing `./gcdApp`.

```CPP
/*
    Author: Mark Boady
    Date: 2/2/2021
    Drexel University
    
    C++ Implementation of Euclid's Algorithm
 */
 #include <iostream>
 #include <string>
 #include<limits>
 
 //Function Templates
 
 //Assumes a and b are positive integers
 int gcd(int a, int b);
 
 //Function for asking for input
 int askForInput(std::string question);
 
 //Main Function of the program
 int main(){
    std::cout << "Welcome to GCD Example" << std::endl;
    int a = askForInput("Enter First Positive Integer: ");
    int b = askForInput("Enter Second Positive Integer: ");
    int result=gcd(a,b);
    std::cout << "The GCD of " << a << " and " << b;
    std::cout << " is " << result << std::endl;
    return 0;
 }

//Functions are defined below main

//The Euclidean Algorithm
int gcd(int a, int b)
{
    if(b==0)
    {
        return a;
    }else{
        return gcd(b, a%b);
    }
}

//Ask for user input
//Repeat the question until we get a positive int
int askForInput(std::string question)
{
    //Print the question
    std::cout << question;
    int userInput;
    std::cin >> userInput;
    //See if we succeeded
    if(std::cin.fail())
    {
        //Clear the stream for the next input
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        //Print Error
        std::cout << "Only Positive Integers allowed." << std::endl;
        //Ask Again
        return askForInput(question);
    }
    //Negative Number Error
    if(userInput < 0)
    {
        //Print Error
        std::cout << "Negative Numbers not allowed." << std::endl;
        //Ask Again
        return askForInput(question);
    }
    //It worked!
    return userInput;
}
```

### Java

Java is a compiled language that runs in a virtual machine. Save this code in the file `gcd.java`.  You need to compile it to Java Virtual Machine code using `javac gcd.java`. The executable can only be run by the Java Virtual Machine. To execute it, we would call `java gcd`.

```Java
/*
    Author: Mark Boady
    Date: 2/2/2021
    Drexel University
    
    Java Implementation of Euclid's Algorithm
*/
 
//Useful Libraries
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class gcd
{
    //Main Function to ask for inputs
    public static void main(String argv[])
    {
        System.out.println("Welcome to GCD Example");
        int a = askForInput("Enter First Positive Integer: ");
        int b = askForInput("Enter Second Positive Integer: ");
        int result=gcd(a,b);
        String output=String.format(
            "The GCD of %d and %d is %d"
            ,a,b,result);
        System.out.println(output);
        return;
    }
    
    //Use Euclid's algorithm to find the gcd
    public static int gcd(int a, int b)
    {
        if(b==0)
        {
            return a;
        }else
        {
            return gcd(b, a%b);
        }
    }
    
    //Function to get a positive integer
    //with input checking
    public static int askForInput(String question)
    {
        BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
        System.out.print(question);
        String text;
        //Java Streams can become unreadable
        try{
            text = reader.readLine();
        }catch(IOException e)
        {
            System.out.println("Could not read input.");
            return 0;//Unrecoverable
        }
        //See if it is an int
        try{
            int x = Integer.parseInt(text);
            //Make sure it is positive
            if(x >= 0)
            {
                return x;
            }
            System.out.println("Negative Numbers not allowed.");
            return askForInput(question);
        }catch(NumberFormatException e) {
            System.out.println("Only Positive Integers allowed.");
            return askForInput(question);
        }
    }
 
}

```

### C

C is a compiled language. It is a very minimal language. Save this code in the file `gcd.c`.  You need to compile it using `gcc -o gcd gcd.c`. To execute it, we would call `./gcd`.

```C
/**
  @file
  @author Mark Boady <mwb33@drexel.edu>
  @date July 11, 2022
  @section DESCRIPTION
 
  Example Program Computing the GCD
 
 */

#include "stdlib.h"
#include "stdio.h"

/**
	Read from stdin until hitting a newline

	@return A pointer to a character array (null terminated) with letters read
 */
char* readFromStdin();

/**
	Check if a string is a positive integer (contains only digits)
 
	@param str is the string to check
	@return 0 if false and 1 if true
 */
char isInteger(char* str);

/**
	Convert String to Integer
 
	@param str is the string to Convert
	@return The numbers converted to an integer
 */
int strToInteger(char* str);

/**
	Compute the GCD using Euclid's Algorithm
 
	@param a is the first number to use
	@param b is the second number to use
	@return The GCD of a and b
 */
int gcd(int a,int b);

/**
	Main Program to Compute GCD
 
 @param argc is the number of Command Line Arguments
 @param argv contains the Command Line Arguments given
 @return 0 if successful and 1 on error
 
 */
int main(int argc, char** argv){
	
	//Print Welcome Message
	printf("Welcome to GCD Example\n");
	
	//Ask for first number
	printf("Enter First Positive Integer: ");
	char* firstInput = readFromStdin();
	if(!isInteger(firstInput)){
		printf("Input is not a positive integer.\n");
		return 1;
	}
	int a = strToInteger(firstInput);
	//Delete string it is no longer needed
	free(firstInput);
	
	//Ask for Second Number
	printf("Enter Second Positive Integer: ");
	char* secondInput = readFromStdin();
	if(!isInteger(secondInput)){
		printf("Input is not a positive integer.\n");
		return 1;
	}
	int b = strToInteger(secondInput);
	//Delete string it is no longer needed
	free(secondInput);
	
	//Compute the GCD
	int c = gcd(a,b);
	
	//Print Results
	printf("The GCD of %d and %d is %d\n",a,b,c);
	
	return 0;
}


//Creates a buffer array
//Reads in characters until hitting newline or
//end of file
char* readFromStdin(){
	//Create an array buffer
	int bufferSize = 100;
	char* buffer = malloc(sizeof(char)*bufferSize);
	//Place characters into buffer starting at pos 0
	int position = 0;
	//Loop over characters
	char temp;
	temp = getchar();
	while(temp != EOF && temp != '\n'){
		buffer[position]=temp;
		temp = getchar();
		position++;
	}
	//Null Terminate the String
	buffer[position]=0;
	position++;
	//Return the string we read in
	return buffer;
}

//Check if the string is an integer
//Loop over anc check each value is a digit
char isInteger(char* str){
	int pos=0;
	char c = str[pos];
	//Null Terminated String
	while(c != 0){
		//Check against ASCII Codes
		if(c < 48 || c > 57){
			return 0;
		}
		//Move to next character
		pos++;
		c = str[pos];
	}
	// No non-digits found
	return 1;
}

//Loop over digits and convert to number
int strToInteger(char* str){
	int pos=0;//Position in str
	int result = 0;//place to store result
	//Get character
	char c = str[pos];
	//Loop over letters
	while(c!=0){
		//Use ASCII Codes
		int digit = c-48;
		//Shift all digits over by 1
		result = result*10 + digit;
		//Move to next character
		pos++;
		c = str[pos];
	}
	//Return the result we computed
	return result;
}

//Implement Euclid's Algorithm
int gcd(int a,int b){
	if(b==0){
		return a;
	}
	return gcd(b,a % b);
}

```
