# Recursive Analysis

Recursive functions are common in computer science. We often have to determine how many times a recursive function will execute and how much work is will do. This is a formal method to solve this. 

We determined that `fact(n)` needed 17 operations every recursive call. It also need 11 extra operations for the base case. We can write this as a recursive function.

$
\begin{align}
    T(n) =& \begin{cases}
        11 & n=0 \\
        T(n-1)+17 & n > 0
        \end{cases}
\end{align}
$

We traditionally use the letter $T$ when counting the number of operations. This is because the count of operations is an approximation of the **time** needed to run the program. We also generally use the value $n$ as a standing for whatever **number** is used to determined the repetitions.

The first step is to **algebraically expand** the formal. In this case, the number of operations needed to compute $T(n)$ is 17 plus the number of operations needed to compute $T(n-1)$. We can replace $T(n-1)$ with another value. The number of operations to compute $T(n-1)$ is $T(n-2)+17$. We will assume $n>0$ is true for now.

$
\begin{align}
    T(n) =& T(n-1) + 17 \\
    =& \left[ T(n-2) + 17 \right] + 17
\end{align}
$

We repeat this process until we find a pattern.

$
\begin{align}
    T(n) =& T(n-1) + 17 \\
    =& \left[ T(n-2) + 17 \right] + 17 & \text{Replace Def T(n-1)} \\
    =& T(n-2) + 2 * 17 & \text{Simplify} \\
    =& \left[ T(n-3) + 17 \right] + 2 * 17 & \text{Replace Def T(n-2)} \\
    =& T(n-3) + 3 * 17 & \text{Simplify} \\
    =& \left[ T(n-4) + 17 \right] + 3 * 17 & \text{Replace Def T(n-3)} \\
    =& T(n-4) + 4 * 17 & \text{Simplify} 
\end{align}
$

We repeat this process until we see a pattern. 

$
\begin{align}
    T(n) =& T(n-1) + 17 & \text{Iteration 1}\\
    =& T(n-2) + 2 * 17 & \text{Iteration 2} \\
    =& T(n-3) + 3 * 17 & \text{Iteration 3} \\
    =& T(n-4) + 4 * 17 & \text{Iteration 4} \\
    =& T(n-k) + k * 17 & \text{Iteration k}
\end{align}
$

How many iterations do we need? We know that $T(0)=11$. So, when $T(n-k)=T(0)$ we can stop. This happens when $n-k=0$. We can solve this for $k$.

$
\begin{align}
n-k =& 0 \\
n =& k & \text{Add k both sides} 
\end{align}
$

The number of iterations we need is $n$. We can plug this into our formula.

$
\begin{align}
T(n) =& T(n-n) + n * 17 &\text{Let k=n} \\
=& T(0) + n * 17 &\text{Simplify} \\
=& 11 + 17n & \text{Use Base Case}
\end{align}
$

We find out that the number of operations is $T(n)=17n+11$. This is the exact same answer we came up with by just reasoning about the code before. This method is more formal and consistent compared to the reasoning we did before.







