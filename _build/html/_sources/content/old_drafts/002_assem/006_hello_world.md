# Hello World

## Implementation

Next, we will look at one of the first programs most people write.

```
 print("Hello World!")
```

Seems easy right? There is actually a lot happening behind the scenes in this code. First of all, what does it mean to print anything? We only have memory. There is no screen. 

A computer screen has a certain area of memory dedicated to it. The operating system will take values from this memory and display it on the screen. In a modern computer, this would be on a graphics card. A special area of memory would be dedicated to storing each pixel to display on the screen. The graphics card handles actually making this binary data into messages to send to the screen. Modern graphics do not expect the programmer to declare a color for each pixel. There are many layers of abstraction between what goes in memory and what is drawn on the screen.

In a purely text base terminal, which is what our assembly simulator is simulating, the memory just needs to be dedicated to text. You will see a few grayed out memory locations starting at location $E8_{16}$ ($232_{10}$). If you write [ASCII Codes](http://www.asciitable.com/) into this memory area it will be displayed to the screen. In our case, the **Output** area. Values placed in the output memory are shown in the display area.

*Output Memory*

![Output Memory](output_memory.jpeg)

*Simulated Screen*

![Simulated Screen](output_area.jpeg)

If we want to display text we need to store it in memory. We could look up the values in the ASCII table and do this manually. We also need to know when to stop. The value 0 is not a printing character. We can put this at the end of the text as a marker. That way we know when to stop. This is called a **Null Terminated String**.

```
 DB 48 ;H
 DB 101;e
 DB 108;l
 DB 108;l
 DB 111;o
 DB 32 ;Space
 ...
```

This gets very tiresome. Luckily, our assembler can do some of the work for us!

```
text:
    DB "Hello World!"
    DB 0 ;Null Terminator
```

```{note}
One important feature of **Null Terminated Strings** is we don't need to store their size. We can *find* the size of the string by counting spaces from the start to the null terminator.
```

To print the string, we need to copy each letter into memory. We will need to keep track of two values. The character to print and where to print it.

```
JMP printout
;----- Variables -------
text:
    DB "Hello World!"
    DB 0 ;Null Terminator
;----- Program -----
printout:
    MOV A, text ;Where is the text
    MOV B, 232 ; Where to print to
loop:
    MOV C, [A] ;Get Letter
    CMP C, 0 ; Are we at the null?
    JZ done ;If at null quit
    MOV [B], C ;Print Letter
    ADD A, 1 ;Move to next character
    ADD B, 1 ; Move next Position
    JMP loop ;Restart Loop
done:
    HLT
```

We printed a value to the screen!

## Runtime

How much work does our Hello World program do?

We could print any string. It might not always be "Hello World!". We want to talk in more general terms. Let us assume that we have a string of text that contains $n$ characters. It is followed by a null terminator 0. This means there are a total of $n+1$ bytes allocated to the string. The $n$ characters and one extra for the null terminator.

First, we can look at memory usage. The string needs to be stored in memory, that takes $8*(n+1)$ bits. Eight bits for each letter and eight bits of zero for the null terminator. The program itself also takes up space. 

Each command takes a certain amount of memory. For example, `MOV` takes two inputs. It needs 3 bytes, the `MOV` itself, the *from* location, and the *to* location.

We use 6 different instructions in this code.

| Operator | Bytes Needed | Example |
| ---------- | ---------------- | ---------- |
| JMP | 2 | `JMP printout` |
| MOV | 3 | `MOV A, text` |
| CMP | 3 | `CMP C, 0` |
| JZ | 2 | `JZ done` |
| ADD | 3 | `ADD A, 1` |
| HLT | 1 | `HLT` |

We know a byte is eight bits. We just need to count how many times each instruction is used and multiply to see how much memory is needed.

| Operator | Bits | Times Used | Total bits |
| ---------- | ---- | -------------- | ----- |
| JMP | 2 * 8 | 2 | 4 * 8 = 32 |
| MOV | 3 * 8 |  4 | 12 * 8 = 96 |
| CMP | 3 * 8 | 1 | 3 * 8 = 24 |
| JZ | 2 * 8 | 1 | 2 * 8 = 16 |
| ADD | 3 * 8 | 2 | 6 * 8 = 48 |
| HLT | 8 | 1 | 8 |

The program itself takes a total of $8 * (4+12+3+2+6+1)=8 * 28$. It takes 28 bytes which is 224 bits. The total memory this program needs to run is the fixed memory and the size of the string. This gives us a total of $8 * (28+n+1)=8 * (n+29)$. For a string of length $n$, we need $29+n$ bytes or $8(29+n)$ bits.

 This formula is called linear. The only number that changes is $n$ and it has an exponent of 1. If we plotted this memory use, it would be a straight line. We would say this program's memory usage is **linear in the number of characters in the string**.
 
 ```{note}
 A linear expression is one of the form $f(x)=a * x + b$ where $a$ and $b$ are both constants.
 ```

We also want to know how much work the program does. How many operations does the process need to complete?

The processor **always** does 1 JMP and 2 MOV operations at the start of the program. It also **always** does 1 HLT command at the end of the program. The body of the loop has two paths. If we get the null terminator, the program executes MOV, CMP, and JZ then the program ends. This will only ever happen once at the end.

For each character before the null terminator, we have a loop. We execute 2 MOV commands, 2 ADD commands, 1 CMP, 1 JZ, and 1 JMP. Note that even through the JZ does not jump, it still has to execute to test if it should jump.

In total, we can determine that every time the program runs it will absolutely do the following: 1 JMP, 3 MOV, 1 CMP, 1 JZ, 1 HLT.

For every character in the string to be printed, we also do 2n MOV, 2n ADD, n CMP, n JZ, n JMP.

| Area of Code | Operations |
| -------------- | ------------- |
| Start of Program | 1 `JMP` and 2 `MOV` |
| For Each Character | 2 `MOV`, 2 `ADD`, 1 `CMP`, 1 `JZ`, and 1 `JMP` |
| For the Null Terminator | 1 `MOV`, 1 `CMP`, and 1 `JZ` |
| End of Program | 1 `HLT` |

We can also write out how many times each operation executes in terms of $n$, the number of characters to print.

| Operation | Times Executed |
| ----------- | ------------------ |
| `ADD` | $2n$ |
| `CMP` | $n+1$ |
| `HLT` | $1$ |
| `JMP` | $n+1$ | 
| `JZ` | $n+1$ |
| `MOV` | $2n+3$ |

We will simplify this by talking in terms of operations in general. Talking about the number of ADDs is just to small a detail to be useful in most cases. The program executes 7 operations every time. It also executes 7 more operations for every characters. The means the number of operations is $7n+7$. This is another linear expression. The number of operations this program takes is **linear in the size of the string**.

Why don't we talk in seconds? Each operation might take a different amount of time on every processor. Saying $7n+7$ operations tells us more about the amount of work the program does than a time. The time is specific to a processor. This expression tells us how much **work** the processor actually does. We would also rather say $28+n$ integer spaces in memory. A 32-bit processor would need more space than an 8-bit processor, but it is $28+n$ times the space the processor uses to store an integer.

These ideas will form the basis for algorithm analysis.





