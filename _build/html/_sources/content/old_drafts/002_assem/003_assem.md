# Introduction to Assembly
    
Keeping track of all these numbers is not easy for humans. We tend to like words more then pages of numbers. **Assembly** is a human readable version of the code run on computers. Assembly has a fairly direct match with what is stored in memory. This means every processor type needs its own assembly. There are a few standardized versions, but new features require new commands.
    
An Assembly program is a plain text file. Each line represents 1 instruction we are asking the computer to do. The computer starts by executing whatever the first line in the assembly file is. 
    
We will look at a few programs and explain them without going through the full instruction set. You can see the [complete list of instructions](https://schweigi.github.io/assembler-simulator/instruction-set.html) online. I advise you to put these into the simulator and step through them to watch what happens.
    
Our first program will just add $5+10=15$.
    
```
MOV A, 5
MOV B, 10
ADD A,B
HLT
```

The first line says *move the number 5 into register A*. This command takes 3 bytes of Memory. In memory it looks like $06_{16} 00_{16} 05_{16}$. Instruction $06_{16}$ is tells the computer to move a constant value into a register. The next memory address $00_{16}$ tells it what register. The $A$ register is the first, so it is named $00_{16}$. The third memory address has $05_{16}$. This is the number to store in the register.

The second line says *move the number 10 into register B*. This looks like $06_{16}01_{16}0A_{16}$. The only different from the previous line is the target register (B is $01_{16}$) and the constant (10 is $0A_{16}$).

The third line tells the computer to *add*. The command in memory is $0A_{16}00_{16}01_{16}$. The $0A_{16}$ command means add two registers. The next two bytes tell us which registers to use. The result is stored in register $A$. Whatever the first value given to ADD is will be where the result is stored. This line is ordering the computer to do $A=A+B$. 

The last line is only 1 byte. The halt command is $00_{16}$. Halt tells the computer that it is done the program and can stop now.

We can add comments to the source code to make it easier to read. The semi-colon is used to add a comment. Anything you write after a semi-colon is ignored. It is just for the human reading the code. Blank lines are also ignored. You can use them to organize the code into sections. 

Our code with some comments is shown below.

```
MOV A, 5 ; A=5
MOV B, 10 ; B=10
ADD A,B ; A=A+B
HLT ; End Program
```

We can't do many calculation with only 4 registers. We need to access all the RAM the computer has. We need two more features to access RAM. The first is the DB command. This is an Assembly command that does not actually run on the computer. It is telling the assembler to just store a number in memory. If we say `DB 10` that literally tells the computer to put a 10 here in memory.

How can we access this number? We need to know where in memory it is stored. We can use labels in Assembly. A label is put at the beginning of the line. It needs to start with a character or a dot. It lets us make a reference to "this line". 

A revised version of the program is written below. The inputs are now stored in RAM. The output is also stored in RAM.

```
    MOV A,[input1]     ;A=input1
    MOV B,[input2]     ;B=input2
    ADD A,B        ;A=A+B
    MOV [result],A    ;result=A
    HLT        ;Exit Program
input1: DB 99 ;Store Number in RAM
input2: DB 35 ;Store Number in RAM
result: DB 0 ;Store Number in RAM
```

There are three labels in this program input1, input2, and result. If we want the value stored at this memory location we put in it square brackets. We can get things out of memory and then put them back. Even though we only have 4 registers, we can work with more numbers. We just need to move things in and out of memory. We can only have 4 active values at any time.
