# Creating Repetition

What if we wanted to add up a few numbers? Lets start with just 5 numbers. Call them $x_1, x_2, x_3, x_4,$ and $x_5$. We want to compute $x_1 + x_2 + x_3 + x_4 + x_5$. We obviously don't have enough registers. We can still total all the values.

We will use two registers. We will use $A$ to store the total. We will use $B$ to store a specific element. The basic algorithmic structure is given below.

1. Set A=0
2. Repeat 5 times
    1. Store a value in B
    2. Add it to A

We can program this explicitly in Assembly

```
    MOV A, 0 ; Let A=0
    MOV B, [x1]; Get first Value
    ADD A, B
    MOV B, [x2]; Get second Value
    ADD A, B
    MOV B, [x3]; Get third Value
    ADD A, B
    MOV B, [x4]; Get fourth Value
    ADD A, B
    MOV B, [x5]; Get fifth Value
    ADD A, B
    HLT    ;End Program
x1:     DB 10
x2:     DB 20
x3:     DB 55
x4:     DB 7
x5:     DB 8
```

We put the values into memory. We know they are all right next to each other. We don't really need to give them all different names. I know for a fact $x_2$ is the memory location next to $x_1$. That is where we put it. We can figure out where things are in memory. 

We will need to use one more register. We will use the $C$ register to track what number we are on. We will start at $x1$ by saying `MOV C, x1`. When we want to get to the second value we can just say `ADD C, 1`. Go one memory location past the value $x1$.

Note that we use `MOV C, x1` here. We want the **memory location** of $x1$. We do not want the **value** stored at x1. The square brackets got the value.

The revised code is show below.

```
    MOV A, 0 ; Let A=0
    MOV C, x1
    MOV B, [C]; Get first Value
    ADD A, B
    ADD C, 1 ;Move to the next Memory Location
    MOV B, [C]; Get second Value
    ADD A, B
    ADD C, 1 ;Move to the next Memory Location
    MOV B, [C]; Get third Value
    ADD A, B
    ADD C, 1 ;Move to the next Memory Location
    MOV B, [C]; Get fourth Value
    ADD A, B
    ADD C, 1 ;Move to the next Memory Location
    MOV B, [C]; Get fifth Value
    ADD A, B
    HLT    ;End Program
x1:     DB 10
     DB 20
     DB 55
     DB 7
     DB 8
```

Look how similar the code is every time. We basically just execute three lines of code over and over again. 

```
    ADD C, 1 
    MOV B, [C]
    ADD A, B
```

This code is repetitive. This is what computers are great at! We can tell the computer to do this for us repeatedly. We need to tell the computer to do this $5$ times.

We need a new set of commands to run code repeatedly. These commands are called **Jumps**. They tell the computer to jump to a different line in the file instead of going in order. The first jump we will look at jumps based on the **Zero Flag**.

First, we need to know how to compare two numbers. The command `CMP A, B` compares two numbers. If the numbers are equal, it sets the **Zero Flag** to true. If the numbers are not equal, it sets the **Zero Flag** to false. We can use the command `JNZ` which jumps **if** the **Zero Flag** is false. We can also use the command `JZ` which jumps **if** the **Zero Flag** is true.

We will add one more register to our list. We will use $D$ to count the number of **iterations**. An iteration is a single execute of a block of code. When the value of $D$ reaches $5$ we can stop.

```
        ;Set Up
            MOV A, 0 ; Let A=0
            MOV C, x1 ;Location of first value
            MOV D, [size] ;Find Size of Array
            ADD D, C ;Where do we stop
        loop: 
            MOV B, [C]; Get Value
            ADD A, B
            ADD C, 1 ;Move to the next Memory Location
            CMP C, D ; Did we reach the end?
            JNZ loop ; They are not equal
            JZ exit; We did 5 loops
        exit:    
            HLT    ;End Program
        size: 
            DB 5
        x1:     
            DB 10
            DB 20
            DB 55
            DB 7
            DB 8
```

The code has gotten much shorter. We have eliminated repetitive code and replaced it with a loop. 


We have built our first **Data Structure**. A **Data Structure** is a way or organize data in a computer. We have made an **Array**. An **Array** is a list of values that are all next to each other in memory. We know they are all in order, so we can predict where certain values are. We need to know two things to have an array. The `size` of the array, so we know when to stop, and the position of the first value. 

```
;This is an Array
size: 
    DB 5
x1:     
    DB 10
    DB 20
    DB 55
    DB 7
    DB 8
```

What would this code look like in a traditional programming language? The algorithm is given in **Go** below. The syntax of **Go** is reasonbly similar to other languages. Treat this more as **pseudocode**. It should give you a feel for how the algorithm would look in any language.

```{note}
**Pseudocode** is a way to write code that resembles a programming languages, but doesn't have any formal rules. It is used by program designers to sketch out the basics without focusing on specific language syntax.
```

```Go
//We need an array with 5 spaces
var size int = 5
var x1 = []int{10, 20, 55, 7, 8}
//We need a place to store the total
var a int = 0
//Loop over the array
for i := 0; i < size; i++ {
    a = a + x1[i]
}
```

Programming Languages generally don't want programmers to directly work with memory address. The command `x1[i]` says to go `i` memory locations past the first memory location in `x1`. This frees the programmer from having to think about how many bytes to shift. It also ensures the programmer can't access memory they aren't allowed to.

We can start writing more useful programs with these basic tools. 

Let us write a program that adds up all the numbers from $0$ up to $N$. We will use $N=10$ as a starting point, but this will be a variable. You can change it to anything. We also need to actually do something with our result. We will make a memory location called result to put the answer in.

The algorithm in psuedocode is provided first.

```Go
var N int = 10
var result int = 0
var A int = 0
var B int = 0
for B <= N {
    A = A + B
    B = B + 1
}
result = A
```

We don't have a command for $B \le N$. We can only compare two numbers. We need to stop when $B==11$. At that point, we have reached the end. We introduce one more command. The `JMP` command jumps immediately. This can let us organize our code differently. The program always starts on the first line, but we might not want to put our instructions there. 

We use a conditional jump to exit the loop. When we don't exit, we just jump back to the start.

The assembly code is given below. Notice the labels are written on the lines above the commands. If the label has no command on it, it matches the next command line.

```
JMP start ;Go to the start of the code

N: DB 10 ;var N int = 10
result: DB 0 ;var result int = 0

start:
    MOV A, 0 ; var A int = 0
    MOV B, 0 ; var B int = 0
    ;We want to continue while B <= N  
    ;That means we stop when B == N + 1
    MOV C, [N] 
    ADD C, 1 ;Stopping point is now in C
loop:
    CMP B, C ;Determine if at stopping point 
    JZ exit ;Jump if at end
    ADD A, B ;   A = A + B
    ADD B, 1 ;    B = B + 1
    JMP loop ;Back to start of loop
exit:
    MOV [result],A ;result = A
    HLT ;Exit Program
```

When this code ends, the result of the computation is stored in the `result` memory location. We can look at it after the program ends. 

In both cases, the `result` variable will have the value $55$ because $0+1+2+3+4+5+6+7+8+9+10=55$.
