# Making Decisions

What if we want to do something that requires making a decision? For example, instead of summing up the numbers we will look for specific values. 

How would we go about searching a list of numbers?

We have three very obvious variables: 

- The value to look for.
- The first value to compare to.
- The last value to compare to.

We can find all the values in between the first and last values. The algorithm just needs to compare each value in memory until it either finds what it is looking for or runs out of numbers to search.

```Go
//Variables
var found int = 0
var target int = 35
var size int = 9
var arr = []int{1, 10, 9, 7, 21, 19, 100, 32, 99}
//Replicate Registers
var a int = 0
var b int = target
var c int = 0
var d int = size
//Loop
for c < d {
    a = arr[c]
    if a == b {
        found = 1
        break //exit loop
    }
    c = c + 1
}
//Result is in variable found
```

This code has both a loop and a decision. In Assembly, both of these will be handled by the jump commands. A loop just means we are jumping in such a way that the code repeated. A conditional is just jumping to different code block. They are both just jumps. It just depends where we jump and why.

The assembly implementation is given on the next page. Some of the jump commands create loops, some just execute different code. The only difference between a loop and a decision is where we decide to jump. 

```
;Linear Search a List of Numbers
JMP main
; //Variables
    found: DB 0 ; var found int = 0
    target: DB 19 ; var target int = 35
    size: DB 9 ; var size int = 9
; var arr = []int{1, 10, 9, 7, 21, 19, 100, 32, 99}
arr:
    DB 1
    DB 10
    DB 9
    DB 7
    DB 21
    DB 19
    DB 100
    DB 32
    DB 99
main:
    ; //Replicate Registers
    MOV A, 0 ; var a int = 0
    MOV B, [target] ; var b int = target
    MOV C, 0 ; var c int = 0
    MOV D, [size]; var d int = size
loop:
    ; for c < d {
    CMP C, D
    JZ endOfLoop
    ;    a = arr[c]
    MOV A, arr
    ADD A, C
    MOV A, [A]
    ;    if a == b {
    CMP A, B
    JZ ifCondTrue
    ADD C, 1;    c = c + 1
    JMP loop ;}
ifCondTrue:
    MOV [found], 1 ; found = 1
    HLT ;break //exit loop
; //Result is in variable found
endOfLoop:
    HLT
```
