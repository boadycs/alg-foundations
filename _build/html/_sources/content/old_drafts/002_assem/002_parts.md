# Parts of the Computer

We will be using a Javascript Assembly Simulator. It is available for free from [https://schweigi.github.io/assembler-simulator/](https://schweigi.github.io/assembler-simulator/). Feel free to download the source code and see how it works if you want.
    
The computer has RAM. This stands for Random Access Memory. It is hardware in the computer where we can store values. This computer has 255 bytes. The first memory address is 00 and the last is 255 ($FF_{16}$). Each byte can store 8 bits. At a basic level, all we can do is put a number into each of these positions. What the number means will be decided by the code we write. The RAM for our computer is shown below.
    
*The computer's RAM*
    
![Computer RAM](ram.jpg)
    
The computer's processor can't just do math anywhere in memory. The processor has a set of **Registers**. These are special memory locations on the physical processor. They are wired up to all the math circuitry in the hardware. Our Processor has 4 registers, they are labeled A, B, C, and D.

The Processor also has some extra memory directly on it for other purposes. There is an **Instruction Pointer** (IP). This is where in memory the computer should look for its next instruction to execute. There is a **Stack Pointer** (SP). This points to the end of usable memory. We will see the **Stack Pointer** when we look at functions. 

The Processor also has 3 **Flags**. These are single bits on the processor. The **Carry Flag** (C) is set to 1 if an math operation caused an overflow. It says the operation carried a bit out of available memory and an overflow happened. The **Zero Flag** (Z) is used to compare values. If two values have a difference of Zero, it is set to 1. There is also an **F** flag but it will not be encountered in our class. The registers and flags we are concerned with are shown the following image.

*The registers the processor can access.*

![Registers](registers.jpeg)

All we need to do to write a program is put the right numbers in RAM and set the IP to the right address. Then we let the computer do its thing. Sounds Easy!
