# Introduction

Algorithms are the foundation of all computer science. It is the job of the programmer to convert the ideas of an algorithm into something a computer can do. This text shows algorithms from the big picture context, but also how they are implemented on simple compute hardware. 

The primary language of the text is C. Other languages and Psuedocode are used as well. It is assumed the reader can follow code structures, even if they do not know all the languages used.

*Note: The course this book is written for is currently being revised, so this text is also being revised. Expect updates and missing sections as it gets revised.*

Have Fun!

