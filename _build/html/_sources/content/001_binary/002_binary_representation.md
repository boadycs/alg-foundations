# Binary Representations

The physical hardware of a circuit board cannot store any digits. There is no way to store $7$ on a physical wire in a computer. If we can't store any digits on a computers hardware, how can we do math?

As we saw in the previous section, digits are just symbols used to represent numbers. There are two different symbols we can send over a wire. We can send a high voltage or low voltage singles over the wire. We can use these voltages as our digits.

| Description | Digit |
| ---- | ---- |
| High Voltage | 1 |
| Low Voltage | 0 |

In practice, there is also a gap between the two voltages where the difference cannot be detected. These values would just be ignored by the hardware. 

If we only have two symbols that can be transmitted along a wire, then we need a number representation built with only two digits. 

The **Binary Number System** is a base-2 system. There are only two digits. The position of the digits is based on powers of two. A single digit in the binary system is called a **bit**. A **bit** is the amount of information that can be sent over a single wire. 

A **byte** is an 8-bit number. It would be made up of 8 individual wires. We will only be working with a single byte for now. A modern processors normally work with 64 bits (or 8 bytes) at a time. 

A single byte would have 8 digits, each representing a power of 2.


| Exponent | $2^7$ | $2^6$ | $2^5$ | $2^4$ | $2^3$ | $2^2$ | $2^1$ | $2^0$ |
| ----------- | ------- | ------- | -------- | ------- | ------- | ------- | -------- | ------- |
| Base 10 Value | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |

Just like we saw in the previous section, we can write number by putting digits next to each other. For example, $101_{2}=5_{10}$. We can use the same conversion method.

$
\begin{align}
    101_{2} =& 1*2^{2} + 0*2^{1} + 1*2^{0} \\
        =& 1*4 + 0*2 + 1*1 \\
        =& 4+1 \\
        =& 5_{10}
\end{align}
$

Since we are now talking about a physical computer, we can't just write any number of bits. An 8-bit computer would only have 8 physical wires. Each wire would need to have a value placed on it. If we are talking about an 8-bit computer, then we should write $00000101_{2}=5_{10}$. This makes it clear that the first 5 bits (wires) are to be put in the low setting. 

With a physical computer, there is a limit on the largest and smallest number the system can handle. If we put a low single on every wire we would get $00000000_{2}=0_{10}$. If we put a high single on every wire we would get $11111111_{2}=255_{10}$. If the computer only has 8-bit, you cannot represent $256_{10}$. There are not enough physical wires present.

This limitation will effect how we do math. A human can always add more digits, but a computer cannot.

When writing any number using a base system, we can assign an **index** to each digit.

| Exponent | $2^7$ | $2^6$ | $2^5$ | $2^4$ | $2^3$ | $2^2$ | $2^1$ | $2^0$ |
| ----------- | ------- | ------- | -------- | ------- | ------- | -------- | ------- | ------- |
| Index | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |

The lowest index is given to the least significant digit. The digit that contributes the least amount to the total.

To convert a number from binary (or any base) into base-10, multiply each digit by $\text{base}^{\text{index}}$ then add up the values.

The base-10 value of the binary number $10011010_{2}$ would be computed as

$
\begin{align}
    10011010_{2} =& 1*2^{7} + 0*2^{6} + 0*2^{5} + 1*2^{4} + \cdots \\
                =&    128+68+1*2^{3} + 0*2^{2} + 1*2^{1} + 0*2^{0} \\
                =&  128 + 16 + 8 + 2 \\
                =& 154_{10}
\end{align}
$

We can convert a base-10 number into binary by repeatedly dividing and taking the remainder. We want to convert 139 to binary.

$
\begin{align*}
 139 \% 2 =& 1  & 139 / 2 =& 69 \\
 69 \% 2 =& 1 & 69 / 2 =& 34 \\
 34 \% 2 =& 0 & 34 / 2 =& 17 \\
 17 \% 2 =& 1 & 17 / 2 =& 8 \\
 8 \% 2 =& 0 & 8 / 2 =& 4 \\
 4 \% 2 =& 0 & 4 / 2 =& 2 \\
 2 \% 2 =& 0 & 2 / 2 =& 1 \\
 1 \% 2 =& 1 & 1 / 2 =& 0 \\
\end{align*}
$

The binary value is 10001011. Notice that the result of each remainder calculation produces one bit.
