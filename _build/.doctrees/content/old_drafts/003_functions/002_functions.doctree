����      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�	Functions�h]�h	�Text����	Functions�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�K�source��n/Users/mwb33/Dropbox/Drexel/Courses/Readings/alg-foundations/content/old_drafts/003_functions/002_functions.md�hhubh	�	paragraph���)��}�(hX�  When designing algorithms, one of the most basic design patterns is the function. A function encapsulates a task. This allows us to study one specific task, then use it inside larger programs. For example, we might encrypt data before sending it over a network. There are lots of tasks that require encryption, so if we can design one encryption algorithm, then implement it as a function. The function can then be used in many places.�h]�hX�  When designing algorithms, one of the most basic design patterns is the function. A function encapsulates a task. This allows us to study one specific task, then use it inside larger programs. For example, we might encrypt data before sending it over a network. There are lots of tasks that require encryption, so if we can design one encryption algorithm, then implement it as a function. The function can then be used in many places.�����}�(hh0hh.hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h�A function has three parts:�h]�h�A function has three parts:�����}�(h�A function has three parts:�hh<hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh	�enumerated_list���)��}�(hhh]�(h	�	list_item���)��}�(hhh]�h-)��}�(h�BThe **function name** is the name we use to refer to the function.�h]�(h�The �����}�(h�The �hhUhhh*Nh)Nubh	�strong���)��}�(h�function name�h]�h�function name�����}�(h�function name�hh`hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hhUhhubh�- is the name we use to refer to the function.�����}�(h�- is the name we use to refer to the function.�hhUhhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhRhhubah}�(h]�h ]�h"]�h$]�h&]�uh(hPh)Kh*h+hhMhhubhQ)��}�(hhh]�h-)��}�(h�fThe **function parameters** are placeholder variables for the values the function needs to do its job.�h]�(h�The �����}�(h�The �hh�hhh*Nh)Nubh_)��}�(h�function parameters�h]�h�function parameters�����}�(h�function parameters�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hh�hhubh�K are placeholder variables for the values the function needs to do its job.�����}�(h�K are placeholder variables for the values the function needs to do its job.�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hh�hhubah}�(h]�h ]�h"]�h$]�h&]�uh(hPh)Kh*h+hhMhhubhQ)��}�(hhh]�h-)��}�(h�?The **function return value** is the result of the computation.�h]�(h�The �����}�(h�The �hh�hhh*Nh)Nubh_)��}�(h�function return value�h]�h�function return value�����}�(h�function return value�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K	h*h+hh�hhubh�" is the result of the computation.�����}�(h�" is the result of the computation.�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K	h*h+hh�hhubah}�(h]�h ]�h"]�h$]�h&]�uh(hPh)K	h*h+hhMhhubeh}�(h]�h ]�h"]�h$]�h&]�uh(hKh)Kh*h+hhhhubh-)��}�(hX  We will write a function to compute the **remainder** of a division. In mathematics, this is traditionally written as the $a \mod b$. We want to compute $ a \mod b = r$ or $a\%b=r$. This requires us two know two values $a$ and $b$. We then compute one value $r$.�h]�(h�(We will write a function to compute the �����}�(h�(We will write a function to compute the �hh�hhh*Nh)Nubh_)��}�(h�	remainder�h]�h�	remainder�����}�(h�	remainder�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hh�hhubh�E of a division. In mathematics, this is traditionally written as the �����}�(h�E of a division. In mathematics, this is traditionally written as the �hh�hhh*Nh)Nubh	�math���)��}�(h�a \mod b�h]�h�a \mod b�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh�. We want to compute �����}�(h�. We want to compute �hh�hhh*Nh)Nubh�)��}�(h� a \mod b = r�h]�h� a \mod b = r�����}�(hhhj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh� or �����}�(h� or �hh�hhh*Nh)Nubh�)��}�(h�a\%b=r�h]�h�a\%b=r�����}�(hhhj#  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh�'. This requires us two know two values �����}�(h�'. This requires us two know two values �hh�hhh*Nh)Nubh�)��}�(h�a�h]�h�a�����}�(hhhj6  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh� and �����}�(h� and �hh�hhh*Nh)Nubh�)��}�(h�b�h]�h�b�����}�(hhhjI  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh�. We then compute one value �����}�(h�. We then compute one value �hh�hhh*Nh)Nubh�)��}�(h�r�h]�h�r�����}�(hhhj\  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hh�hhubh�.�����}�(h�.�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(hXm  Our assembly languages has a divide command, but no remainder command. We can build one. The divide command computes the quotient of division $a / b = q$. The remainder is whatever is left. For example, $9 / 2 = 4$. The quotient of this division is 4. If we compute $2 * 4=8$, we don't get back $9$. We are off by 1. We could also define the remainder as $r=a-q*b$.�h]�(h��Our assembly languages has a divide command, but no remainder command. We can build one. The divide command computes the quotient of division �����}�(h��Our assembly languages has a divide command, but no remainder command. We can build one. The divide command computes the quotient of division �hju  hhh*Nh)Nubh�)��}�(h�	a / b = q�h]�h�	a / b = q�����}�(hhhj~  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hju  hhubh�2. The remainder is whatever is left. For example, �����}�(h�2. The remainder is whatever is left. For example, �hju  hhh*Nh)Nubh�)��}�(h�	9 / 2 = 4�h]�h�	9 / 2 = 4�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hju  hhubh�4. The quotient of this division is 4. If we compute �����}�(h�4. The quotient of this division is 4. If we compute �hju  hhh*Nh)Nubh�)��}�(h�2 * 4=8�h]�h�2 * 4=8�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hju  hhubh�, we don’t get back �����}�(h�, we don't get back �hju  hhh*Nh)Nubh�)��}�(h�9�h]�h�9�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hju  hhubh�9. We are off by 1. We could also define the remainder as �����}�(h�9. We are off by 1. We could also define the remainder as �hju  hhh*Nh)Nubh�)��}�(h�r=a-q*b�h]�h�r=a-q*b�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hju  hhubh�.�����}�(hjn  hju  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h�|This leads us to a very simple algorithm for computing the remainder. The algorithm given in a function named **remainder**.�h]�(h�nThis leads us to a very simple algorithm for computing the remainder. The algorithm given in a function named �����}�(h�nThis leads us to a very simple algorithm for computing the remainder. The algorithm given in a function named �hj�  hhh*Nh)Nubh_)��}�(h�	remainder�h]�h�	remainder�����}�(h�	remainder�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj�  hhubh�.�����}�(hjn  hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh	�literal_block���)��}�(h�`func remainder(a int, b int) int {
    var q int = a / b
    var r int = a - q*b
    return r
}
�h]�h�`func remainder(a int, b int) int {
    var q int = a / b
    var r int = a - q*b
    return r
}
�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]��language��Go��	xml:space��preserve�uh(j  h)Kh*h+hhhhubh-)��}�(hX�  We can run this function by hand on paper. When we use a function with specific values, it is referred to as **calling the function**.  What happens when we **call** the function `remainder(9,2)`. The first command says assign $q=a/b$. We know that $a=9$ and $b=2$ from the function call. The first number is $9$ and that matches with the first parameter $a$. This is telling us to compute $q=9/2=4$.�h]�(h�mWe can run this function by hand on paper. When we use a function with specific values, it is referred to as �����}�(h�mWe can run this function by hand on paper. When we use a function with specific values, it is referred to as �hj  hhh*Nh)Nubh_)��}�(h�calling the function�h]�h�calling the function�����}�(h�calling the function�hj!  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj  hhubh�.  What happens when we �����}�(h�.  What happens when we �hj  hhh*Nh)Nubh_)��}�(h�call�h]�h�call�����}�(h�call�hj5  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj  hhubh� the function �����}�(h� the function �hj  hhh*Nh)Nubh	�literal���)��}�(h�remainder(9,2)�h]�h�remainder(9,2)�����}�(hhhjK  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)Kh*h+hj  hhubh� . The first command says assign �����}�(h� . The first command says assign �hj  hhh*Nh)Nubh�)��}�(h�q=a/b�h]�h�q=a/b�����}�(hhhj^  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh�. We know that �����}�(h�. We know that �hj  hhh*Nh)Nubh�)��}�(h�a=9�h]�h�a=9�����}�(hhhjq  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh� and �����}�(h� and �hj  hhh*Nh)Nubh�)��}�(h�b=2�h]�h�b=2�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh�- from the function call. The first number is �����}�(h�- from the function call. The first number is �hj  hhh*Nh)Nubh�)��}�(hj�  h]�h�9�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh�+ and that matches with the first parameter �����}�(h�+ and that matches with the first parameter �hj  hhh*Nh)Nubh�)��}�(hj8  h]�h�a�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh� . This is telling us to compute �����}�(h� . This is telling us to compute �hj  hhh*Nh)Nubh�)��}�(h�q=9/2=4�h]�h�q=9/2=4�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj  hhubh�.�����}�(hjn  hj  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h��The second command says assign $r=a-q * b$. Plugging in the values we get $r=9-4*2=1$. The last command says **return** the value r. What does it mean to **return 1**? It means we are giving that back as the solution to our computation.�h]�(h�The second command says assign �����}�(h�The second command says assign �hj�  hhh*Nh)Nubh�)��}�(h�	r=a-q * b�h]�h�	r=a-q * b�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj�  hhubh� . Plugging in the values we get �����}�(h� . Plugging in the values we get �hj�  hhh*Nh)Nubh�)��}�(h�	r=9-4*2=1�h]�h�	r=9-4*2=1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj�  hhubh�. The last command says �����}�(h�. The last command says �hj�  hhh*Nh)Nubh_)��}�(h�return�h]�h�return�����}�(h�return�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj�  hhubh�# the value r. What does it mean to �����}�(h�# the value r. What does it mean to �hj�  hhh*Nh)Nubh_)��}�(h�return 1�h]�h�return 1�����}�(h�return 1�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj�  hhubh�F? It means we are giving that back as the solution to our computation.�����}�(h�F? It means we are giving that back as the solution to our computation.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h��What is the value of `remainder(9,2)+4`? The value of `remainder(9,2)` is 1, the return value. We can now do the addition $1+4=5$ and get our answer.�h]�(h�What is the value of �����}�(h�What is the value of �hj0  hhh*Nh)NubjJ  )��}�(h�remainder(9,2)+4�h]�h�remainder(9,2)+4�����}�(hhhj9  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)Kh*h+hj0  hhubh�? The value of �����}�(h�? The value of �hj0  hhh*Nh)NubjJ  )��}�(h�remainder(9,2)�h]�h�remainder(9,2)�����}�(hhhjL  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)Kh*h+hj0  hhubh�4 is 1, the return value. We can now do the addition �����}�(h�4 is 1, the return value. We can now do the addition �hj0  hhh*Nh)Nubh�)��}�(h�1+4=5�h]�h�1+4=5�����}�(hhhj_  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h�h)Kh*h+hj0  hhubh� and get our answer.�����}�(h� and get our answer.�hj0  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h��We might want to compute remainders multiple times in our code. By encapsulating this as a function, we can reuse it as needed. We have an algorithm for how the function works, now we need a program to actually do it.�h]�h��We might want to compute remainders multiple times in our code. By encapsulating this as a function, we can reuse it as needed. We have an algorithm for how the function works, now we need a program to actually do it.�����}�(hjz  hjx  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hhhhubh-)��}�(h�QFirst, we just implement the logic of the computing a remainder for fixed values.�h]�h�QFirst, we just implement the logic of the computing a remainder for fixed values.�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K!h*h+hhhhubj  )��}�(h��;Compute the remainder
JMP rem
rem:
    MOV A, 9 ;regA= 9
    MOV B, 2 ;regB = 2
    DIV B ;regA = 9/2=4
    MUL B ;regA = 4*2=8
    MOV B, 9 ; regB=9 We need to remember 9
    SUB B,A ; RegB=9-8=1 The remainder
�h]�h��;Compute the remainder
JMP rem
rem:
    MOV A, 9 ;regA= 9
    MOV B, 2 ;regB = 2
    DIV B ;regA = 9/2=4
    MUL B ;regA = 4*2=8
    MOV B, 9 ; regB=9 We need to remember 9
    SUB B,A ; RegB=9-8=1 The remainder
�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]��language��default�j  j  uh(j  h)K#h*h+hhhhubh-)��}�(h��This works mechanically, but it is not very general. What if A is already in use by something else? What if the we need to compute the remainder repeatedly for different numbers?�h]�h��This works mechanically, but it is not very general. What if A is already in use by something else? What if the we need to compute the remainder repeatedly for different numbers?�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K/h*h+hhhhubh-)��}�(hX�  This is where the **Stack Pointer** comes in. The **Stack Pointer** is the address of the last space in your program's usable memory. Assuming you didn't use all available memory, this should be pointing to empty space. We can use this as temporary memory. It gives us a place to store values that we only need for a short time, then leave the memory blank again for the next function/part of the program.�h]�(h�This is where the �����}�(h�This is where the �hj�  hhh*Nh)Nubh_)��}�(h�Stack Pointer�h]�h�Stack Pointer�����}�(h�Stack Pointer�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K1h*h+hj�  hhubh� comes in. The �����}�(h� comes in. The �hj�  hhh*Nh)Nubh_)��}�(h�Stack Pointer�h]�h�Stack Pointer�����}�(h�Stack Pointer�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K1h*h+hj�  hhubhXV   is the address of the last space in your program’s usable memory. Assuming you didn’t use all available memory, this should be pointing to empty space. We can use this as temporary memory. It gives us a place to store values that we only need for a short time, then leave the memory blank again for the next function/part of the program.�����}�(hXR   is the address of the last space in your program's usable memory. Assuming you didn't use all available memory, this should be pointing to empty space. We can use this as temporary memory. It gives us a place to store values that we only need for a short time, then leave the memory blank again for the next function/part of the program.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K1h*h+hhhhubh-)��}�(hX  The command **PUSH** puts a value in the last available memory position and decreases the stack pointer by 1. The **POP** command increases the stack pointer by one, allowing that memory location to be used again. The **POP** command also puts the value back into a register.�h]�(h�The command �����}�(h�The command �hj�  hhh*Nh)Nubh_)��}�(h�PUSH�h]�h�PUSH�����}�(h�PUSH�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K3h*h+hj�  hhubh�^ puts a value in the last available memory position and decreases the stack pointer by 1. The �����}�(h�^ puts a value in the last available memory position and decreases the stack pointer by 1. The �hj�  hhh*Nh)Nubh_)��}�(h�POP�h]�h�POP�����}�(h�POP�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K3h*h+hj�  hhubh�a command increases the stack pointer by one, allowing that memory location to be used again. The �����}�(h�a command increases the stack pointer by one, allowing that memory location to be used again. The �hj�  hhh*Nh)Nubh_)��}�(h�POP�h]�h�POP�����}�(h�POP�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K3h*h+hj�  hhubh�2 command also puts the value back into a register.�����}�(h�2 command also puts the value back into a register.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K3h*h+hhhhubh-)��}�(h��We know that remainder needs two numbers. It had two **parameters** when we wrote the function. We will decide that you need to push those two values onto the stack before you call remainder. We will get the values out of memory.�h]�(h�5We know that remainder needs two numbers. It had two �����}�(h�5We know that remainder needs two numbers. It had two �hj4  hhh*Nh)Nubh_)��}�(h�
parameters�h]�h�
parameters�����}�(h�
parameters�hj=  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K5h*h+hj4  hhubh�� when we wrote the function. We will decide that you need to push those two values onto the stack before you call remainder. We will get the values out of memory.�����}�(h�� when we wrote the function. We will decide that you need to push those two values onto the stack before you call remainder. We will get the values out of memory.�hj4  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K5h*h+hhhhubh-)��}�(h��Assuming the function call is set up correctly, we know that A is at `SP+2` and B is at `SP+1`. The stack pointer will be at an empty position. `SP+1` is the location of the **last** value pushed, this is `B`. The value of `A` is before it, at `SP+2`.�h]�(h�EAssuming the function call is set up correctly, we know that A is at �����}�(h�EAssuming the function call is set up correctly, we know that A is at �hjW  hhh*Nh)NubjJ  )��}�(h�SP+2�h]�h�SP+2�����}�(hhhj`  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh� and B is at �����}�(h� and B is at �hjW  hhh*Nh)NubjJ  )��}�(h�SP+1�h]�h�SP+1�����}�(hhhjs  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh�2. The stack pointer will be at an empty position. �����}�(h�2. The stack pointer will be at an empty position. �hjW  hhh*Nh)NubjJ  )��}�(h�SP+1�h]�h�SP+1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh� is the location of the �����}�(h� is the location of the �hjW  hhh*Nh)Nubh_)��}�(h�last�h]�h�last�����}�(h�last�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)K7h*h+hjW  hhubh� value pushed, this is �����}�(h� value pushed, this is �hjW  hhh*Nh)NubjJ  )��}�(h�B�h]�h�B�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh�. The value of �����}�(h�. The value of �hjW  hhh*Nh)NubjJ  )��}�(h�A�h]�h�A�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh� is before it, at �����}�(h� is before it, at �hjW  hhh*Nh)NubjJ  )��}�(h�SP+2�h]�h�SP+2�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)K7h*h+hjW  hhubh�.�����}�(hjn  hjW  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K7h*h+hhhhubj  )��}�(hXb  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    JMP rem ;Run the function
rem:
    MOV A, [SP+2] ;regA= 9
    MOV B, [SP+1] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+2] ; We need to remember org a
    SUB B,A ; Compute Remainder
�h]�hXb  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    JMP rem ;Run the function
rem:
    MOV A, [SP+2] ;regA= 9
    MOV B, [SP+1] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+2] ; We need to remember org a
    SUB B,A ; Compute Remainder
�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]��language�j�  j  j  uh(j  h)K9h*h+hhhhubh-)��}�(h��Assembly has a special command for **calling** functions. Since we might need to call a function from many different places in our code. How would we know where to jump back to? We wouldn't. The processor can track this for us.�h]�(h�#Assembly has a special command for �����}�(h�#Assembly has a special command for �hj�  hhh*Nh)Nubh_)��}�(h�calling�h]�h�calling�����}�(h�calling�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)KIh*h+hj�  hhubh�� functions. Since we might need to call a function from many different places in our code. How would we know where to jump back to? We wouldn’t. The processor can track this for us.�����}�(h�� functions. Since we might need to call a function from many different places in our code. How would we know where to jump back to? We wouldn't. The processor can track this for us.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)KIh*h+hhhhubh-)��}�(hX�  The `CALL` command jumps to a function and stores the position in code it left on the stack. The `RET` command returns to wherever the function was called from. Since `CALL` will use the stack, we need to change our offsets. We know the memory location of the instruction we left will be added to the stack. That instruction will be at `SP+1`, so our parameters are at `SP+2` and `SP+3`.�h]�(h�The �����}�(h�The �hj  hhh*Nh)NubjJ  )��}�(h�CALL�h]�h�CALL�����}�(hhhj&  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh�W command jumps to a function and stores the position in code it left on the stack. The �����}�(h�W command jumps to a function and stores the position in code it left on the stack. The �hj  hhh*Nh)NubjJ  )��}�(h�RET�h]�h�RET�����}�(hhhj9  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh�A command returns to wherever the function was called from. Since �����}�(h�A command returns to wherever the function was called from. Since �hj  hhh*Nh)NubjJ  )��}�(h�CALL�h]�h�CALL�����}�(hhhjL  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh�� will use the stack, we need to change our offsets. We know the memory location of the instruction we left will be added to the stack. That instruction will be at �����}�(h�� will use the stack, we need to change our offsets. We know the memory location of the instruction we left will be added to the stack. That instruction will be at �hj  hhh*Nh)NubjJ  )��}�(h�SP+1�h]�h�SP+1�����}�(hhhj_  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh�, so our parameters are at �����}�(h�, so our parameters are at �hj  hhh*Nh)NubjJ  )��}�(h�SP+2�h]�h�SP+2�����}�(hhhjr  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh� and �����}�(h� and �hj  hhh*Nh)NubjJ  )��}�(h�SP+3�h]�h�SP+3�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(jI  h)KKh*h+hj  hhubh�.�����}�(hjn  hj  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)KKh*h+hhhhubj  )��}�(hXq  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember q
    SUB B,A ; RegB= The remainder
    RET
�h]�hXq  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember q
    SUB B,A ; RegB= The remainder
    RET
�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]��language�j�  j  j  uh(j  h)KMh*h+hhhhubh-)��}�(h�DNotice that this code now returns to the correct position and halts.�h]�h�DNotice that this code now returns to the correct position and halts.�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K_h*h+hhhhubh-)��}�(hXZ  We need to place the answer somewhere in memory. We can put it back on the stack when we are done. We can put the return value in '[SP+3]' so that it can be found later. Since we are implementing this in assembly, we need to decide where the return value goes. This would normally be something a high level programming language did automatically.�h]�hX^  We need to place the answer somewhere in memory. We can put it back on the stack when we are done. We can put the return value in ‘[SP+3]’ so that it can be found later. Since we are implementing this in assembly, we need to decide where the return value goes. This would normally be something a high level programming language did automatically.�����}�(hXZ  We need to place the answer somewhere in memory. We can put it back on the stack when we are done. We can put the return value in '[SP+3]' so that it can be found later. Since we are implementing this in assembly, we need to decide where the return value goes. This would normally be something a high level programming language did automatically.�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kah*h+hhhhubj  )��}�(hX�  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember a
    SUB B,A ; RegB= The remainder
    MOV [SP+3],B
    RET
�h]�hX�  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    MOV A, [SP+3] ;regA= 9
    MOV B, [SP+2] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+3] ; We need to remember a
    SUB B,A ; RegB= The remainder
    MOV [SP+3],B
    RET
�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]��language�j�  j  j  uh(j  h)Kch*h+hhhhubh-)��}�(h��Lastly, to truly make this an **self-contained** function, it should not effect any existing code. This allows us to call that function anywhere in our code and be certain it will not have any **side-effects**.�h]�(h�Lastly, to truly make this an �����}�(h�Lastly, to truly make this an �hj�  hhh*Nh)Nubh_)��}�(h�self-contained�h]�h�self-contained�����}�(h�self-contained�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kvh*h+hj�  hhubh�� function, it should not effect any existing code. This allows us to call that function anywhere in our code and be certain it will not have any �����}�(h�� function, it should not effect any existing code. This allows us to call that function anywhere in our code and be certain it will not have any �hj�  hhh*Nh)Nubh_)��}�(h�side-effects�h]�h�side-effects�����}�(h�side-effects�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kvh*h+hj�  hhubh�.�����}�(hjn  hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kvh*h+hhhhubh	�note���)��}�(h��A **side-effect** is when a function modifies the state of a program. For example, changing variables or registers. This means that calling the function can have unexpected consequences in the program. �h]�h-)��}�(h��A **side-effect** is when a function modifies the state of a program. For example, changing variables or registers. This means that calling the function can have unexpected consequences in the program.�h]�(h�A �����}�(h�A �hj  ubh_)��}�(h�side-effect�h]�h�side-effect�����}�(h�side-effect�hj  ubah}�(h]�h ]�h"]�h$]�h&]�uh(h^h)Kh*h+hj  ubh�� is when a function modifies the state of a program. For example, changing variables or registers. This means that calling the function can have unexpected consequences in the program.�����}�(h�� is when a function modifies the state of a program. For example, changing variables or registers. This means that calling the function can have unexpected consequences in the program.�hj  ubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Kh*h+hj  ubah}�(h]�h ]�h"]�h$]�h&]�uh(j  hhhhh*h+h)Kxubh-)��}�(hXY  We need to use register A and B. What if they are already in use? We can make backup copies of A and B and restore them when we are done. We need to change our stack offsets yet again to make sure everything lines up correctly. Now, we can call this function at any point without damaging any ongoing computations. It is completely encapsulated.�h]�hXY  We need to use register A and B. What if they are already in use? We can make backup copies of A and B and restore them when we are done. We need to change our stack offsets yet again to make sure everything lines up correctly. Now, we can call this function at any point without damaging any ongoing computations. It is completely encapsulated.�����}�(hj?  hj=  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K|h*h+hhhhubj  )��}�(hX%  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
�h]�hX%  ;Compute the remainder
JMP start
start:
    PUSH 9 ;Put into last memory add
    PUSH 2 ;Put into next to last memory
    CALL rem ;Run the function
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
�����}�(hhhjK  ubah}�(h]�h ]�h"]�h$]�h&]��language�j�  j  j  uh(j  h)Kh*h+hhhhubh-)��}�(h��Now, we can use this function to solve a problem. What if we wanted to print a number? The following algorithm gives a method for printing out a number.�h]�h��Now, we can use this function to solve a problem. What if we wanted to print a number? The following algorithm gives a method for printing out a number.�����}�(hj\  hjZ  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K�h*h+hhhhubh-)��}�(h��If we want to print out a 3 digit number, we know the first digit is the number divided by 100. The remainder will leave us with the digits we need to print. By repeatedly taking digits, we can print the whole number.�h]�h��If we want to print out a 3 digit number, we know the first digit is the number divided by 100. The remainder will leave us with the digits we need to print. By repeatedly taking digits, we can print the whole number.�����}�(hjj  hjh  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K�h*h+hhhhubj  )��}�(h��var number int = 137
var order int = 100
for number != 0 {
    var digit int = number / order
    number = remainder(number, order)
    order = order / 10
    fmt.Print(digit)
}
�h]�h��var number int = 137
var order int = 100
for number != 0 {
    var digit int = number / order
    number = remainder(number, order)
    order = order / 10
    fmt.Print(digit)
}
�����}�(hhhjv  ubah}�(h]�h ]�h"]�h$]�h&]��language��Go�j  j  uh(j  h)K�h*h+hhhhubh-)��}�(h�iThe assembly for printing the result is shown below. It uses the remainder function to solve the problem.�h]�h�iThe assembly for printing the result is shown below. It uses the remainder function to solve the problem.�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)K�h*h+hhhhubj  )��}�(hX  ;Print A Number
JMP start
number: DB 255
order: DB 100
start:
    MOV D, 232 ;Output Goes here
loop:
    MOV A, [number]
    MOV B, [order]
    CMP A,0
    JZ exit ;If number==0 exit
    DIV B ;Let digit = Number/ order
    MOV C,A ;store digit for later
    ;Let Number = remainder(Number, order)
    PUSH [number]
    PUSH [order]
    CALL rem
    POP A ;The order
    POP A ;The Return Value!
    MOV [number],A
    ;Base = Base / 10
    MOV A, B ;We can only Divide in A
    DIV 10
    MOV [order], A
    ;Display ASCII for digit
    ADD C, 48 ;ASCII digits start at 48='0'
    MOV [D], C
    ADD D, 1
    JMP loop
exit:
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
�h]�hX  ;Print A Number
JMP start
number: DB 255
order: DB 100
start:
    MOV D, 232 ;Output Goes here
loop:
    MOV A, [number]
    MOV B, [order]
    CMP A,0
    JZ exit ;If number==0 exit
    DIV B ;Let digit = Number/ order
    MOV C,A ;store digit for later
    ;Let Number = remainder(Number, order)
    PUSH [number]
    PUSH [order]
    CALL rem
    POP A ;The order
    POP A ;The Return Value!
    MOV [number],A
    ;Base = Base / 10
    MOV A, B ;We can only Divide in A
    DIV 10
    MOV [order], A
    ;Display ASCII for digit
    ADD C, 48 ;ASCII digits start at 48='0'
    MOV [D], C
    ADD D, 1
    JMP loop
exit:
    HLT
rem:
    ;Make Backups
    PUSH A ;Back up register A
    PUSH B ;Back up register B
    ;Load Inputs
    MOV A, [SP+5] ;regA= 9
    MOV B, [SP+4] ;regB = 2
    DIV B ;regA = regA/regB (q in alg)
    MUL B ;regB = q*b in alg
    MOV B, [SP+5] ; We need to remember 9
    SUB B,A ; RegB=a-q*b (The remainder)
    ;Return Value
    MOV [SP+5],B
    ;Restore Backups
    POP B
    POP A
    RET
�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]��language�j�  j  j  uh(j  h)K�h*h+hhhhubeh}�(h]��	functions�ah ]�(�tex2jax_ignore��mathjax_ignore�eh"]��	functions�ah$]�h&]�uh(h
h)Kh*h+hhhhubah}�(h]�h ]�h"]�h$]�h&]��source�h+uh(h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�wordcount-words�h	�substitution_definition���)��}�(h�1039�h]�h�1039�����}�(hhhj
  ubah}�(h]�h ]�h"]��wordcount-words�ah$]�h&]�uh(j  h*h+ub�wordcount-minutes�j	  )��}�(h�5�h]�h�5�����}�(hhhj  ubah}�(h]�h ]�h"]��wordcount-minutes�ah$]�h&]�uh(j  h*h+ubu�substitution_names�}�(�wordcount-words�j  �wordcount-minutes�j  u�refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.